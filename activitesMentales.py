#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 20:16:27 2020

@author: guill
"""

from amutils import DBObject, Exo, LatexModel, UserConfig
from amutils import ui
import os
import shutil
import sys
import wx

# if True, work on a copy of the database
TEST = False

# TODO : default location for the config file ?
DEFAULT_CONF_FILE = "activitesMentales.conf"
DB = "am.db"
DBTEST_FILE = "amtest.db"

if len(sys.argv) == 2:
    confFile = os.path.abspath(sys.argv[1])
else:
    confFile = DEFAULT_CONF_FILE
d = os.path.dirname(sys.argv[0])
os.chdir(d)
if not os.path.exists(confFile):
    print("Fichier de configuration '{}' non trouvé".format(
        os.path.abspath(confFile)))
    exit(-1)
userConfig = UserConfig(confFile)
# TODO : si thumb Path n'existe pas
Exo.SetThumbPath(userConfig.GetValue('Paths', 'thumbsDir'))
if TEST:
    if not os.path.exists(DBTEST_FILE):
        shutil.copyfile(DB, DBTEST_FILE)
    DBObject.Open(DBTEST_FILE)
else:
    DBObject.Open(userConfig.GetValue('Paths', 'database'))
app = wx.App()
ui.AMFrame.userConfig = userConfig
LatexModel.userConfig = userConfig
frm = ui.AMFrame(None)
# frm.SetStash(500)
frm.Show()
app.MainLoop()
DBObject().Close()
del app
