
-- Version_0.0.1

-- Table : caseineRegexpQuestion
CREATE TABLE caseineRegexpQuestions (
    caseineQuestionId    INTEGER REFERENCES caseineQuestions (id) ON DELETE CASCADE
                                 NOT NULL,
    usehint              BOOLEAN NOT NULL
                                 DEFAULT (0),
    usecase              BOOLEAN NOT NULL
                                 DEFAULT (1),
    studentshowalternate BOOLEAN NOT NULL
                                 DEFAULT (0) 
);

--------------------------
--- caseineQ inside exo
-------------------------------
PRAGMA foreign_keys = 0;

--
-- caseineAlgebraQuestion Remove caseineQuestionId and add ExoId
--

CREATE TABLE sqlitestudio_temp_table AS SELECT * FROM caseineAlgebraQuestions;

DROP TABLE caseineAlgebraQuestions;

CREATE TABLE caseineAlgebraQuestions (
    exoId        INTEGER PRIMARY KEY
                         REFERENCES exos (id) ON DELETE CASCADE
                         NOT NULL,
    compareby    TEXT    NOT NULL
                         DEFAULT ('eval'),
    tolerance    DECIMAL NOT NULL
                         DEFAULT (0.001),
    nchecks      INTEGER NOT NULL
                         DEFAULT (10),
    answerprefix TEXT    NOT NULL
                         DEFAULT ('')
);

INSERT INTO caseineAlgebraQuestions (
                                        exoId,
                                        compareby,
                                        tolerance,
                                        nchecks,
                                        answerprefix
                                    )
                                    SELECT CQ.exoId,
                                           compareby,
                                           tolerance,
                                           nchecks,
                                           answerprefix

                                      FROM sqlitestudio_temp_table TMP
                                      LEFT JOIN caseineQuestions AS CQ ON TMP.caseineQuestionId=CQ.id;

DROP TABLE sqlitestudio_temp_table;

--
-- caseineAnswers Remove caseineQuestionId and add ExoId
--

CREATE TABLE sqlitestudio_temp_table AS SELECT * FROM caseineAnswers;

DROP TABLE caseineAnswers;

CREATE TABLE caseineAnswers (
    exoId    INTEGER REFERENCES exos (id) ON DELETE CASCADE
                     NOT NULL,
    answer   TEXT    NOT NULL
                     DEFAULT ('')
                     CHECK (trim(answer, " " || char(9) || char(10) ) != ""),
    note     INTEGER DEFAULT (100)
                     NOT NULL,
    feedback TEXT    NOT NULL
                     DEFAULT ('')
);

INSERT INTO caseineAnswers (
                               exoId,
                               answer,
                               note,
                               feedback
                           )
                           SELECT CQ.exoId,
                                  answer,
                                  note,
                                  feedback
                          FROM sqlitestudio_temp_table TMP
                              LEFT JOIN caseineQuestions AS CQ ON TMP.caseineQuestionId=CQ.id;

DROP TABLE sqlitestudio_temp_table;

--
-- caseineVariables Remove caseineQuestionId and add ExoId
--
CREATE TABLE sqlitestudio_temp_table AS SELECT * FROM caseineAlgebraQVariables;

DROP TABLE caseineAlgebraQVariables;

CREATE TABLE caseineVariables (
    exoId INTEGER REFERENCES exos (id) ON DELETE CASCADE
                  NOT NULL,
    name  TEXT    NOT NULL
                  DEFAULT ('x')
                  CHECK (trim(name, " " || char(9) || char(10) ) != ""),
    min   DECIMAL NOT NULL
                  DEFAULT ( -10.0),
    max   DECIMAL NOT NULL
                  DEFAULT (10.0)
);

INSERT INTO caseineVariables (
                                 exoId,
                                 name,
                                 min,
                                 max
                             )
                             SELECT CQ.exoId,
                                    name,
                                    min,
                                    max
                          FROM sqlitestudio_temp_table TMP
                              LEFT JOIN caseineQuestions AS CQ ON TMP.caseineQuestionId=CQ.id;

DROP TABLE sqlitestudio_temp_table;

--
-- caseineFigures Remove caseineQuestionId and add ExoId
--
CREATE TABLE sqlitestudio_temp_table AS SELECT * FROM caseineFigures;

DROP TABLE caseineFigures;

CREATE TABLE caseineFigures (
    exoId INTEGER REFERENCES exos (id) ON DELETE CASCADE
                  NOT NULL,
    name  TEXT    NOT NULL
                  CHECK (trim(name, " " || char(9) || char(10) ) != "")
                  DEFAULT (''),
    code  TEXT    NOT NULL
                  DEFAULT ('')
);

INSERT INTO caseineFigures (
                               exoId,
                               name,
                               code
                           )
                           SELECT CQ.exoId,
                                  name,
                                  code
                          FROM sqlitestudio_temp_table TMP
                              LEFT JOIN caseineQuestions AS CQ ON TMP.caseineQuestionId=CQ.id;

DROP TABLE sqlitestudio_temp_table;


--
-- caseineMultichoiceQuestions Remove caseineQuestionId and add ExoId
--

CREATE TABLE sqlitestudio_temp_table AS SELECT * FROM caseineMultichoiceQuestions;

DROP TABLE caseineMultichoiceQuestions;


CREATE TABLE caseineMultichoiceQuestions (
    exoId                    INTEGER REFERENCES exos (id) ON DELETE CASCADE
                                     NOT NULL
                                    PRIMARY KEY,
    single                   BOOLEAN NOT NULL
                                     DEFAULT (1),
    shuffleanswers           BOOLEAN NOT NULL
                                     DEFAULT (1),
    answernumbering          TEXT    NOT NULL
                                     DEFAULT ('abc'),
    correctfeedback          TEXT    NOT NULL
                                     DEFAULT (''),
    partiallycorrectfeedback TEXT    NOT NULL
                                     DEFAULT (''),
    incorrectfeedback                NOT NULL
                                     DEFAULT ('')
);

INSERT INTO caseineMultichoiceQuestions (
                                            exoId,
                                            single,
                                            shuffleanswers,
                                            answernumbering,
                                            correctfeedback,
                                            partiallycorrectfeedback,
                                            incorrectfeedback
                                        )
                                        SELECT CQ.exoId,
                                               single,
                                               shuffleanswers,
                                               answernumbering,
                                               correctfeedback,
                                               partiallycorrectfeedback,
                                               incorrectfeedback
                          FROM sqlitestudio_temp_table TMP
                              LEFT JOIN caseineQuestions AS CQ ON TMP.caseineQuestionId=CQ.id;

DROP TABLE sqlitestudio_temp_table;


--
-- caseineRegexpQuestions Remove caseineQuestionId and add ExoId
--
CREATE TABLE sqlitestudio_temp_table AS SELECT * FROM caseineRegexpQuestions;

DROP TABLE caseineRegexpQuestions;

CREATE TABLE caseineRegexpQuestions (
    exoId                        REFERENCES exos (id) ON DELETE CASCADE
                                 NOT NULL
                                 PRIMARY KEY,
    usehint              BOOLEAN NOT NULL
                                 DEFAULT (0),
    usecase              BOOLEAN NOT NULL
                                 DEFAULT (1),
    studentshowalternate BOOLEAN NOT NULL
                                 DEFAULT (0)
);

INSERT INTO caseineRegexpQuestions (
                                       exoId,
                                       usehint,
                                       usecase,
                                       studentshowalternate
                                   )
                                   SELECT CQ.exoId,
                                          usehint,
                                          usecase,
                                          studentshowalternate
                          FROM sqlitestudio_temp_table TMP
                              LEFT JOIN caseineQuestions AS CQ ON TMP.caseineQuestionId=CQ.id;

DROP TABLE sqlitestudio_temp_table;


--
-- caseineQuestions Remove caseineQuestionId and add ExoId
--

CREATE TABLE sqlitestudio_temp_table AS SELECT * FROM caseineQuestions;

DROP TABLE caseineQuestions;

CREATE TABLE caseineQuestions (
    exoId           INTEGER REFERENCES exos (id) ON DELETE CASCADE
                            NOT NULL
                            PRIMARY KEY,
    type            TEXT    NOT NULL,
    status          TEXT    NOT NULL
                            DEFAULT ('to export'),
    questiontxt     TEXT    NOT NULL
                            DEFAULT ('')
                            CHECK (trim(questiontxt, " " || char(9) || char(10) ) != ""),
    generalfeedback STRING  NOT NULL
                            DEFAULT (''),
    defaultgrade    DECIMAL NOT NULL
                            DEFAULT (1.0),
    penalty         DECIMAL NOT NULL
                            DEFAULT (0.5),
    idnumber        INTEGER REFERENCES exos (id) ON DELETE CASCADE,
    hidden          BOOLEAN NOT NULL
                            DEFAULT (0)
);

INSERT INTO caseineQuestions (
                                 exoId,
                                 type,
                                 status,
                                 questiontxt,
                                 generalfeedback,
                                 defaultgrade,
                                 penalty,
                                 idnumber,
                                 hidden
                             )
                             SELECT exoId,
                                    type,
                                    status,
                                    questiontxt,
                                    generalfeedback,
                                    defaultgrade,
                                    penalty,
                                    idnumber,
                                    hidden
                          FROM caseineQuestions;



DROP TABLE sqlitestudio_temp_table;

DROP TABLE caseineClozeQuestions;

DROP TABLE caseineTruefalseQuestions;

PRAGMA foreign_keys = 1;