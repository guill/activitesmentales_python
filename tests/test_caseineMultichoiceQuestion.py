"""
Created on 24/03/20

@author: GuiΩ
"""


import pytest
from tests import Tests, MotAleatoire
import amutils
from amutils import caseine
import sqlite3
import random


DBTEST_FILE = "amutils/tests/amtest.db"


def test_CaseineMultichoiceQuestion(Tests):

    exos = amutils.Exo.GetList()
    exo = random.choice(exos)

    print("Create a new Question q")
    q = caseine.CaseineQuestion()
    assert q.exo is None

    q.exo = exo
    assert q.type is None
    assert q.status == 'to export'
    assert q.questiontxt == ''
    assert q.answers == []
    assert q.figures == []

    with pytest.raises(sqlite3.IntegrityError):
        q.Save()
    q.type = 'multichoice'
    q.questiontxt = MotAleatoire()

    print("Save")
    q.Save()
    assert id

    q.single = random.randint(0, 1)
    q.shuffleanswers = random.randint(0, 1)
    q.answernumbering = random.choice(caseine.MULTICHOICE_NUMBERING_OPTIONS)
    q.correctfeedback = MotAleatoire()
    q.partiallycorrectfeedback = MotAleatoire()
    q.incorrectfeedback = MotAleatoire()
    q.Save()

    q2 = caseine.CaseineQuestion.FromExo(exo, forceReload=True)

    for k in q._fields.keys():
        if k != 'data':
            print(k)
            assert q2.__getattr__(k) == q.__getattr__(k)
    for k in q.data._fields.keys():
        print(k)
        assert q2.__getattr__(k) == q.__getattr__(k)



    print("add variable, answers and figures and Save")

    answer1 = caseine.CaseineAnswer({
        'answer': MotAleatoire(),
        'note': random.randint(0, 100),
        'feedback': MotAleatoire()
    })
    answer2 = caseine.CaseineAnswer({
        'answer': MotAleatoire(),
        'note': random.randint(0, 100),
        'feedback': MotAleatoire()
    })
    answers = [answer1, answer2]
    q.answers = answers
    figure1 = caseine.CaseineFigure({
        'name': MotAleatoire(),
        'code': MotAleatoire()
    })
    figure2 = caseine.CaseineFigure({
        'name': MotAleatoire(),
        'code': MotAleatoire()
    })
    figures = [figure1, figure2]
    q.figures = figures
    q.Save()

    q3 = caseine.CaseineQuestion.FromExo(exo, forceReload=True)

    print('Teste l\'enregistrement des réponses')
    for i in range(2):
        for k in answers[i]._fields.keys():
            print(k)
            assert q3.answers[i].__getattr__(k) == q.answers[i].__getattr__(k)

    print('Teste l\'enregistrement des figures')
    for i in range(2):
        for k in figures[i]._fields.keys():
            print(k)
            assert q3.figures[i].__getattr__(k) == q.figures[i].__getattr__(k)