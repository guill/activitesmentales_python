"""
Created on 24/03/20

@author: GuiΩ
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

import pytest
from tests import Tests, MotAleatoire
import amutils
from amutils import caseine
import sqlite3
import random


DBTEST_FILE = "amutils/tests/amtest.db"


def test_CaseineAlgebraQuestion(Tests):

    exos = amutils.Exo.GetList()
    exo = random.choice(exos)

    print("Create a new Question q")
    q = caseine.CaseineQuestion()
    print(q._fields)
    assert q.exo is None

    q.exo = exo
    assert q.type is None
    assert q.status == 'to export'
    assert q.questiontxt == ''
    assert q.variables == []
    assert q.answers == []
    assert q.figures == []

    with pytest.raises(sqlite3.IntegrityError):
        q.Save()

    q.type = 'algebra'
    q.questiontxt = MotAleatoire()
    print(q.__dict__, q.data.__dict__)

    print("Save")
    q.Save()

    q.answerprefix = MotAleatoire()
    q.generalfeedback = MotAleatoire()
    q.Save()
    q2 = caseine.CaseineQuestion.FromExo(exo, forceReload=True)

    for k in q._fields.keys():
        if k != 'data':
            print(k)
            assert q2.__getattr__(k) == q.__getattr__(k)
    for k in q.data._fields.keys():
        print(k)
        assert q2.__getattr__(k) == q.__getattr__(k)
    print("add variable, answers and figures and Save")

    var1 = caseine.CaseineVariable({
        'name': MotAleatoire(),
        'min': random.random()* -10,
        'max': random.random() *10})
    var2 = caseine.CaseineVariable({
        'name': MotAleatoire(),
        'min': random.random() * -10,
        'max': random.random() * 10})
    variables = [var1, var2]
    q.variables = variables

    answer1 = caseine.CaseineAnswer({
        'answer': MotAleatoire(),
        'note': random.randint(0, 100),
        'feedback': MotAleatoire()
    })
    answer2 = caseine.CaseineAnswer({
        'answer': MotAleatoire(),
        'note': random.randint(0, 100),
        'feedback': MotAleatoire()
    })
    answers = [answer1, answer2]
    q.answers = answers
    figure1 = caseine.CaseineFigure({
        'name': MotAleatoire(),
        'code': MotAleatoire()
    })
    figure2 = caseine.CaseineFigure({
        'name': MotAleatoire(),
        'code': MotAleatoire()
    })
    figures = [figure1, figure2]
    q.figures = figures
    q.Save()

    print(q._fields)

    q3 = caseine.CaseineQuestion.FromExo(exo, forceReload=True)

    print('Teste l\'enregistrement des variables')
    for i in range(2):
        assert len(q3.variables) == 2
        for k in variables[i]._fields.keys():
            assert q3.variables[i].__getattr__(k) == q.variables[i].__getattr__(k)

    print('Teste l\'enregistrement des réponses')
    for i in range(2):
        for k in answers[i]._fields.keys():
            print(k)
            assert q3.answers[i].__getattr__(k) == q.answers[i].__getattr__(k)

    print('Teste l\'enregistrement des figures')
    for i in range(2):
        for k in figures[i]._fields.keys():
            print(k)
            assert q3.figures[i].__getattr__(k) == q.figures[i].__getattr__(k)
