#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

import pytest
from tests import Tests, MotAleatoire

import amutils
import random
import sqlite3


DBTEST_FILE = "amutils/tests/amtest.db"


def test_Exo(Tests):

    print("Create new Exo e")
    e = amutils.Exo()
    assert e.id is None
    assert e.code == ''
    assert e.position is None

    with pytest.raises(sqlite3.IntegrityError):
        e.Save()

    e.code = """ \t
    """
    with pytest.raises(sqlite3.IntegrityError):
        e.Save()

    print("Save e")
    e.code = MotAleatoire()
    e.Save()
    id = e.id
    assert id

    e.position = random.randint(0, 100)
    e.Save()

    # Test de l'enregistrement des exos
    niveaux = []
    for _ in range(random.randint(1, 5)):
        niveau = random.choice(amutils.Niveau.GetList())
        while niveau in niveaux:
            niveau = random.choice(amutils.Niveau.GetList())
        niveaux.append(niveau)
    print("Ajout des niveaux ", niveaux)
    e.niveaux = niveaux
    themes = []
    for _ in range(random.randint(1, 5)):
        theme = random.choice(amutils.Theme.GetList())
        while theme in themes:
            theme = random.choice(amutils.Theme.GetList())
        themes.append(theme)
    print("Ajout des themes", themes)
    e.themes = themes
    print(e.themes, e._fields['themes'])

    print("Save and reload e")
    e.Save()

    e2 = amutils.Exo.FromId(id, forceReload=True)
    # avant de tester les variables pour charger e.exos
    assert e.niveaux == e2.niveaux
    assert e.themes == e2.themes
    assert e._fields == e2._fields

    print("Create a copy e3")
    e3 = e.Copy()
    e3.Save()
    id3 = e3.id
    assert id3 != id
    e4 = amutils.Exo.FromId(id3, forceReload=True)
    assert e.niveaux == e4.niveaux
    assert e.themes == e4.themes
    # for key, val in e._fields.items():
    #     assert  e4._fields[key] == val, 'key={}'.format(key)
    for key, val in e._fields.items():
        assert key == 'id' or val == e4._fields[key]
    print("Delete e and e3")
    e.Delete()
    assert amutils.Exo.FromId(id) is None
    vars = (id,)
    row = amutils.DBObject.connexion.execute(
                "SELECT COUNT(*) FROM lienExosNiveaux WHERE exoId=?",
                vars).fetchone()
    assert row[0] == 0

    row = amutils.DBObject.connexion.execute(
                "SELECT COUNT(*) FROM lienExosThemes WHERE exoId=?",
                vars).fetchone()
    assert row[0] == 0

    e3.Delete()

    print("Clear Exo._list")
    amutils.Exo.Clear()
    assert "_list" not in amutils.Exo.__dict__
