"""
Created on 24/03/20

@author: GuiΩ
"""

import pytest
from tests import Tests, MotAleatoire
import amutils
from amutils import caseine
import sqlite3
import random


DBTEST_FILE = "amutils/tests/amtest.db"


def test_CaseineClozeQuestion(Tests):

    exos = amutils.Exo.GetList()
    exo = random.choice(exos)

    print("Create a new Question q")
    q = caseine.CaseineQuestion()
    print(q._fields)
    assert q.exo is None

    q.exo = exo
    assert q.type is None
    assert q.status == 'to export'
    assert q.questiontxt == ''
    assert q.figures == []

    with pytest.raises(sqlite3.IntegrityError):
        q.Save()

    q.type = 'cloze'
    q.questiontxt = MotAleatoire()

    print("Save and reload t")
    q.Save()

    # assert q.__dict__ == q2.__dict__

    figure1 = caseine.CaseineFigure({
        'name': MotAleatoire(),
        'code': MotAleatoire()
    })
    figure2 = caseine.CaseineFigure({
        'name': MotAleatoire(),
        'code': MotAleatoire()
    })
    figures = [figure1, figure2]
    q.figures = figures
    q.Save()

    q3 = caseine.CaseineQuestion.FromExo(exo, forceReload=True)

    print('Teste l\'enregistrement des figures')
    for i in range(2):
        for k in figures[i]._fields.keys():
            print(k)
            assert q3.figures[i].__getattr__(k) == q.figures[i].__getattr__(k)