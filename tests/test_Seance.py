#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

import pytest
from tests import Tests

import sqlite3
import random
from amutils import DBObject, Exo, Feuille, Seance, NB_EXOS_PER_SEANCE


DBTEST_FILE = "amutils/tests/amtest.db"


def test_Seance(Tests):

    print("Create new Seance s")
    s = Seance()
    assert s.id is None
    assert s.feuille is None
    assert s.date is None
    assert s.position is None

    s.position = random.randint(0, 100)
    with pytest.raises(sqlite3.IntegrityError):
        s.Save()

    s.position = None
    s.feuille = random.choice(Feuille.GetList())
    with pytest.raises(sqlite3.IntegrityError):
        s.Save()

    s.position = random.randint(0, 100)
    s.Save()

    id = s.id
    assert s.id
    assert s.exos == [None for _ in range(NB_EXOS_PER_SEANCE)]

    # Test de l'enregistrement des exos
    for i in range(NB_EXOS_PER_SEANCE):
        if random.choice([True, True, False]):
            s.SetExo(i, random.choice(Exo.GetList()))
            print("set s.exos[{}] = {}".format(i, s.GetExo(i)))
            s.SetScore(i, random.random())
            print("set s.scores[{}] = {}".format(i, s.scores[i]))
    s.Save()

    print("Save and reload s")
    s2 = Seance.FromId(id, forceReload=True)
    # avant de tester les variables pour charger s.exos
    assert s.exos == s2.exos
    assert s.scores == s2.scores
    assert s.__dict__ == s2.__dict__

    # Test de la suppressinon d'un exo
    i = random.randint(0, NB_EXOS_PER_SEANCE-1)
    print("Remove s.exos[{}]".format(i))
    s.RemoveExo(i)
    print("Save and reload s")
    s.Save()
    del Seance._list[id]
    s3 = Seance.FromId(id)
    assert s3.GetExo(i) is None
    assert s3.scores[i] is None
    assert s.exos == s3.exos
    assert s.scores == s3.scores
    assert s.__dict__ == s3.__dict__

    print("Delete s")
    s.Delete()
    assert Seance.FromId(id) is None
    vars = (id,)
    row = DBObject.connexion.execute(
                "SELECT COUNT(*) FROM lienExosSeances WHERE seanceId=?",
                vars).fetchone()
    assert row[0] == 0

    print("Clear Seance.list")
    Seance.Clear()
    assert "list" not in Seance.__dict__
