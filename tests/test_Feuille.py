#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

import pytest
from tests import Tests

import amutils
import random
import sqlite3


DBTEST_FILE = "amutils/tests/amtesf.db"


def test_Feuille(Tests):

    print("Create a new Feuille f")
    f = amutils.Feuille()
    assert f.id is None
    assert f.classe is None
    assert f.position is None

    f.position = random.randint(0, 100)
    with pytest.raises(sqlite3.IntegrityError):
        f.Save()

    f.position = None
    f.classe = random.choice(amutils.Classe.GetList())
    with pytest.raises(sqlite3.IntegrityError):
        f.Save()

    f.position = random.randint(0, 100)
    print("Save and reload f")
    f.Save()
    id = f.id
    assert f.id
    f2 = amutils.Feuille.FromId(id, forceReload=True)
    assert f.__dict__ == f2.__dict__

    print("Delete f")
    f.Delete()
    assert amutils.Feuille.FromId(id) is None
    vars = (id,)
    row = amutils.DBObject.connexion.execute(
                "SELECT COUNT(*) FROM seances WHERE feuilleId=?",
                vars).fetchone()
    assert row[0] == 0

    amutils.Feuille.Clear()
    assert "list" not in amutils.Feuille.__dict__
