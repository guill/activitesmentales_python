#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

import pytest
from tests import Tests, MotAleatoire
from amutils import DBObject, Exo, Theme
import sqlite3
import random


DBTEST_FILE = "amutils/tests/amtest.db"


def test_Theme(Tests):

    print("Create a new Theme t")
    t = Theme()
    assert t.id is None
    assert t.label == ''
    assert t.position is None
    assert t.parent is None

    with pytest.raises(sqlite3.IntegrityError):
        t.Save()

    t.label = """ \t
    """
    with pytest.raises(sqlite3.IntegrityError):
        t.Save()

    t.label = None
    with pytest.raises(sqlite3.IntegrityError):
        t.Save()

    t.label = MotAleatoire()
    themes = Theme.GetList()
    t.parent = random.choice(themes)

    print("Save and reload t")
    t.Save()
    id = t.id
    assert t.id
    t.__dict__.copy()
    t2 = Theme.FromId(id, forceReload=True)
    assert t.__dict__ == t2.__dict__

    print("Add a Exos to t")
    exo = random.choice(Exo.GetList())
    themes = exo.themes
    themes.append(t)
    exo.themes = themes
    exo.Save()
    print("Delete t")
    t.Delete()
    assert Theme.FromId(id) is None
    vars = (id,)
    row = DBObject.connexion.execute(
                "SELECT COUNT(*) FROM lienExosThemes WHERE themeId=?",
                vars).fetchone()
    assert row[0] == 0

    print("Clear Theme.list")
    Theme.Clear()
    assert "list" not in Theme.__dict__
