#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

from tests import Tests, MotAleatoire

import amutils
from amutils import do
import random


DBTEST_FILE = "amutils/tests/amtest.db"

# TODO: test marginal cases : empty seance, 1st and last position,...

def test_SeanceAdd(Tests):
    controller = amutils.Controller()

    exos = amutils.Exo.GetList()
    feuilles = amutils.Feuille.GetList()
    seance = amutils.Seance()

    exosSeance = [None]*amutils.NB_EXOS_PER_SEANCE
    for ind in range(amutils.NB_EXOS_PER_SEANCE-1):
        exo = random.choice(exos)
        seance.SetExo(ind, exo)
        exosSeance[ind] = exo
    print('Create a new seance with exos:', exosSeance)

    feuille = random.choice(feuilles)
    feuilleId = feuille.id
    nSeances = len(feuille.seances)
    position = random.randint(0, len(feuille.seances))
    print('*** Add the seance to the Feuille {}, at the position {}'
          .format(feuille.id, position))
    controller.DoSeanceAction(
        do.ID_SEANCE_ADD,
        seance=seance,
        feuilleDest=feuille,
        positionDest=position)
    feuille = amutils.Feuille.FromId(feuilleId, forceReload=True)
    assert len(feuille.seances) == nSeances + 1
    seance = feuille.seances[position]
    for i in range(amutils.NB_EXOS_PER_SEANCE-1):
        assert seance.exos[i] == exosSeance[i]
    ind = 0
    for s in feuille.seances:
        assert s.position == ind
        ind += 1

    print('*** Undo (Remove {})'.format(seance))
    controller.Undo()
    feuille = amutils.Feuille.FromId(feuilleId, forceReload=True)
    for s in feuille.seances:
        assert s != seance
    ind = 0
    for s in feuille.seances:
        assert s.position == ind
        ind += 1

    print('*** Redo')
    controller.Redo()
    feuille = amutils.Feuille.FromId(feuilleId, forceReload=True)
    seance = feuille.seances[position]
    for ind in range(amutils.NB_EXOS_PER_SEANCE-1):
        assert seance.exos[ind] == exosSeance[ind]
    ind = 0
    for seance in feuille.seances:
        assert seance.position == ind
        ind += 1

def test_SeanceRemove(Tests):
    controller = amutils.Controller()

    seances = amutils.Seance.GetList()
    seance = random.choice(seances)
    exosSeance = seance.exos.copy()
    print('Suppression des scores de', seance)
    for ind in range(amutils.NB_EXOS_PER_SEANCE):
        seance.SetScore(ind, None)
    seance.Save()
    feuille = seance.feuille
    feuilleId = feuille.id
    position = seance.position
    nSeances = len(feuille.seances)
    print('*** Delete {} from {}, at the position {}'
          .format(seance, feuille, position))
    controller.DoSeanceAction(do.ID_SEANCE_REMOVE, seance=seance)
    feuille = amutils.Feuille.FromId(feuilleId, forceReload=True)
    assert len(feuille.seances) == nSeances - 1
    for s in feuille.seances:
        assert s != seance
    ind = 0
    for s in feuille.seances:
        assert s.position == ind
        ind += 1

    print('*** Undo')
    controller.Undo()
    feuille = amutils.Feuille.FromId(feuilleId, forceReload=True)
    assert len(feuille.seances) == nSeances
    seance = feuille.seances[position]
    for ind in range(amutils.NB_EXOS_PER_SEANCE-1):
        assert seance.exos[ind] == exosSeance[ind]
    ind = 0
    for s in feuille.seances:
        assert s.position == ind
        ind += 1

    print('*** Redo')
    controller.Redo()
    feuille = amutils.Feuille.FromId(feuilleId, forceReload=True)
    assert len(feuille.seances) == nSeances - 1
    for s in feuille.seances:
        assert s != seance
    r = 0
    for s in feuille.seances:
        assert s.position == r
        r += 1

def test_SeanceEdit(Tests):
    controller = amutils.Controller()
    seances = amutils.Seance.GetList()
    seance = random.choice(seances)
    seanceId = seance.id
    exos = amutils.Exo.GetList()
    exos1 = seance.exos
    exos2 = [random.choice(exos)
             for _ in range(amutils.NB_EXOS_PER_SEANCE)]
    scores1 = seance.scores
    scores2 = [None] * amutils.NB_EXOS_PER_SEANCE
    controller.DoSeanceAction(action=do.ID_SEANCE_EDIT,
                              seance=seance,
                              scores=scores2)
    seance = amutils.Seance.FromId(seanceId, forceReload=True)                                       
    for score in seance.scores:
        assert score is None
    controller.DoSeanceAction(action=do.ID_SEANCE_EDIT,
                              seance=seance,
                              exos=exos2)
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    for position, exo in enumerate(seance.exos):
        assert exo == exos2[position]

    controller.Undo()
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    for position, exo in enumerate(seance.exos):
        assert exo == exos1[position]

    controller.Undo()
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    for position, score in enumerate(seance.scores):
        assert score == scores1[position]
