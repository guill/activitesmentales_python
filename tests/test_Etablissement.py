#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

import pytest
from tests import Tests, MotAleatoire

import amutils
import random
import sqlite3


DBTEST_FILE = "amutils/tests/amtesf.db"


def test_Etablissement(Tests):

    print("Create a new Etablissement e")
    e = amutils.Etablissement()
    print('label="{}" '.format(e.label))
    assert e.id is None
    assert e.label == ""
    assert e.position is None

    with pytest.raises(sqlite3.IntegrityError):
        e.Save()

    e.Set('label', """ \t
    """)
    with pytest.raises(sqlite3.IntegrityError):
        e.Save()

    print("Save and reload e")
    e.label = MotAleatoire()
    e.Save()

    e.position = random.randint(0, 100)
    e.Save()

    id = e.id
    assert e.id is not None

    e2 = amutils.Etablissement.FromId(id, forceReload=True)
    assert e.__dict__ == e2.__dict__

    print("Add a niveau and try to delete e")
    niveau = random.choice(amutils.Niveau.GetList())
    print("niveau.id=", niveau.id)
    oldE = niveau.etablissement
    niveau.etablissement = e
    niveau.Save()
    with pytest.raises(sqlite3.IntegrityError):
        e.Delete()

    print("Remove the niveau and delete e")
    niveau.etablissement = oldE
    print("niveau.id=", niveau.id)
    niveau.Save()
    e.Delete()
    assert amutils.Etablissement.FromId(id) is None

    print("Clear Etablissement._list")
    amutils.Etablissement.Clear()
    assert "_list" not in amutils.Etablissement.__dict__

    print("Test GetNiveaux()")
    e3 = random.choice(amutils.Etablissement.GetList())
    for n in e3.niveaux:
        assert n.__class__.__name__ == "Niveau"
