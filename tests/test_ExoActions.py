#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

import pytest
from tests import Tests, MotAleatoire

import amutils
from amutils import do
import random
import sqlite3


DBTEST_FILE = "amutils/tests/amtest.db"

def test_ExoNew(Tests):
    controller = amutils.Controller()

    code = MotAleatoire()
    position = random.randint(0, 5000)
    exo = amutils.Exo({'code': code, 'position': position})
    controller.DoExoAction(do.ID_EXO_NEW, exo=exo)

    exoId = exo.id
    assert exoId is not None

    controller.Undo()
    assert amutils.Exo.FromId(exoId) is None

    controller.Redo()
    assert amutils.Exo.FromId(exoId) == exo

def test_ExoDelete(Tests):
    controller = amutils.Controller()
    exos = amutils.Exo.GetList()
    exo = exos[random.randint(0, len(exos)-1)]
    if exo.IsUsed():
        with pytest.raises(sqlite3.IntegrityError):
            controller.DoExoAction(do.ID_EXO_DELETE, exo=exo)

def test_ExoEdit(Tests):
    controller = amutils.Controller()
    exos = amutils.Exo.GetList()
    exo = exos[random.randint(0, len(exos)-1)]
    exoId = exo.id

    code1 = exo.code
    position1 = exo.position
    niveaux1 = exo.niveaux
    themes1 = exo.themes

    themes = amutils.Theme.GetList()
    niveaux = amutils.Niveau.GetList()
    code2 = MotAleatoire()
    position2 = random.randint(0, 5000)
    niveaux2 = [niveaux[random.randint(0, len(niveaux)-1)]
                for _ in range(random.randint(1, 5))]
    themes2 = [themes[random.randint(0, len(themes)-1)]
               for _ in range(random.randint(1, 5))]
    print('old values: code:{} ; position:{} ; niveaux:{} ; themes:{}'
          .format(code1, position1, niveaux1, themes1))
    print('new values: code:{} ; position:{} ; niveaux:{} ; themes:{}'
          .format(code2, position2, niveaux2, themes2))
    controller.DoExoAction(do.ID_EXO_EDIT,
                           exo=exo,
                           code=code2,
                           niveaux=niveaux2,
                           themes=themes2)
    exo = amutils.Exo.FromId(exoId, forceReload=True)
    assert exo.code == code2
    assert exo.position == position1
    assert exo.niveaux == niveaux2
    assert exo.themes == themes2

    controller.Undo()
    exo = amutils.Exo.FromId(exoId, forceReload=True)
    assert exo.code == code1
    assert exo.position == position1
    assert exo.niveaux == niveaux1
    assert exo.themes == themes1

    controller.Redo()
    exo = amutils.Exo.FromId(exoId, forceReload=True)
    assert exo.code == code2
    assert exo.position == position1
    assert exo.niveaux == niveaux2
    assert exo.themes == themes2

def test_ExoAdd(Tests):
    controller = amutils.Controller()

    print('** Ajout d\'un exo **')
    seances = amutils.Seance.GetList()
    seance = seances[random.randint(0, len(seances)-1)]
    seanceId = seance.id
    position = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    exos = amutils.Exo.GetList()
    exo = exos[random.randint(0, len(exos)-1)]
    exoId = exo.id
    print('seanceId:{} ; exoposition:{} ; exoId:{}'.format(
        seanceId, position, exoId))
    seance.SetExo(position, None, None)
    controller.DoExoAction(
        do.ID_EXO_ADD,
        exo=exo,
        seanceDest=seance,
        positionDest=position)
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    assert seance.exos[position].id == exoId

    print('** Undo **')
    controller.Undo()
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    assert seance.exos[position] is None

    print('** Redo **')
    controller.Redo()
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    assert seance.exos[position].id == exoId

def test_ExoRemove(Tests):
    controller = amutils.Controller()

    print('** Suppression d\'un exo **')
    seances = amutils.Seance.GetList()
    seance = seances[random.randint(0, len(seances)-1)]
    while seance.IsEmpty():
        seance = seances[random.randint(0, len(seances)-1)]
    seanceId = seance.id
    position = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    while seance.exos[position] is None:
        position = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    exo = seance.exos[position]
    exoId = exo.id
    print('seanceId:{} ; exoPosition:{} ; exoId:{}'.format(
        seanceId, position, exoId))
    seance.SetScore(position, 0.2)
    with pytest.raises(AssertionError):
        controller.DoExoAction(
            do.ID_EXO_REMOVE,
            seanceDest=seance,
            positionDest=position)
    seance.SetScore(position, None)
    controller.DoExoAction(
        do.ID_EXO_REMOVE,
        seanceDest=seance,
        positionDest=position)
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    assert seance.exos[position] is None

    print('** Undo **')
    controller.Undo()
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    assert seance.exos[position].id == exoId

    print('** Redo **')
    controller.Redo()
    seance = amutils.Seance.FromId(seanceId, forceReload=True)
    assert seance.exos[position] is None

def test_ExoMove(Tests):
    # TODO : test with seanceDest = seanceSource
    controller = amutils.Controller()
    seances = amutils.Seance.GetList()

    seanceDest = seances[random.randint(0, len(seances)-1)]
    seanceDestId = seanceDest.id
    positionDest = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    seanceDest.SetScore(positionDest, 0)
    seanceSource = seances[random.randint(0, len(seances)-1)]
    while seanceSource.IsEmpty():
        seanceSource = seances[random.randint(0, len(seances)-1)]
    seanceSourceId = seanceSource.id
    positionSource = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    while seanceSource.exos[positionSource] is None:
        positionSource = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    exo = seanceSource.exos[positionSource]
    exoId = exo.id
    exoDest = seanceDest.exos[positionDest]
    print('seanceSourceId:{} ; exoSourcePosition:{} ;'
          + ' seanceDestId:{} ; exoPositionDest:{} ; exoId:{}'.format(
              seanceSourceId, positionSource, seanceDestId, positionDest, exoId))

    seanceSource.SetScore(positionSource, 0)
    with pytest.raises(AssertionError):
        controller.DoExoAction(
            do.ID_EXO_MOVE,
            seanceSource=seanceSource,
            positionSource=positionSource,
            seanceDest=seanceDest,
            positionDest=positionDest)
    if seanceSource != seanceDest or positionSource != positionDest:
        seanceDest.SetScore(positionDest, None)
        with pytest.raises(AssertionError):
            controller.DoExoAction(
                do.ID_EXO_MOVE,
                seanceSource=seanceSource,
                positionSource=positionSource,
                seanceDest=seanceDest,
                positionDest=positionDest)

        seanceDest.SetScore(positionDest, 0)
        seanceSource.SetScore(positionSource, None)
        with pytest.raises(AssertionError):
            controller.DoExoAction(
                do.ID_EXO_MOVE,
                seanceSource=seanceSource,
                positionSource=positionSource,
                seanceDest=seanceDest,
                positionDest=positionDest)

    seanceDest.SetScore(positionDest, None)
    controller.DoExoAction(
        do.ID_EXO_MOVE,
        seanceSource=seanceSource,
        positionSource=positionSource,
        seanceDest=seanceDest,
        positionDest=positionDest)
    seanceSource = amutils.Seance.FromId(seanceSourceId, forceReload=True)
    seanceDest = amutils.Seance.FromId(seanceDestId, forceReload=True)
    if seanceSource != seanceDest or positionSource != positionDest:
        assert seanceSource.exos[positionSource] is None
    assert seanceDest.exos[positionDest] == exo

    print('** Undo **')
    controller.Undo()
    seanceSource = amutils.Seance.FromId(seanceSourceId, forceReload=True)
    seanceDest = amutils.Seance.FromId(seanceDestId, forceReload=True)
    assert seanceSource.exos[positionSource] == exo
    assert seanceDest.exos[positionDest] == exoDest

    print('** Redo **')
    controller.Redo()
    seanceSource = amutils.Seance.FromId(seanceSourceId, forceReload=True)
    seanceDest = amutils.Seance.FromId(seanceDestId, forceReload=True)
    assert seanceSource.exos[positionSource] is None
    assert seanceDest.exos[positionDest] == exo

def test_ExoSwap(Tests):
    controller = amutils.Controller()
    seances = amutils.Seance.GetList()

    seanceSource = seances[random.randint(0, len(seances)-1)]
    while seanceSource.IsEmpty():
        seanceSource = seances[random.randint(0, len(seances)-1)]
    seanceSourceId = seanceSource.id
    positionSource = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    while seanceSource.exos[positionSource] is None:
        positionSource = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    seanceDest = seances[random.randint(0, len(seances)-1)]
    while seanceDest.IsEmpty():
        seanceDest = seances[random.randint(0, len(seances)-1)]
    seanceDestId = seanceDest.id
    positionDest = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    while seanceDest.exos[positionDest] is None:
        positionDest = random.randint(0, amutils.NB_EXOS_PER_SEANCE-1)
    exo1 = seanceSource.exos[positionSource]
    exo2 = seanceDest.exos[positionDest]
    print('seanceSourceId:{} ; exoSourcePosition:{} ;'
          + ' seanceDestId:{} ; exoPositionDest:{}'.format(
              seanceSourceId, positionSource, seanceDestId, positionDest))

    seanceSource.SetScore(positionSource, 0)
    with pytest.raises(AssertionError):
        controller.DoExoAction(
            action=do.ID_EXO_SWAP,
            seanceSource=seanceSource,
            positionSource=positionSource,
            seanceDest=seanceDest,
            positionDest=positionDest)

    if seanceDest != seanceSource or positionDest != positionSource:
        seanceDest.SetScore(positionDest, None)
        with pytest.raises(AssertionError):
            controller.DoExoAction(
                action=do.ID_EXO_SWAP,
                seanceSource=seanceSource,
                positionSource=positionSource,
                seanceDest=seanceDest,
                positionDest=positionDest)

        seanceDest.SetScore(positionDest, 0)
        seanceSource.SetScore(positionSource, None)
        with pytest.raises(AssertionError):
            controller.DoExoAction(
                action=do.ID_EXO_SWAP,
                seanceSource=seanceSource,
                positionSource=positionSource,
                seanceDest=seanceDest,
                positionDest=positionDest)

    seanceDest.SetScore(positionDest, None)
    controller.DoExoAction(
        action=do.ID_EXO_SWAP,
        seanceSource=seanceSource,
        positionSource=positionSource,
        seanceDest=seanceDest,
        positionDest=positionDest)
    seanceSource = amutils.Seance.FromId(seanceSourceId, forceReload=True)
    seanceDest = amutils.Seance.FromId(seanceDestId, forceReload=True)
    assert seanceSource.exos[positionSource] == exo2
    assert seanceDest.exos[positionDest] == exo1

    print('** Undo **')
    controller.Undo()
    seanceSource = amutils.Seance.FromId(seanceSourceId, forceReload=True)
    seanceDest = amutils.Seance.FromId(seanceDestId, forceReload=True)
    assert seanceSource.exos[positionSource] == exo1
    assert seanceDest.exos[positionDest] == exo2

    print('** Redo **')
    controller.Redo()
    seanceSource = amutils.Seance.FromId(seanceSourceId, forceReload=True)
    seanceDest = amutils.Seance.FromId(seanceDestId, forceReload=True)
    assert seanceSource.exos[positionSource] == exo2
    assert seanceDest.exos[positionDest] == exo1
