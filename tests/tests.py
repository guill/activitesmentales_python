#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:57:24 2020

@author: guill
"""
import pytest
import random
import shutil
from amutils import DBObject, LatexModel, UserConfig

DB = "am.db"
DBTEST_FILE = "tests/amtest.db"
DEFAULT_CONF_FILE = "activitesMentales.conf"
CONF_FILE_TEST = "tests/test.conf"


@pytest.yield_fixture()
def Tests():
    shutil.copyfile(DB, DBTEST_FILE)
    shutil.copyfile(DEFAULT_CONF_FILE, CONF_FILE_TEST)
    DBObject.Open(DBTEST_FILE)
    userConfig = UserConfig(CONF_FILE_TEST)
    LatexModel.userConfig = userConfig

    yield()

    DBObject().Close()


def MotAleatoire():
    len = random.randint(1, 50)
    mot = ""
    for _ in range(len):
        mot += chr(random.randint(32, 126))
    return mot
