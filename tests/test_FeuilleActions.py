#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:35:30 2020

@author: guill
"""

from tests import Tests, MotAleatoire

import amutils
from amutils import do
import random


DBTEST_FILE = "amutils/tests/amtest.db"

# TODO: test marginal cases : empty seance, 1st and last position,...

def test_FeuilleAdd(Tests):
    controller = amutils.Controller()

    feuilles = amutils.Feuille.GetList()
    feuille = random.choice(feuilles)
    classes = amutils.Classe.GetList()
    classe = random.choice(classes)
    classeId = classe.id
    position = random.randint(0, classe.feuillesCount)
    nFeuilles = classe.feuillesCount
    print('*** Add {} to {} at pos {})'
          .format(feuille, classe, position))
    controller.DoFeuilleAction(action=do.ID_FEUILLE_ADD,
                               feuille=feuille.Copy(),
                               classe=classe,
                               position=position)
    classe = amutils.Classe.FromId(classeId, forceReload=True)
    assert classe.feuillesCount == nFeuilles + 1
    for iS, s in enumerate(classe.feuilles[position].seances):
        assert s.id != feuille.seances[iS].id
        for i in range(amutils.NB_EXOS_PER_SEANCE):
            assert s.GetExo(i) == feuille.seances[iS].GetExo(i)

    print('*** Undo')
    controller.Undo()
    classe = amutils.Classe.FromId(classeId, forceReload=True)
    assert classe.feuillesCount == nFeuilles 

def test_FeuilleRemove(Tests):
    controller = amutils.Controller()

    feuilles = amutils.Feuille.GetList()
    feuille = random.choice(feuilles)
    classe = feuille.classe
    classeId = classe.id
    nFeuilles = classe.feuillesCount
    seances = feuille.seances.copy()
    print('Suppression des scores des seances')
    exos = [seance.exos.copy() for seance in seances]
    for seance in seances:
        for position in range(amutils.NB_EXOS_PER_SEANCE):
            seance.SetScore(position, None)
        seance.Save()
    position = feuille.position
    print('*** Delete {} from {} (position {})'
          .format(feuille, classe, feuille.position))
    controller.DoFeuilleAction(
        action=do.ID_FEUILLE_REMOVE,
        feuille=feuille)
    classe = amutils.Classe.FromId(classeId, forceReload=True)
    if nFeuilles > 1:
        assert len(classe.feuilles) == nFeuilles - 1
    for f in classe.feuilles:
        assert f != feuille
    r = 0
    for f in classe.feuilles:
        assert f.position == r
        r += 1

    print('*** Undo')
    controller.Undo()
    classe = amutils.Classe.FromId(classeId, forceReload=True)
    assert len(classe.feuilles) == nFeuilles
    assert classe.feuilles[position] == feuille
    r = 0
    for f in classe.feuilles:
        assert f.position == r
        r += 1
    for iS, seance in enumerate(feuille.seances):
        for iE, exo in enumerate(seance.exos):
            assert exos[iS][iE] == exo

    print('*** Redo')
    controller.Redo()
    classe = amutils.Classe.FromId(classeId, forceReload=True)
    if nFeuilles > 1:
        assert len(classe.feuilles) == nFeuilles - 1
    for f in classe.feuilles:
        assert f != feuille
    r = 0
    for f in classe.feuilles:
        assert f.position == r
        r += 1
