#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 20:59:08 2020

@author: guill
"""

import configparser
from amutils import DBObject


class UserConfig:
    def __init__(self, fileName):
        self.fileName = fileName
        self.config = configparser.ConfigParser()
        self.config.read(fileName)

    def GetValue(self, section, key):
        return self.config[section][key]

    def SetValue(self, section, key, val):
        self.config[section][key] = val

    def Save(self):
        with open(self.fileName, 'w') as configfile:
            self.config.write(configfile)


class SystemConfig:
    def __init__(self):
        self.configs = {}
        for row in DBObject.connexion.execute(
                "SELECT key, val, type FROM confisg"):
            if row['type'] == "integer":
                self.config[row['key']] = int(row['val'])
            elif row['type'] == "decimal":
                self.config[row['key']] = float(row['val'])
            else:
                self.config[row['key']] = row['val']

    def GetValue(self, key):
        return self.config[key]
