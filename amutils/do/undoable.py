#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  9 11:11:41 2020

@author: guill
"""

import abc
import amutils


class UndoList:

    def __init__(self, eventHandler, maxSize):
        self._eventHandler = eventHandler
        self._maxSize = maxSize
        self._queue = []
        self._position = 0

    @property
    def undoableMsg(self):
        return self._queue[self._position-1].label

    @property
    def redoableMsg(self):
        return self._queue[self._position].label

    @property
    def isUndoable(self):
        return self._position > 0

    @property
    def isRedoable(self):
        return self._position < len(self._queue)

    def Add(self, action):
        self._queue = self._queue[:self._position]
        self._queue.append(action)
        self.Redo()

    def Undo(self):
        assert self._position > 0
        self._position -= 1
        action = self._queue[self._position]
        action.Undo(self._eventHandler)
        if self._eventHandler is not None:
            if self._position > 0:
                self._eventHandler.undoBtn.SetToolTip(
                    'Annuler :\n' + self._queue[self._position-1].label)
            else:
                self._eventHandler.undoBtn.SetToolTip('')
                self._eventHandler.undoBtn.Disable()
            self._eventHandler.redoBtn.Enable()
            self._eventHandler.redoBtn.SetToolTip(
                'Refaire :\n' + self._queue[self._position].label)

    def Redo(self):
        assert self._position < len(self._queue)
        action = self._queue[self._position]
        self._position += 1
        action.Redo(self._eventHandler)
        if self._eventHandler is not None:
            self._eventHandler.undoBtn.Enable()
            self._eventHandler.undoBtn.SetToolTip(
                'Annuler :\n' + self._queue[self._position-1].label)
            if self._position < len(self._queue):
                self._eventHandler.redoBtn.SetToolTip(
                    'Refaire :\n' + self._queue[self._position].label)
            else:
                self._eventHandler.redoBtn.SetToolTip('')
                self._eventHandler.redoBtn.Disable()


class UndoableAction(abc.ABC):
    id = 0

    def __init__(self):
        self._done = False
        self.id = UndoableAction.id
        UndoableAction.id += 1
        self.label = ""

    @abc.abstractmethod
    def Do(self, eventHandler):
        pass

    def Undo(self, eventHandler):
        self.Do(eventHandler)

    def Redo(self, eventHandler):
        self.Do(eventHandler)

    def __repr__(self):
        return '{}_{}'.format(self.__class__.__name__, self.id)


class UndoableMultipleAction(UndoableAction):

    def __init__(self, actions=None, label=""):
        super().__init__()
        if actions is None:
            actions = []
        self._actions = actions
        self.label = label

    def Do(self, eventHandler):
        assert False

    @amutils.debug
    def Undo(self, eventHandler):
        assert self._done
        for action in reversed(self._actions):
            action.Undo(eventHandler)
        self._done = False

    @amutils.debug
    def Redo(self, eventHandler):
        assert not self._done
        for action in self._actions:
            action.Redo(eventHandler)
        self._done = True
    