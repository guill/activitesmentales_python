#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 12:04:45 2020

@author: user
"""

import amutils
from amutils import do
import wx
import wx.lib.newevent as NE

ID_FEUILLE_ADD = wx.Window.NewControlId()
ID_FEUILLE_COPY = wx.Window.NewControlId()
ID_FEUILLE_REMOVE = wx.Window.NewControlId()
ID_FEUILLE_PASTE = wx.Window.NewControlId()

FeuilleChangedEvent, EVT_FEUILLE_CHANGED = NE.NewCommandEvent()
FeuilleDeletedEvent, EVT_FEUILLE_DELETED = NE.NewCommandEvent()
FeuilleAddedEvent, EVT_FEUILLE_ADDED = NE.NewCommandEvent()


class UndoableFeuilleEditAction(do.UndoableAction):
    @amutils.debug
    def __init__(self, feuille=None, classe=None, position=None):
        super().__init__()
        self._feuille = feuille
        self._classe = classe
        self._position = position

    @amutils.debug
    def Do(self, eventHandler):
        classeTmp = self._feuille.classe
        self._feuille.classe = self._classe
        self._classe = classeTmp
        positionTmp = self._feuille.position
        self._feuille.position = self._position
        self._position = positionTmp
        self._feuille.Save()
        self._done = not self._done
        if eventHandler is not None:
            event = FeuilleChangedEvent(wx.ID_ANY, feuille=self._feuille)
            wx.PostEvent(eventHandler, event)


class UndoableFeuilleMoveAction(do.UndoableMultipleAction):
    @amutils.debug
    def __init__(self, action, feuille=None, classe=None, position=None):
        super().__init__()
        if action == do.ID_FEUILLE_ADD:
            assert feuille is not None \
                   and classe is not None \
                   and position is not None
            for feuilleCurr in classe.feuilles:
                if feuilleCurr.position >= position:
                    self._actions.append(UndoableFeuilleEditAction(
                        feuille=feuilleCurr,
                        classe=classe,
                        position=feuilleCurr.position + 1))
            self._actions.append(
                UndoableFeuilleAddAction(feuille, classe, position))
            self.label = 'Ajouter une feuille'
        elif action == ID_FEUILLE_REMOVE:
            assert feuille is not None
            classe = feuille.classe
            position = feuille.position
            for seance in feuille.seances:
                self._actions.append(do.UndoableSeanceMoveAction(
                    action=do.ID_SEANCE_REMOVE,
                    seance=seance))
            self._actions.append(_UndoableFeuilleDelAction(feuille=feuille))
            for feuilleCurr in classe.feuilles:
                if feuilleCurr.position > position:
                    self._actions.append(UndoableFeuilleEditAction(
                        feuille=feuilleCurr,
                        classe=classe,
                        position=feuilleCurr.position - 1))
            self.label = 'Supprimer une feuille'
        else:
            assert False


class _UndoableFeuilleAddDelAction(do.UndoableAction):
    @amutils.debug
    def __init__(self, action, feuille=None, classe=None, position=None):
        super().__init__()
        self.action = action
        if action == ID_FEUILLE_ADD:
            self._feuille = feuille
            self._classe = classe
            self._position = position
        elif action == ID_FEUILLE_REMOVE:
            self._feuille = feuille
            self._classe = feuille.classe
            self._position = feuille.position
        self.label = 'Ajouter une feuille'

    @amutils.debug
    def Do(self, eventHandler):
        if self.action == ID_FEUILLE_ADD and not self._done \
           or self.action == ID_FEUILLE_REMOVE and self._done:
            self._feuille.classe = self._classe
            self._feuille.position = self._position
            self._feuille.Save()
            event = FeuilleAddedEvent(wx.ID_ANY, feuille=self._feuille)
        else:
            self._feuille.Delete()
            event = FeuilleDeletedEvent(wx.ID_ANY, feuille=self._feuille)
        self._done = not self._done
        if eventHandler is not None:
            wx.PostEvent(eventHandler, event)


class UndoableFeuilleAddAction(_UndoableFeuilleAddDelAction):
    @amutils.debug
    def __init__(self, feuille, classe, position):
        super().__init__(action=ID_FEUILLE_ADD,
                         feuille=feuille,
                         classe=classe,
                         position=position)


class _UndoableFeuilleDelAction(_UndoableFeuilleAddDelAction):
    @amutils.debug
    def __init__(self, feuille):
        super().__init__(action=ID_FEUILLE_REMOVE, feuille=feuille)
