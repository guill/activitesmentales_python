#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 12:09:16 2020

@author: user
"""

import amutils
from amutils import do
import wx
import wx.lib.newevent as NE

ID_EXO_ADD = wx.Window.NewControlId()
ID_EXO_COPY = wx.Window.NewControlId()
ID_EXO_DELETE = wx.Window.NewControlId()
ID_EXO_DUPLICATE = wx.Window.NewControlId()
ID_EXO_EDIT = wx.Window.NewControlId()
ID_EXO_MOVE = wx.Window.NewControlId()
ID_EXO_NEW = wx.Window.NewControlId()
ID_EXO_PASTE = wx.Window.NewControlId()
ID_EXO_REMOVE = wx.Window.NewControlId()
ID_EXO_SWAP = wx.Window.NewControlId()

ExoLeftDownEvent, EVT_EXO_LDOWN = NE.NewCommandEvent()
ExoRightDownEvent, EVT_EXO_RDOWN = NE.NewCommandEvent()
ExoRightUpEvent, EVT_EXO_RUP = NE.NewCommandEvent()
ExoDoubleClickedEvent, EVT_EXO_DCLICK = NE.NewCommandEvent()
ExoDraggingEvent, EVT_EXO_DRAGGING = NE.NewCommandEvent()
ExoNewEvent, EVT_EXO_NEW = NE.NewCommandEvent()
ExoChangedEvent, EVT_EXO_CHANGED = NE.NewCommandEvent()
ExoDeletedEvent, EVT_EXO_DELETED = NE.NewCommandEvent()


class __UndoableExoNewDelAction(do.UndoableAction):
    @amutils.debug
    def __init__(self, action, exo):
        super().__init__()
        self.action = action
        self._exo = exo

    @amutils.debug
    def Do(self, eventHandler):
        if self.action == ID_EXO_NEW and not self._done \
           or self.action == ID_EXO_DELETE and self._done:
            self._exo.Save()
            # self._exo = amutils.Exo.FromId(self._exo.id)
            event = ExoChangedEvent(wx.ID_ANY, exo=self._exo)
        else:
            # Load informations before removing of the database
            self._exo.themes
            self._exo.niveaux
            self._exo.Delete()
            event = ExoDeletedEvent(wx.ID_ANY, exo=self._exo)
        self._done = not self._done
        if eventHandler is not None:
            wx.PostEvent(eventHandler, event)


class UndoableExoNewAction(__UndoableExoNewDelAction):
    @amutils.debug
    def __init__(self, exo):
        super().__init__(ID_EXO_NEW, exo)


class UndoableExoDelAction(__UndoableExoNewDelAction):
    @amutils.debug
    def __init__(self, exo):
        super().__init__(ID_EXO_DELETE, exo)


class UndoableExoEditAction(do.UndoableAction):
    @amutils.debug
    def __init__(self, exo, code, position, niveaux, themes, updateDate):
        super().__init__()
        self._exo = exo
        self._code = code
        self._position = position
        self._niveaux = niveaux
        self._themes = themes
        self._updateDate = updateDate
        self.label = "Modification de la question {}"\
                     .format(self._exo.id)

    @amutils.debug
    def Do(self, eventHandler):
        tmp = self._exo.code
        self._exo.code = self._code
        self._code = tmp
        tmp = self._exo.position
        self._exo.position = self._position
        self._position = tmp
        tmp = self._exo.niveaux
        self._exo.niveaux = self._niveaux
        self._niveaux = tmp
        tmp = self._exo.themes
        self._exo.themes = self._themes
        self._themes = tmp
        self._exo.updateDate, self._updateDate = self._updateDate, self._exo.updateDate
        self._exo.Save()
        # emit exochanged
        self._done = not self._done
        if eventHandler is not None:
            event = ExoChangedEvent(wx.ID_ANY, exo=self._exo)
            wx.PostEvent(eventHandler, event)


class UndoableExoMoveAction(do.UndoableMultipleAction):
    @amutils.debug
    def __init__(self, action, exo=None,
                 seanceSource=None, positionSource=None,
                 seanceDest=None, positionDest=None):
        super().__init__()
        if seanceDest is not None \
           and seanceDest.feuille.id is None:
            self._actions.append(do.UndoableFeuilleMoveAction(
                action=do.ID_FEUILLE_ADD,
                feuille=seanceDest.feuille,
                classe=seanceDest.feuille.classe,
                position=seanceDest.feuille.position))
        if seanceDest is not None and seanceDest.id is None:
            self._actions.append(do.UndoableSeanceMoveAction(
                action=do.ID_SEANCE_ADD,
                seance=seanceDest,
                feuilleDest=seanceDest.feuille,
                positionDest=seanceDest.position))
        if action == do.ID_EXO_ADD:
            assert seanceDest.scores[positionDest] is None
            self.label = "Ajout de question"
            exos = seanceDest.exos
            exos[positionDest] = exo
            self._actions.append(do.UndoableSeanceEditAction(seance=seanceDest,
                                                             exos=exos))
        elif action == do.ID_EXO_MOVE:
            assert seanceSource.scores[positionSource] is None
            assert seanceDest.scores[positionDest] is None
            self.label = "Déplacement de question"
            exos = seanceSource.exos
            exo = exos[positionSource]
            exos[positionSource] = None
            self._actions.append(do.UndoableSeanceEditAction(
                seance=seanceSource,
                exos=exos))
            if seanceDest != seanceSource:
                exos = seanceDest.exos
            exos[positionDest] = exo
            self._actions.append(do.UndoableSeanceEditAction(seance=seanceDest,
                                                             exos=exos))
        elif action == do.ID_EXO_REMOVE:
            self.label = "Suppression de question"
            assert seanceDest.scores[positionDest] is None
            exos = seanceDest.exos
            exos[positionDest] = None
            self._actions.append(do.UndoableSeanceEditAction(
                seance=seanceDest,
                exos=exos))
        elif action == do.ID_EXO_SWAP:
            assert seanceSource.scores[positionSource] is None
            assert seanceDest.scores[positionDest] is None
            self.label = "Échange de question"
            exosS = seanceSource.exos
            exosD = seanceDest.exos
            exosS[positionSource], exosD[positionDest] = \
                exosD[positionDest], exosS[positionSource]
            self._actions.append(do.UndoableSeanceEditAction(
                seance=seanceSource,
                exos=exosS))
            self._actions.append(do.UndoableSeanceEditAction(
                seance=seanceDest,
                exos=exosD))
