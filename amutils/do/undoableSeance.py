#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 12:12:09 2020

@author: user
"""

import amutils
from amutils import do
import wx
import wx.lib.newevent as NE

ID_SEANCE_ADD = wx.Window.NewControlId()
ID_SEANCE_COPY = wx.Window.NewControlId()
ID_SEANCE_REMOVE = wx.Window.NewControlId()
ID_SEANCE_PASTE = wx.Window.NewControlId()
ID_SEANCE_EDIT = wx.Window.NewControlId()

SeanceChangedEvent, EVT_SEANCE_CHANGED = NE.NewCommandEvent()
SeanceNewEvent, EVT_SEANCE_NEW = NE.NewCommandEvent()
SeanceDeletedEvent, EVT_SEANCE_DELETED = NE.NewCommandEvent()


class UndoableSeanceEditAction(do.UndoableAction):
    @amutils.debug
    def __init__(self, seance, feuille=None, position=None,
                 date=None, exos=None, scores=None, desc=''):
        super().__init__()
        assert seance is not None
        self._seance = seance
        self._feuille = feuille
        self._position = position
        self._date = date
        self._exos = exos
        self._scores = scores
        self.label = desc
        self._changedExos = []
        for i, exo in enumerate(seance.exos):
            if (exos is not None and exo != exos[i]) \
               or (scores is not None and seance.scores[i] != scores[i]):
               if exo is not None and exo not in self._changedExos:
                    self._changedExos.append(exo)
               if scores is not None and scores[i] not in self._changedExos:
                   self._changedExos.append(seance.exos[i])

    @amutils.debug
    def Do(self, eventHandler):
        if self._feuille is not None:
            self._feuille, self._seance.feuille = \
                self._seance.feuille, self._feuille
            self._position, self._seance.position = self._seance.position, self._position
        self._date, self._seance.date = self._seance.date, self._date
        if self._exos is not None:
            self._exos, self._seance.exos = self._seance.exos, self._exos
        if self._scores is not None:
            self._scores, self._seance.scores = \
                self._seance.scores, self._scores
        self._seance.Save()
        self._done = not self._done
        if eventHandler is not None:
            event = SeanceChangedEvent(wx.ID_ANY,
                                       seance=self._seance,
                                       exos=self._changedExos)
            wx.PostEvent(eventHandler, event)


class UndoableSeanceMoveAction(do.UndoableMultipleAction):
    @amutils.debug
    def __init__(self, action, seance=None,
                 feuilleSource=None, positionSource=None,
                 feuilleDest=None, positionDest=None):
        super().__init__()
        if action == do.ID_SEANCE_ADD:
            for seanceCurr in feuilleDest.seances:
                if seanceCurr.position >= positionDest:
                    self._actions.append(UndoableSeanceEditAction(
                        seance=seanceCurr,
                        feuille=feuilleDest,
                        position=seanceCurr.position+1))
            self._actions.append(UndoableSeanceAddAction(
                seance=seance, feuille=feuilleDest, position=positionDest))
            self.label = 'Ajouter une séance'
        elif action == do.ID_SEANCE_REMOVE:
            assert seance is not None
            feuilleDest = seance.feuille
            positionDest = seance.position
            assert not seance.IsEvaluated()
            exos = [None] * amutils.NB_EXOS_PER_SEANCE
            self._actions.append(UndoableSeanceEditAction(
                exos=exos,
                seance=seance,
                feuille=seance.feuille,
                position=seance.position))
            self._actions.append(UndoableSeanceDelAction(seance))
            for seanceCurr in feuilleDest.seances:
                if seanceCurr.position > positionDest:
                    self._actions.append(UndoableSeanceEditAction(
                        seance=seanceCurr,
                        feuille=feuilleDest,
                        position=seanceCurr.position-1))
            self.label = 'Supprimer une séance'


class _UndoableSeanceAddDelAction(do.UndoableAction):
    def __init__(self, action, seance=None, feuille=None, position=None):
        super().__init__()
        self.action = action
        self.feuille = feuille
        self.position = position
        self._changedExos = []
        for exo in seance.exos:
            if exo is not None:
                self._changedExos.append(exo)
        if action == ID_SEANCE_ADD:
            assert seance is not None
            self.seance = seance
        elif action == ID_SEANCE_REMOVE:
            self.seance = seance
            self.feuille = seance.feuille
            self.position = seance.position
        else:
            assert False
        self.label = 'Ajouter une séance'

    @amutils.debug
    def Do(self, eventHandler):
        if self.action == ID_SEANCE_ADD and not self._done \
           or self.action == ID_SEANCE_REMOVE and self._done:
            self.seance.feuille = self.feuille
            self.seance.position = self.position
            self.seance.Save()
            event = SeanceNewEvent(
                wx.ID_ANY,
                seance=self.seance,
                exos=self._changedExos)
        else:
            self.seance.Delete()
            event = SeanceDeletedEvent(
                wx.ID_ANY,
                seance=self.seance,
                exos=self._changedExos)
        self.feuille.ReloadSeances()
        self._done = not self._done
        if eventHandler is not None:
            wx.PostEvent(eventHandler, event)


class UndoableSeanceAddAction(_UndoableSeanceAddDelAction):
    @amutils.debug
    def __init__(self, seance, feuille, position):
        super().__init__(action=ID_SEANCE_ADD,
                         seance=seance,
                         feuille=feuille,
                         position=position)


class UndoableSeanceDelAction(_UndoableSeanceAddDelAction):
    @amutils.debug
    def __init__(self, seance):
        super().__init__(action=ID_SEANCE_REMOVE, seance=seance)

