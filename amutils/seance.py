#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 15:01:10 2020

@author: guill
"""

# TODO: option : le nombre d'exos par seance

import amutils

NB_EXOS_PER_SEANCE = 5


class Seance(amutils.DBObject):
    table = "seances"

    def __init__(self, data=None):
        super().__init__(data)
        self._fields['exos'] = None
        self._fields['scores'] = None

    def Copy(self):
        newSeance = super().Copy()
        newSeance.exos = self.exos
        newSeance.scores = [None for _ in range(NB_EXOS_PER_SEANCE)]
        return newSeance

    def __contains__(self, val):
        for exo in self.exos:
            if exo == val:
                return True
        return False

    def _getattr(self, attr):
        if attr == 'classe':
            return self.feuille.classe
        elif attr == 'exos':
            return self.__GetExosScores().copy()
        elif attr == 'scores':
            self.__GetExosScores()
            return self._fields['scores'].copy()
        else:
            return super()._getattr(attr)

    def GetExo(self, position):
        return self.__GetExosScores()[position]

    def SetExo(self, position, exo, score=None):
        self.__GetExosScores()
        self._fields['exos'][position] = exo
        self._fields['scores'][position] = score
        self._dirty = True

    def RemoveExo(self, position):
        self.__GetExosScores()
        self._fields['exos'][position] = None
        self._fields['scores'][position] = None
        self._dirty = True

    def SetScore(self, position, score):
        self.__GetExosScores()
        self._fields['scores'][position] = score
        self._dirty = True

    def IsEmpty(self):
        return self.__GetExosScores() ==\
            [None for _ in range(NB_EXOS_PER_SEANCE)]

    def IsEvaluated(self):
        """
        Returns
        -------
        boolean
            Returns True if at least one score is not None for this seance.

        """
        self.__GetExosScores()
        return self.scores != [None for _ in range(NB_EXOS_PER_SEANCE)]

    def IsFirst(self):
        """

        Returns
        -------
        boolean
            Returns True if this seance is the first of its Feuille.
        """
        return self.position == 0

    def IsLast(self):
        """

        Returns
        -------
        boolean
            Returns True if this seance is the last of its Feuille.
        """
        return self.position == len(self.feuille.seances) - 1

    @amutils.debug
    def Delete(self, commit=True):
        vars = (self.id,)
        amutils.DBObject.connexion.execute(
            'DELETE FROM "lienExosSeances" WHERE "seanceId"=?;', vars)
        super().Delete(commit=False)
        if commit:
            amutils.DBObject.connexion.commit()
        self._dirty = True

    @amutils.debug
    def Save(self, commit=True):
        super().Save(commit=False)
        self.__GetExosScores()
        vars = (self.id,)
        amutils.DBObject.connexion.execute(
            'DELETE FROM "lienExosSeances" WHERE "seanceId"=?;', vars)
        for i, exo in enumerate(self.exos):
            if exo is not None:
                vars = (self.id,
                        i,
                        exo.id,
                        self.scores[i])
                sql = """INSERT INTO "lienExosSeances"
                      ("seanceId", "position", "exoId", "score")
                      VALUES (?, ?, ?, ?);"""
                if amutils.DEBUG_SQL:
                    print(sql, vars)
                amutils.DBObject.connexion.execute(sql, vars)
        if commit:
            amutils.DBObject.connexion.commit()
        self._dirty = False

    def __GetExosScores(self):
        if self._fields['exos'] is None or self._fields['scores'] is None:
            exos = [None for _ in range(NB_EXOS_PER_SEANCE)]
            scores = [None for _ in range(NB_EXOS_PER_SEANCE)]
            vars = (self.id,)
            for row in amutils.DBObject.connexion.execute(
                    """SELECT "exoId", "position", "score"
                       FROM "lienExosSeances"
                       WHERE "seanceId"=?
                       ORDER BY "position";""",
                    vars):
                exos[row['position']] = amutils.Exo.FromId(row['exoId'])
                scores[row['position']] = row['score']
            if self._fields['exos'] is None:
                self._fields['exos'] = exos
            if self._fields['scores'] is None:
                self._fields['scores'] = scores
        return self._fields['exos']
