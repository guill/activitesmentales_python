#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 23:55:34 2020

@author: guill
"""


import amutils


class Etablissement(amutils.DBObject):
    table = "etablissements"

    def __init__(self, row=None):
        super().__init__(row)
        self.__niveaux = None

    @property
    def niveaux(self):
        """
        Returns the list of all the Niveaux for this etablissement.
        """
        if self.__niveaux is None:
            self.__niveaux = []
            vars = (self.id,)
            for row in amutils.DBObject.connexion.execute(
                        """SELECT "id" FROM "{}"
                        WHERE "etablissementId"=?
                            ORDER BY "position";""".format(amutils.Niveau.table),
                        vars):
                self.__niveaux.append(amutils.Niveau.FromId(row['id']))
        return self.__niveaux.copy()
