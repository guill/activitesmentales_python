#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 23:33:12 2020

@author: GuiΩ
"""

import amutils
from amutils import ui
import wx


class ConfigThemesPanel(ui.AbstractConfigThemesPanel):
    def __init__(self, *args, **argz):
        super().__init__(*args, **argz)
        self.themesTree.SetFilter(None)
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnSelectionChanged)
        self.Bind(wx.EVT_TREE_END_LABEL_EDIT, self.OnEndLabelEdit)
        self.Bind(wx.EVT_TREE_BEGIN_DRAG, self.OnBeginDrag)
        self.Bind(wx.EVT_TREE_END_DRAG, self.OnEndDrag)
        self.Layout()

    def Validate(self):
        ids = [0]
        rootItem = self.themesTree.GetRootItem()
        self.__ValidateSubThemes(0, rootItem, ids)
        for theme in amutils.Theme.GetList():
            if theme.Get('id') not in ids:
                theme.Delete()

    def Cancel(self):
        amutils.Theme.Clear()

    def OnBeginDrag(self, event):
        if self.themesTree.GetSelection() != self.themesTree.GetRootItem():
            event.Allow()

    # TODO: mark as dirty the item beetween the old and the new pos
    # if the new pos is before the old one
    def OnEndDrag(self, event):
        item, flags = self.themesTree.HitTest(event.GetPoint())
        parent = self.themesTree.GetItemParent(item)
        it, cookie = self.themesTree.GetFirstChild(parent)
        pos = 0
        while it != item:
            it, cookie = self.themesTree.GetNextChild(parent, cookie)
            pos += 1
        self.themesTree.MoveTheme(self.themesTree.GetSelection(), parent, pos)
        # print(self.themesTree.GetItemData(item)['theme'].label)
        # print(self.themesTree.GetItemData(self.themesTree.GetSelection())['theme'].label)

    def OnSubthemeAddClick(self, event):
        dialog = wx.TextEntryDialog(self, 'Nom', 'Nouveau Sous-Thème')
        if dialog.ShowModal() == wx.ID_OK:
            self.themesTree.AddSubTheme(dialog.GetValue())

    def OnThemeRenameClick(self, event):
        self.themesTree.EditLabel(self.themesTree.GetSelection())

    def OnEndLabelEdit(self, event):
        item = self.themesTree.GetSelection()
        self.themesTree.GetItemData(item)['theme'].label = event.GetLabel()

    def OnThemeRemoveClick(self, event):
        item = self.themesTree.GetSelection()
        self.themesTree.Delete(item)

    def OnSelectionChanged(self, event):
        theme = self.themesTree.GetItemData(
            self.themesTree.GetSelection())['theme']
        themes = theme.descendants
        themes.append(theme)
        self.themeRemoveBtn.Enable(
            theme is None or len(amutils.Exo.GetSelection(themes=themes)) == 0)
        self.themeRenameBtn.Enable(theme is None or theme.Get('id') != 0)

    def __ValidateSubThemes(self, parentId, treeItem, ids):
        position = 0
        (item, cookie) = self.themesTree.GetFirstChild(treeItem)
        while item.IsOk():
            theme = self.themesTree.GetItemData(item)['theme']
            if theme is None:
                theme = amutils.Theme()
            theme.Set('parent', amutils.Theme.FromId(parentId))
            theme.Set('label', self.themesTree.GetItemText(item))
            theme.Set('position', position)
            theme.Save()
            position += 1
            ids.append(theme.Get('id'))
            self.__ValidateSubThemes(theme.Get('id'), item, ids)
            (item, cookie) = self.themesTree.GetNextChild(treeItem, cookie)
