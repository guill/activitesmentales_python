#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 20:16:27 2020

@author: guill
"""

import amutils
from amutils import do
from amutils import ui
import datetime
import subprocess
import wx

if amutils.EXPORT_CASEINE:
    from amutils import caseine

# TODO : Logs


class AMFrame(ui.AMFrameAbstract):
    userConfig = None

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        ui.SeancesList.amFrame = self
        self.caseineImportDir = './'
        self.caseineExportDir = './'

        # TODO: user config for undolist size
        self.controller = amutils.Controller(self)
        self.__set_properties()
        # self.etablissements = amutils.Etablissement.GetList(True)
        # self.themeRoot = amutils.Theme.GetRoot()
        self.__UpdateAnnee()
        self.anneeCtrl.Select(0)
        self.__UpdateClassesList()
        self.Bind(wx.EVT_TREE_SEL_CHANGED,
                  self.OnNiveauxTreeSelChanged,
                  self.niveauxTree)
        self.Bind(wx.EVT_TREE_SEL_CHANGED,
                  self.OnThemesTreeSelectionChanged,
                  self.themesTree)
        self.Bind(do.EVT_EXO_CHANGED, self.OnExoEvent)
        self.Bind(do.EVT_EXO_DELETED, self.OnExoEvent)
        self.Bind(do.EVT_FEUILLE_ADDED, self.OnFeuilleEvent)
        self.Bind(do.EVT_FEUILLE_DELETED, self.OnFeuilleEvent)
        self.Bind(do.EVT_SEANCE_CHANGED, self.OnSeanceChanged)
        self.Bind(do.EVT_SEANCE_DELETED, self.OnSeanceChanged)
        self.Bind(do.EVT_SEANCE_NEW, self.OnSeanceChanged)
        self.Bind(wx.EVT_COMBOBOX, self.OnAnneeChanged, self.anneeCtrl)
        self.Bind(wx.EVT_COMBOBOX, self.OnClasseChanged, self.classeCtrl)
        self.Bind(do.EVT_EXO_DCLICK, self.OnExoDClick)
        self.Bind(wx.EVT_MENU, self.OnMenuClick)

    def __set_properties(self):
        self.themesTree.SetFilter(None)
        self.seancesList.controller = self.controller
        self.exosList.controller = self.controller
        self.exosList.SetActionHandler(self)
        self.mainSplitterPane.SetSashPosition(450)
        self.selectionSplitterPane.SetSashPosition(200)
        if amutils.EXPORT_CASEINE:
            self.caseineExportButton.Show()

    def EditExo(self, exo):
        if exo.IsUsed():
            dialog = wx.MessageDialog(
                self,
                """Cette question est déjà utilisée.
Les modifications seront visibles dans toutes les séances où elle est utilisée.
Cela devrait se limiter à des corrections.
Sinon il est préférable de créer une nouvelle question""",
                style=wx.OK | wx.CANCEL)
            if dialog.ShowModal() == wx.ID_CANCEL:
                return False
        exoDialog = ui.ExoDialog(self, self.controller)
        return exoDialog.ShowModal(exo) == wx.ID_OK

    @property
    def selectedClasse(self):
        item = self.classeCtrl.GetSelection()
        if item != wx.NOT_FOUND and self.classeCtrl.HasClientData():
            return self.classeCtrl.GetClientData(item)['classe']
        return None

    @property
    def selectedNiveaux(self):
        return self.niveauxTree.GetSelection()

    @property
    def selectedSeance(self):
        return self.seancesList.selectedSeance

    @property
    def selectedThemes(self):
        return self.themesTree.GetSelections()

    def UpdateExoList(self):
        """
        Update the exos list from the niveaux and themes selected

        Returns
        -------
        None.
        """
        self.exosList.SetFilter(
            self.selectedNiveaux if len(self.selectedNiveaux) > 0 else None,
            self.selectedThemes,
            self.selectedClasse)

    def ShowSeances(self, seances):
        i = 1
        for seance in seances:
            seance.RemoveExo(i)
            i += 1
            self.seancesList.Add(seance)

    def FeuilleDelete(self, feuille):
        self.controller.DoFeuilleAction(
            action=do.ID_FEUILLE_DELETE,
            feuille=feuille)

    def FeuilleNew(self):
        feuille = amutils.Feuille()
        self.controller.DoFeuilleAction(
            action=do.ID_FEUILLE_ADD,
            feuille=feuille,
            classe=self.selectedClasse,
            position=self.selectedClasse.feuillesCount)

    def OnAnneeChanged(self, event):
        self.__UpdateClassesList()
        self.UpdateExoList()

    def OnCaseineExportClick(self, event):
        with wx.FileDialog(self,
                           "Export vers Caseine",
                            defaultDir=self.caseineExportDir,
                           defaultFile='ExportCaseine_{}.xml'.format(
                               datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')),
                           wildcard="Fichiers XML (*.xml)|*.xml",
                           style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT
                           ) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_OK:
                # save the current contents in the file
                self.caseineExportDir = fileDialog.GetDirectory()
                pathname = fileDialog.GetPath()
                try:
                    with open(pathname, 'w') as file:
                        file.write(caseine.CaseineQuestion.ToXML())
                except IOError as err:
                    wx.LogError("Cannot save current data in file '{}'.\n{}".format( pathname, err))
        with wx.FileDialog(self,
                           "Import de Caseine",
                           defaultDir=self.caseineImportDir,
                           wildcard="Fichiers XML (*.xml)|*.xml",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
                           ) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_OK:
                print(fileDialog.GetPath())
                caseine.CaseineQuestion.FromXML(fileDialog.GetPath(), self)


    def OnClasseChanged(self, event):
        self.__UpdateSeancesList()
        self.UpdateExoList()

    def OnConfigBtnClick(self, event):
        configDialog = ui.ConfigDialog(self, self.userConfig)
        configDialog.Show()

    def OnEvaluateClick(self, event):
        ui.ScoresDialog.userConfig = self.userConfig
        scoresDialog = ui.ScoresDialog(
            self, self.controller, self.selectedSeance)
        scoresDialog.ShowModal()
        scoresDialog.Destroy()

    def OnExoDClick(self, event):
        self.EditExo(event.exo)

    def OnExoEvent(self, event):
        wx.PostEvent(self.exosList, event)
        wx.PostEvent(self.seancesList, event)

    def OnFeuilleAddBtnClick(self, event):
        self.FeuilleNew()

    def OnFeuilleEvent(self, event):
        wx.PostEvent(self.seancesList, event)

    def OnMenuClick(self, event):
        if event.GetId() == wx.ID_EDIT:
            self.EditExo(self.exosList.selection)

    def OnNiveauxTreeSelChanged(self, event):
        self.themesTree.SetFilter(self.niveauxTree.GetSelection())
        self.UpdateExoList()

    def OnPdfBtnClick(self, event):
        model = amutils.LatexModel.FromId(1)
        selectedSeance = self.selectedSeance
        if selectedSeance is None:
            print("no seance selected")
            return
        feuille = selectedSeance.feuille
        if model.CompileModel(feuille=feuille):
            msgD = wx.MessageBox("Les fichiers suivants ont été créés :\n" +
                                 "\n".join(model.GetOutfiles()))
            for file in model.GetOutfiles():
                subprocess.check_output(
                    "{} '{}'"
                    .format(self.userConfig.GetValue('Paths', 'pdfViewer'),
                            file),
                    stderr=subprocess.STDOUT,
                    shell=True)
        else:
            msgD = ui.LongMessageDialog(self)
            msgD.SetContent("Sortie de la commande", model.GetOutput())
            # TODO: Scroll to the end of the content
            msgD.ShowModal()
            msgD.Destroy()

    def OnSeanceChanged(self, event):
        wx.PostEvent(self.exosList, event)
        wx.PostEvent(self.seancesList, event)
        # TODO :
        #self.seancesList.SetSelection(event.seance, positions=event.positions)

    def OnThemesTreeSelectionChanged(self, event):
        self.UpdateExoList()

    def OnUndoClick(self, event):
        self.controller.Undo()

    def OnRedoClick(self, event):
        self.controller.Redo()

    # TODO: update when classes are updated, added
    def __UpdateAnnee(self):
        self.anneeCtrl.Clear()
        for year in amutils.Classe.GetAnnees():
            self.anneeCtrl.Append(str(year))
        self.toolbar.Layout()

    def __UpdateClassesList(self):
        self.classeCtrl.Clear()
        annee = int(self.anneeCtrl.GetString(self.anneeCtrl.GetSelection()))
        for classe in amutils.Classe.GetListByYear(annee):
            self.classeCtrl.Append(classe.label, {'classe': classe})
        if self.classeCtrl.GetCount() > 0:
            self.classeCtrl.SetSelection(0)
        self.__UpdateSeancesList()
        self.toolbar.Layout()

    def __UpdateSeancesList(self):
        """
        Update the seances in seancesList according to the selected classe

        Returns
        -------
        None.
        """
        self.seancesList.classe = self.selectedClasse
        self.controller.amFrame.pdfBtn.Enable(False)
        self.controller.amFrame.evaluateBtn.Enable(False)

    def __SetSubThemes(self, parentItem, parentTheme):
        niveaux = self.selectedNiveaux
        for theme in parentTheme.children:
            if len(niveaux) == 0 or theme.ContainsNiveaux(niveaux):
                item = self.themesTree.AppendItem(
                    parentItem,
                    theme.label,
                    data={'theme': theme})
                self.__SetSubThemes(item, theme)

    def Info(self, msg):
        self.statusbar.SetStatusText(msg, 0)
