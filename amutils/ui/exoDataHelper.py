#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 06:22:31 2020

@author: GuiΩ
"""

import amutils
import wx
import pickle


class ExoDataHelper:
    def __init__(self, exo=None, seance=None, position=None, dataObject=None):
        self.exo = exo
        self.seance = seance
        self.position = position
        if dataObject is not None:
            self.SetDataObject(dataObject)

    def GetDataObject(self):
        exoId = None
        if self.exo is not None:
            exoId = self.exo.Get('id')
        seanceId = None
        if self.seance is not None:
            seanceId = self.seance.Get('id')
        data = pickle.dumps({'exoId': exoId,
                             'seanceId': seanceId,
                             'position': self.position})
        dataObject = wx.CustomDataObject('Exo')
        dataObject.SetData(data)
        return dataObject

    def SetDataObject(self, dataObject):
        data = pickle.loads(dataObject.GetData())
        self.exo = amutils.Exo.FromId(data['exoId'])
        self.seance = None
        if data['seanceId'] is not None:
            self.seance = amutils.Seance.FromId(data['seanceId'])
        self.position = data['position']

    def GetExo(self):
        return self.exo

    def GetSeance(self):
        return self.seance

    def GetPosition(self):
        return self.position
