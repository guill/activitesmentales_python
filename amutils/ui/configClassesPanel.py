#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 09:05:20 2020

@author: GuiΩ
"""
import amutils
from amutils import do
from amutils import ui
from datetime import datetime
import wx


class ConfigClassesPanel(ui.AbstractConfigClassesPanel):

    def __init__(self, *args, **argz):
        super().__init__(*args, **argz)
        self.classeDialog = None
        self.anneeSpin.SetValue(datetime.now().year)
        self.__UpdateList()
        self.Bind(wx.EVT_LISTBOX, self.OnClasseSelected, self.classesList)
        self.Bind(do.EVT_CLASSE, self.OnClasseEvent)

    def GetSelection(self):
        item = self.classesList.GetSelection()
        return self.classesList.GetClientData(item)['classe']

    def OnAnneeChanged(self, event):
        self.__UpdateList()

    def OnAddClasseBtnClick(self, event):
        classe = amutils.Classe()
        self.__ShowClasseDialog(classe)

    # TODO : Maintenir la selection
    def OnClasseEvent(self, event):
        self.__UpdateList()

    def OnClasseSelected(self, event):
        self.EditClasseBtn.Enable()
        self.DeleteClasseBtn.Enable()
        event.Skip()

    def __ShowClasseDialog(self, classe):
        if not self.classeDialog:
            self.classeDialog = ui.ClasseDialog(self)
        self.classeDialog.Show(classe)

    def __UpdateList(self):
        self.classesList.Clear()
        year = self.anneeSpin.GetValue()
        for classe in amutils.Classe.GetListByYear(year):
            self.classesList.Append(
                classe.Get('label'), clientData={'classe': classe})
        self.EditClasseBtn.Disable()
        self.DeleteClasseBtn.Disable()
