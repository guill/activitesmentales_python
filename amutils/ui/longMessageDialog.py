#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 06:49:23 2020

@author: guill
"""

from amutils.ui import LongMessageDialogAbstract


class LongMessageDialog(LongMessageDialogAbstract):

    def SetContent(self, title="", content=""):
        self.titleLbl.SetLabel(title)
        self.contentLbl.SetLabel(content)
        self.contentPanel.GetSizer().FitInside(self.contentPanel)
        self.SetMinSize((500, 600))
        self.contentPanel.Scroll((0,self.contentPanel.GetClientSize().GetHeight()))
        self.okBtn.SetFocus()
