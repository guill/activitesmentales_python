#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 00:48:19 2020

@author: guill
"""

from amutils import do
from amutils import ui
import wx


class ConfigDialog(ui.AbstractConfigDialog):

    def __init__(self, parent, userConfig, *args, **kwds):
        super().__init__(parent, *args, **kwds)
        self.userConfig = userConfig
        self.notebook.DeleteAllPages()
        self.generalPanel = ui.ConfigGeneralPanel(self.notebook, userConfig)
        self.notebook.AddPage(self.generalPanel, 'Général')
        self.classesPanel = ui.ConfigClassesPanel(self.notebook)
        self.notebook.AddPage(self.classesPanel, 'Classes')
        self.themesPanel = ui.ConfigThemesPanel(self.notebook)
        self.notebook.AddPage(self.themesPanel, 'Thèmes')
        self.modelsPanel = ui.ConfigModelsPanel(self.notebook)
        self.notebook.AddPage(self.modelsPanel, 'Modèles')
        self.Bind(wx.EVT_CLOSE, self.OnCancelClick)
        # To force refreshing
        # self.notebook.ChangeSelection(1)
        # self.notebook.ChangeSelection(0)

    def OnEditClasseBtnClick(self, event):
        self.__ShowClasseDialog(self.__GetSelectedClasse())

    def OnDeleteClasseBtnClick(self, event):
        rep = wx.MessageDialog(
            self,
            "Êtes-vous sûr de vouloir supprimer cette classe ?",
            caption="Attention",
            style=wx.OK | wx.CANCEL | wx.CANCEL_DEFAULT | wx.ICON_QUESTION
            | wx.CENTRE,
            pos=wx.DefaultPosition).ShowModal()
        if rep == wx.ID_OK:
            classe = self.__GetSelectedClasse()
            classe.Delete()
            event = do.ClasseEvent(classe=classe)
            wx.PostEvent(self, event)

    def OnCancelClick(self, event):
        self.themesPanel.Cancel()
        self.Hide()
        self.Destroy()

    def OnValiderBtnClick(self, event):
        self.generalPanel.Validate()
        self.themesPanel.Validate()
        self.modelsPanel.Validate()
        self.Hide()
        self.Destroy()
