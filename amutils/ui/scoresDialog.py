#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 15:40:17 2020

@author: guill
"""

import amutils
from amutils import do
from amutils import ui
import math
import wx


class ScoresDialog(ui.AbstractScoresDialog):
    userConfig = None

    def __init__(self, parent, controller, seance, *args, **kwds):
        super().__init__(parent, *args, **kwds)
        self.controller = controller
        self.seance = seance
        self.scoreCtrls = []
        self.__set_properties()
        self.__do_layout()
        self.scoresPane.SetFocus()
        self.dateCtrl.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)

    def __set_properties(self):
        self.scoreBase = int(self.userConfig.GetValue('Questions',
                                                      'scoreBase'))
        if self.seance.date is not None:
            date = wx.DateTime()
            date.ParseISODate(self.seance.date)
            self.dateCtrl.SetValue(date)
        date = self.dateCtrl.GetValue()
        self.dayTxt.SetLabel(date.GetWeekDayName(date.GetWeekDay()))
        maxBmpH = 0
        for exo in self.seance.exos:
            if exo is not None:
                maxBmpH = max(maxBmpH, exo.thumb.GetHeight())
        self.thumb.SetMinSize((-1, maxBmpH))

    def __do_layout(self):
        exos = self.seance.exos
        scores = self.seance.scores
        scoresSizer = wx.BoxSizer(wx.HORIZONTAL)
        for i in range(len(scores)):
            score = scores[i]
            label = wx.StaticText(self.scoresPane,
                                  wx.ID_ANY,
                                  " score {} :".format(i+1))
            scoresSizer.Add(label, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 0)
            scoreStr = ""
            if score is not None:
                scoreStr = str(round(score * self.scoreBase))
            widget = wx.TextCtrl(self.scoresPane, wx.ID_ANY, scoreStr)
            # widget.SetValidator(ScoreValidator())
            widget.SetMinSize((42, -1))
            widget.position = i
            if exos[i] is None:
                widget.Disable()
            else:
                widget.Bind(wx.EVT_TEXT, self.OnScoreChange, widget)
                widget.Bind(wx.EVT_SET_FOCUS, self.OnScoreCrtlFocused, widget)
            self.scoreCtrls.append(widget)
            scoresSizer.Add(widget, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 3)
            label = wx.StaticText(self.scoresPane,
                                  wx.ID_ANY,
                                  "/{} ".format(self.scoreBase))
            scoresSizer.Add(label, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 0)

        self.scoresPane.MoveBeforeInTabOrder(self.ValiderBtn)
        exos = self.seance.exos
        for i in range(len(exos)):
            if exos[i] is not None:
                self.thumb.SetBitmap(exos[i].thumb)
                break
        self.scoresPane.SetSizer(scoresSizer)
        self.Fit()

    def OnAnnulerClick(self, event):
        self.Hide()

    def OnDateChange(self, event):
        date = self.dateCtrl.GetValue()
        self.dayTxt.SetLabel(date.GetWeekDayName(date.GetWeekDay()))
        event.Skip()

    def OnKeyDown(self, event):
        print('keyDown', event.GetKeyCode())
        if event.GetKeyCode() != wx.WXK_TAB:
            return
        widget = event.GetEventObject()
        if widget == self.dateCtrl:
            if event.GetDirection():
                self.scoresPane.SetFocus()
            else:
                self.ValiderBtn.GetFocus()
        else:
            event.Skip()

    def OnScoreChange(self, event):
        scoreCtrl = event.GetEventObject()
        s = event.GetString()
        self._CheckValues()
        if s == '':
            scoreCtrl.SetBackgroundColour(wx.WHITE)
            return
        if not s.isdigit() or int(s) > self.scoreBase:
            scoreCtrl.SetBackgroundColour(wx.RED)
            scoreCtrl.SetFocus()
        else:
            scoreCtrl.SetBackgroundColour(wx.WHITE)
            if len(scoreCtrl.GetValue()) \
               >= math.ceil(math.log10(self.scoreBase+1)):
                self._SetFocusNext(scoreCtrl)

    def OnNextDateClick(self, event):
        date = self.dateCtrl.GetValue()
        date.Add(wx.DateSpan(days=+1))
        self.dayTxt.SetLabel(date.GetWeekDayName(date.GetWeekDay()))
        self.dateCtrl.SetValue(date)
        self.scoresPane.SetFocus()

    def OnPrevDateClick(self, event):
        date = self.dateCtrl.GetValue()
        date.Add(wx.DateSpan(days=-1))
        self.dayTxt.SetLabel(date.GetWeekDayName(date.GetWeekDay()))
        self.dateCtrl.SetValue(date)
        self.scoresPane.SetFocus()

    def OnScore1Changed(self, event):
        print("Event handler 'OnScore1Changed' not implemented!")
        event.Skip()

    def OnScoreCrtlFocused(self, event):
        for scoreCtrl in self.scoreCtrls:
            if scoreCtrl.HasFocus():
                self.thumb.SetBitmap(
                    self.seance.GetExo(scoreCtrl.position).thumb)
        self.Update()
        event.Skip()

    def OnValiderClick(self, event):
        self.seance.date = self.dateCtrl.GetValue().FormatISODate()
        scoreBase = int(self.userConfig.GetValue('Questions', 'scoreBase'))
        scores = [None] * amutils.NB_EXOS_PER_SEANCE
        for i in range(len(self.scoreCtrls)):
            val = self.scoreCtrls[i].GetValue()
            if val == '':
                scores[i] = None
            else:
                scores[i] = int(val) / scoreBase
        self.controller.DoSeanceAction(
            action=do.ID_SEANCE_EDIT,
            seance=self.seance,
            date=self.dateCtrl.GetValue().FormatISODate(),
            scores=scores,
            desc='Changer les scores')
        self.Hide()

    def _SetFocusNext(self, scoreCtrl):
        position = self.scoreCtrls.index(scoreCtrl) + 1
        while position < len(self.scoreCtrls) \
              and not self.scoreCtrls[position].IsEnabled():
            position += 1
        if position < len(self.scoreCtrls):
            self.scoreCtrls[position].SetFocus()
        else:
            self.ValiderBtn.SetFocus()

    def _CheckValues(self):
        flag = True
        for ctrl in self.scoreCtrls:
            s = ctrl.GetValue()
            if ctrl.IsEnabled() and s != '' \
               and (not s.isdigit() or int(s) > self.scoreBase):
                flag = False
        self.ValiderBtn.Enable(flag)
