#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 10:24:24 2020

@author: guill
"""

import wx
import amutils
from amutils import ui


class ClasseDialog(ui.ClasseDialogAbstract):

    def Show(self, classe):
        self.classe = classe
        self.SetTitle("Modification de la classe {}".format(classe.label))
        self.labelCtrl.SetValue(classe.label)
        self.latexLabelCtrl.SetValue(classe.latexLabel)
        self.__UpdateNiveaux()
        if classe.niveau:
            self.niveauCtrl.SetSelection(
                self.niveauCtrl.FindString(classe.niveau.label, True))
        self.anneeDCtrl.SetValue(classe.anneeDebut)
        if classe.anneeFin:
            self.anneeFCtrl.SetValue(classe.anneeFin)
        else:
            self.anneeFCtrl.SetValue(classe.anneeDebut)
        super().Show()
        # end wxGlade

    def OnAnnulerBtnClick(self, event):
        self.Hide()

    def OnValiderBtnClick(self, event):
        self.classe.label = self.labelCtrl.GetValue()
        self.classe.latexLabel = self.latexLabelCtrl.GetValue()
        self.classe.niveau = self.niveauCtrl.GetClientData(
            self.niveauCtrl.GetSelection())['niveau']
        self.classe.anneeDebut = self.anneeDCtrl.GetValue()
        self.classe.anneeFin = self.anneeFCtrl.GetValue()
        self.classe.Save()
        self.Hide()
        event = amutils.do.ClasseEvent(classe=self.classe)
        wx.PostEvent(self.GetParent(), event)

    def __UpdateNiveaux(self):
        self.niveauCtrl.Clear()
        for niveau in amutils.Niveau.GetList():
            self.niveauCtrl.Append(niveau.label, {'niveau': niveau})
