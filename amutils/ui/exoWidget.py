#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 23:15:43 2020

@author: guill
"""

import amutils
from amutils import caseine
from amutils import do
from PIL import Image, ImageDraw
import wx

SELECTED_COL = wx.Colour(255, 255, 200)
UNSELECTED_COL = wx.Colour(255, 255, 255)  # wx.NullColour for transparancy

EXO_WIDTH = 215
# Size of the score icons
SCORE_SIZE = 14

BORDER_WIDTH = 2



class ExoWidget(wx.Panel):
    scoreBmps = {}

    def __init__(self, parent, exo, classe=None, *args, **kwds):
        """
        Parameters
        ----------
        parent : wx.Window
            DESCRIPTION.
        exo : amutils.Exo
            DESCRIPTION.
        classe : amutils.Classe, optional
            For what classe show the scores. The default is None.
        *args : TYPE
            DESCRIPTION.
        **kwds : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        super().__init__(parent, *args, **kwds)
        self.__exo = exo
        self.__classe = classe
        self.__selected = False
        self.__thumb = None
        self.__set_properties()
        self.__do_layout()

    def __set_properties(self):
        self.SetBackgroundColour(UNSELECTED_COL)

    def __do_layout(self):
        self.sizerV = wx.BoxSizer(wx.VERTICAL)
        if self.classe is not None:
            self.scoresPanel = wx.Panel(self)
            self.scoresPanel.SetBackgroundColour(wx.Colour(220, 220, 220))
            self.scoresSizer = wx.BoxSizer(wx.HORIZONTAL)
            self.UpdateScores()
            self.scoresPanel.SetSizer(self.scoresSizer)
            self.sizerV.Add(self.scoresPanel,
                            1,
                            wx.EXPAND | wx.TOP | wx.LEFT | wx.RIGHT,
                            BORDER_WIDTH)
        self.UpdateThumb()
        self.SetSizer(self.sizerV)

    @property
    def classe(self):
        return self.__classe

    @property
    def exo(self):
        return self.__exo

    @property
    def thumb(self):
        return self.__thumb
#    def __getattr__(self, field):
#        return self.__dict__['__' + field]

    @property
    def selected(self):
        return self.__selected

    @selected.setter
    def selected(self, selected=True):
        if selected:
            self.SetBackgroundColour(SELECTED_COL)
        else:
            self.SetBackgroundColour(UNSELECTED_COL)
        self.__selected = selected

    def UpdateThumb(self):
        if self.__thumb is not None:
            thumbItem = self.sizerV.GetItem(self.__thumb)
            thumbItem.DeleteWindows()
        bmp = wx.Bitmap(self.exo.thumb)
        self.__thumb = wx.StaticBitmap(self, wx.ID_ANY, bmp)
        self.__thumb.Bind(wx.EVT_MOUSE_EVENTS, self.OnMouseEvent)
        self.sizerV.Add(self.__thumb,
                        0,
                        wx.BOTTOM | wx.LEFT | wx.RIGHT,
                        BORDER_WIDTH)
        self.Layout()

    @amutils.debug
    def UpdateScores(self):
        self.scoresSizer.Clear(True)
        for score in self.exo.GetScores(self.classe)['scores']:
            scoreBmp = wx.StaticBitmap(
                self.scoresPanel,
                wx.ID_ANY,
                wx.Bitmap(self.GetScoreBmp(score)))
            self.scoresSizer.Add(scoreBmp, 0, wx.TOP | wx.LEFT | wx.BOTTOM, 2)
        if amutils.EXPORT_CASEINE:
            caseineQ = self.exo.caseineQuestion
            if caseineQ is not None and caseineQ.status != 'not exported':
                self.scoresSizer.Add((0,0), 1, wx.EXPAND)
                txt = wx.StaticText(self.scoresPanel, label='⏳' if caseineQ.status == 'to export' else '✔')
                self.scoresSizer.Add(txt, 0, wx.ALIGN_CENTER_VERTICAL, 3)
        self.Layout()

    def OnMouseEvent(self, event):
        newEvent = True
        if event.GetEventType() == wx.wxEVT_LEFT_DOWN:
            self.dragging = False
            exoEvent = do.ExoLeftDownEvent(wx.ID_ANY, exo=self.exo)
        elif event.GetEventType() == wx.wxEVT_RIGHT_DOWN:
            exoEvent = do.ExoRightDownEvent(wx.ID_ANY, exo=self.exo)
        elif event.GetEventType() == wx.wxEVT_LEFT_DCLICK:
            exoEvent = do.ExoDoubleClickedEvent(wx.ID_ANY, exo=self.exo)
        elif event.GetEventType() == wx.wxEVT_RIGHT_UP:
            exoEvent = do.ExoRightUpEvent(wx.ID_ANY, exo=self.exo)
        elif event.GetEventType() == wx.wxEVT_MOTION:
            event.ResumePropagation(2)
            newEvent = False
        else:
            newEvent = False
        if newEvent:
            exoEvent.SetEventObject(self)
            wx.PostEvent(self.GetEventHandler(), exoEvent)
        event.ResumePropagation(2)
        event.Skip()

    @staticmethod
    def GetScoreBmp(score):
        if score not in ExoWidget.scoreBmps.keys():
            img = Image.new('RGBA',
                            (SCORE_SIZE, SCORE_SIZE),
                            (255, 255, 255, 0))
            draw = ImageDraw.Draw(img)
            if score is None:
                draw.ellipse(xy=(0, 0, SCORE_SIZE-1, SCORE_SIZE-1),
                             fill=(200, 200, 200, 255),
                             outline=(0, 0, 0, 255),
                             width=1)
            else:
                draw.ellipse(xy=(0, 0, SCORE_SIZE-1, SCORE_SIZE-1),
                             fill=(255, 0, 0, 255))
                draw.pieslice(xy=(0, 0, SCORE_SIZE-1, SCORE_SIZE-1),
                              start=-90,
                              end=score*360-90,
                              fill=(0, 255, 0, 255))
                draw.ellipse(xy=(0, 0, SCORE_SIZE-1, SCORE_SIZE-1),
                             outline=(0, 0, 0, 255),
                             width=1)
            ExoWidget.scoreBmps[score] = \
                wx.Bitmap.FromBufferRGBA(SCORE_SIZE,
                                         SCORE_SIZE,
                                         img.tobytes())
        return ExoWidget.scoreBmps[score]

    def __repr__(self):
        return '{}_{}'.format(self.__class__.__name__, self.exo.id)
