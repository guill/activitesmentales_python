#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 08:48:50 2020

@author: GuiΩ
"""

from amutils.ui import AbstractConfigGeneralPanel


class ConfigGeneralPanel(AbstractConfigGeneralPanel):

    def __init__(self, parent, userConfig, *args, **kwds):
        super().__init__(parent, *args, **kwds)
        self.userConfig = userConfig
        self.databasePicker.SetPath(
            self.userConfig.GetValue('Paths', 'database'))
        self.thumbsDirPicker.SetPath(
            self.userConfig.GetValue('Paths', 'thumbsDir'))
        self.imagesDirPicker.SetPath(
            self.userConfig.GetValue('Paths', 'imagesDir'))
        self.pdflatexPicker.SetPath(
            self.userConfig.GetValue('Paths', 'pdflatex'))
        self.pdfViewerPicker.SetPath(
            self.userConfig.GetValue('Paths', 'pdfViewer'))
        self.scoreBaseCtrl.SetValue(
            int(self.userConfig.GetValue('Questions', 'scoreBase')))
        self.Layout()

    def Validate(self):
        self.userConfig.SetValue('Paths',
                                 'database',
                                 self.databasePicker.GetPath())
        # Gérer le déplaceent des thumbs
        self.userConfig.SetValue('Paths',
                                 'thumbsDir',
                                 self.thumbsDirPicker.GetPath())
        self.userConfig.SetValue('Paths',
                                 'imagesDir',
                                 self.imagesDirPicker.GetPath())
        self.userConfig.SetValue('Paths',
                                 'pdflatex',
                                 self.pdflatexPicker.GetPath())
        self.userConfig.SetValue('Paths',
                                 'pdfViewer',
                                 self.pdfViewerPicker.GetPath())
        self.userConfig.SetValue('Questions',
                                 'scoreBase',
                                 str(self.scoreBaseCtrl.GetValue()))
        self.userConfig.Save()
