#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 22:18:17 2020

@author: GuiΩ
"""

import amutils
import wx


class ThemesTree(wx.TreeCtrl):

    def __init__(self, *args, **argz):
        super().__init__(*args, **argz)
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnSelectionChanged)

    def SetFilter(self, niveaux):
        if niveaux is None or len(niveaux) == 0:
            themes = amutils.Theme.GetList()
        else:
            themes = amutils.Theme.GetFilteredList(niveaux)
        self._SetThemes(themes)

    def AddSubTheme(self, label):
        self.AppendItem(
            self.GetSelection(), label, data={'theme': None})

    def MoveTheme(self, treeItem, parent, pos):
        self.GetItemData(treeItem)
        newItem = self.InsertItem(
            parent,
            pos,
            self.GetItemText(treeItem),
            data={'theme': self.GetItemData(treeItem)['theme'],
                  'dirty': True})
        self.__CopyItem(treeItem, newItem)
        self.Delete(treeItem)

    def GetSelection(self):
        return self.GetItemData(super().GetSelection())['theme']

    def GetSelections(self):
        selection = []
        for item in super().GetSelections():
            selection.append(self.GetItemData(item)['theme'])
        return selection

    def OnSelectionChanged(self, event):
        """Empeach parent items to be selected"""
        for sel in super().GetSelections():
            if self.ItemHasChildren(sel):
                self.UnselectItem(sel)
        event.Skip()

    def __CopyItem(self, oldItem, newItem):
        item, cookie = self.GetFirstChild(oldItem)
        while item.IsOk():
            newSubItem = self.AppendItem(
                newItem,
                self.GetItemText(item),
                data={'theme': self.GetItemData(item)['theme']})
            self.__CopyItem(item, newSubItem)
            item, cookie = self.GetNextChild(oldItem, cookie)

    def _SetThemes(self, themes):
        self.DeleteAllItems()
        rootTheme = amutils.Theme.GetRoot()
        rootItem = self.AddRoot("Thèmes",
                                data={'theme': rootTheme})
        self._SetSubThemes(rootItem, rootTheme, themes)

    def _SetSubThemes(self, item, theme, themes):
        for th in theme.children:
            if th in themes:
                it = self.AppendItem(item,
                                     th.label,
                                     data={'theme': th})
                self._SetSubThemes(it, th, themes)
