#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 08:09:25 2020

@author: guill
"""

import wx
import amutils
from amutils import do
from amutils import ui

LIMITS_COLOR = wx.Colour(255, 0xdf, 0xfb)
SELECTED_COLOR = wx.Colour(150, 200, 255)
SELECTED_COLOR = wx.RED
UNSELECTED_COLOR = wx.NullColour
FEUILLE_LIMIT_HEIGHT = 5
FEUILLE_LIMIT_COLOR = wx.BLACK
EMPTY_SEANCE_HEIGHT = 50


class SeanceWidget(wx.Panel):

    def __init__(self, parent, controller, seance, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.controller = controller
        self.seance = seance
        self.__set_properties()
        self.__do_layout()
        self.Update()
        # self.Bind(do.EVT_EANCE_CHANGED, self.OnSeanceChanged)

    def __set_properties(self):
        self.__slots = []
        self.__selected = False
        self.__exoWs = [None for _ in range(amutils.NB_EXOS_PER_SEANCE)]
        self.SetMinSize((-1, EMPTY_SEANCE_HEIGHT))

    def __do_layout(self):
        sizerV = wx.BoxSizer(wx.VERTICAL)
        panel = wx.Panel(self)
        sizerV.Add(panel, 1, wx.EXPAND | wx.CENTER | wx.ALL, 1)
        self.SetSizer(sizerV)
        self.sizerH = wx.BoxSizer(wx.HORIZONTAL)
        for i in range(amutils.NB_EXOS_PER_SEANCE):
            if i != 0:
                vsep = wx.StaticLine(panel, wx.ID_ANY, style=wx.LI_VERTICAL)
                vsep.SetBackgroundColour(LIMITS_COLOR)
                self.sizerH.Add(vsep, 0, wx.EXPAND, 0)
            slot = ExoSlot(panel, self.controller, seanceW=self, position=i)
            self.sizerH.Add(slot, 1, wx.EXPAND | wx.TOP, 0)
            self.__slots.append(slot)
        panel.SetSizer(self.sizerH)
        self.Update()
        self.Fit()

    @amutils.debug
    def Update(self):
        exos = self.seance.exos
        for i in range(len(exos)):
            self.__SetExo(exos[i], i)
        if self.seance.IsEmpty():
            self.SetMinSize((-1, EMPTY_SEANCE_HEIGHT))
        else:
            self.SetMinSize((-1, -1))
        self.GetParent().Layout()

    @property
    def feuille(self):
        return self.seance.feuille

    def Getposition(self, exoW):
        for i in range(amutils.NB_EXOS_PER_SEANCE):
            if self.GetSizer().GetItem(2*i).GetSizer() is not None \
               and self.GetSizer().GetItem(2*i) \
               .GetSizer().GetItem(0).GetWindow() == exoW:
                return i
        return None

    # @property
    # def nextSeanceW(self):
    #     sizer = self.GetParent().GetSizer()
    #     position = sizer.GetChildren().index(sizer.GetItem(self))
    #     if position == len(sizer.GetChildren()) - 1:
    #         return None
    #     return sizer.GetChildren()[position + 2].GetWindow()

    def SetSelected(self, positions, selected=True):
        if selected:
            self.SetBackgroundColour(SELECTED_COLOR)
            self.SetFocus()
        else:
            self.SetBackgroundColour(UNSELECTED_COLOR)
        for i in range(len(self.slots)):
            self.__slots[i].selected = (selected and i in positions)
        self.__selected = selected
        self.Layout()

    @property
    def selected(self):
        return self.__selected

    @property
    def slots(self):
        return self.__slots

    def __SetExo(self, exo, position):
        slot = self.__slots[position]
        slot.SetExo(exo, self.seance.classe)

    def __repr__(self):
        return '{}_{}'.format(self.__class__.__name__, self.seance.id)


class ExoDropTarget(wx.DropTarget):
    def __init__(self, controller, exoSlot, *args, **argz):
        super().__init__(*args, **argz)
        self.controller = controller
        self.exoSlot = exoSlot
        self._data = ui.ExoDataHelper().GetDataObject()
        self.SetDataObject(self._data)

    def OnDragOver(self, x, y, defResult):
        if self.exoSlot.score is not None:
            return wx.DragError
        else:
            return defResult

    def OnDrop(self, x, y):
        if self.exoSlot.score is not None:
            return False
        else:
            return True

    def OnData(self, x, y, defResult):
        self.GetData()
        dataHelper = ui.ExoDataHelper(dataObject=self.GetDataObject())
        if self.exoSlot.exo is None:
            if dataHelper.seance is not None \
               and dataHelper.seance.scores[dataHelper.position] \
               is None:
                self.controller.DoExoAction(
                    action=do.ID_EXO_MOVE,
                    exo=dataHelper.exo,
                    seanceSource=dataHelper.seance,
                    positionSource=dataHelper.position,
                    seanceDest=self.exoSlot.seance,
                    positionDest=self.exoSlot.position)
                return defResult
            else:
                self.controller.DoExoAction(
                    action=do.ID_EXO_ADD,
                    exo=dataHelper.exo,
                    seanceDest=self.exoSlot.seance,
                    positionDest=self.exoSlot.position)
                return defResult
        self.exoSlot.PopupMenu(
            DropContextMenu(
                self.controller,
                exo=dataHelper.exo,
                seanceSource=dataHelper.seance,
                positionSource=dataHelper.position,
                seanceDest=self.exoSlot.seance,
                positionDest=self.exoSlot.position))
        return defResult


class ContextMenu(wx.Menu):
    def __init__(self, controller, exoSlot, *args, **argz):
        super().__init__(*args, **argz)
        self.controller = controller
        self.exoSlot = exoSlot
        # SubMenu 'Question'
        menu = wx.Menu()
        menuItem = self.AppendSubMenu(menu, 'Question')
        exoIsNone = exoSlot.exo is None
        menu.Bind(wx.EVT_MENU, self.OnMenuClick)
        menuItem = menu.Append(do.ID_EXO_EDIT, "Modifier")
        menuItem.Enable(not exoIsNone)
        menuItem = menu.Append(do.ID_EXO_COPY, "Copier")
        menuItem.Enable(not exoIsNone)
        menuItem = menu.Append(do.ID_EXO_PASTE, "Coller")
        menuItem.Enable(self.controller.exoBuffer is not None
                        and (exoIsNone or exoSlot.score is None))
        menuItem = menu.Append(do.ID_EXO_REMOVE, "Supprimer")
        menuItem.Enable(not exoIsNone and exoSlot.score is None)
        # SubMenu 'Seance'
        menu = wx.Menu()
        menuItem = self.AppendSubMenu(menu, 'Séance')
        menu.Bind(wx.EVT_MENU, self.OnMenuClick)
        menuItem = menu.Append(do.ID_SEANCE_COPY, "Copier")
        menuItem.Enable(exoSlot.seance.id is not None)
        menuItem = menu.Append(do.ID_SEANCE_PASTE, "Coller")
        menuItem.Enable(self.controller.seanceBuffer is not None)
        menuItem = menu.Append(do.ID_SEANCE_REMOVE, "Supprimer")
        menuItem.Enable(exoSlot.seance.id is not None
                        and not exoSlot.seance.IsEvaluated())
        # SubMenu 'Feuille'
        menu = wx.Menu()
        menu.Bind(wx.EVT_MENU, self.OnMenuClick)
        menuItem = self.AppendSubMenu(menu, 'Feuille')
        menuItem = menu.Append(do.ID_FEUILLE_COPY, "Copier")
        menuItem = menu.Append(do.ID_FEUILLE_PASTE, "Coller")
        menuItem.Enable(self.controller.feuilleBuffer is not None)
        menuItem = menu.Append(do.ID_FEUILLE_REMOVE, "Supprimer")
        menuItem.Enable(not exoSlot.feuille.IsEvaluated())
        self.Append(wx.ID_CANCEL, 'Annuler')
        self.Bind(wx.EVT_MENU, self.OnMenuClick)

    def OnMenuClick(self, event):
        action = event.GetId()
        if action == wx.ID_CANCEL:
            return
        if action == do.ID_EXO_EDIT:
            SeancesList.amFrame.EditExo(self.exoSlot.exo)
        elif action == do.ID_EXO_COPY:
            self.controller.exoBuffer = self.exoSlot.exo
        elif action == do.ID_EXO_PASTE:
            self.controller.DoExoAction(
                do.ID_EXO_ADD,
                exo=self.controller.exoBuffer,
                seanceDest=self.exoSlot.seance,
                positionDest=self.exoSlot.position)
        elif action in (do.ID_EXO_ADD,
                        do.ID_EXO_DELETE,
                        do.ID_EXO_MOVE,
                        do.ID_EXO_NEW,
                        do.ID_EXO_REMOVE,
                        do.ID_EXO_SWAP):
            self.controller.DoExoAction(action=action,
                                        exo=self.exoSlot.exo,
                                        seanceDest=self.exoSlot.seance,
                                        positionDest=self.exoSlot.position)
        elif action == do.ID_SEANCE_COPY:
            self.controller.seanceBuffer = self.exoSlot.seance.Copy()
        elif action == do.ID_SEANCE_PASTE:
            self.controller.DoSeanceAction(
                action=do.ID_SEANCE_ADD,
                seance=self.controller.seanceBuffer,
                feuilleDest=self.exoSlot.feuille,
                positionDest=self.exoSlot.seance.position)
        elif action == do.ID_SEANCE_REMOVE:
            self.controller.DoSeanceAction(action=do.ID_SEANCE_REMOVE,
                                           seance=self.exoSlot.seance)
        elif action == do.ID_FEUILLE_COPY:
            self.controller.feuilleBuffer = self.exoSlot.feuille.Copy()
        elif action == do.ID_FEUILLE_PASTE:
            self.controller.DoFeuilleAction(
                action=do.ID_FEUILLE_ADD,
                feuille=self.controller.feuilleBuffer,
                classe=self.exoSlot.classe,
                position=self.exoSlot.feuille.position)
        elif action == do.ID_FEUILLE_REMOVE:
            self.controller.DoFeuilleAction(action=action,
                                            feuille=self.exoSlot.feuille)
        else:
            assert False, 'action : {}'.format(action)


class DropContextMenu(wx.Menu):
    def __init__(self,
                 controller,
                 exo=None,
                 seanceSource=None,
                 positionSource=None,
                 seanceDest=None,
                 positionDest=None,
                 *args, **argz):
        super().__init__(*args, **argz)
        self.controller = controller
        self.__exo = exo
        self.seanceSource = seanceSource
        self.positionSource = positionSource
        self.seanceDest = seanceDest
        self.positionDest = positionDest
        if seanceSource is not None \
           and seanceSource.GetExo(positionSource) is not None \
           and seanceDest is not None \
           and seanceDest.GetExo(positionDest) is not None:
            menuItem = self.Append(do.ID_EXO_SWAP, "Échanger")
            menuItem.SetBitmap(wx.Bitmap("./amutils/ui/images/swap.png",
                                         wx.BITMAP_TYPE_ANY))
            menuItem.Enable(False)
            if seanceSource.scores[positionSource] is None \
               and seanceDest.scores[positionDest] is None:
                menuItem.Enable()
        menuItem = self.Append(do.ID_EXO_MOVE, "Remplacer")
        menuItem.SetBitmap(wx.Bitmap("./amutils/ui/images/replace.png",
                                     wx.BITMAP_TYPE_ANY))
        if seanceSource is None \
           or seanceSource.scores[positionSource] is not None:
            menuItem.Enable(False)
        menuItem = self.Append(do.ID_EXO_ADD, "Remplacer avec une copie")
        menuItem.SetBitmap(wx.Bitmap(
            "./amutils/ui/images/copy_replace.png",
            wx.BITMAP_TYPE_ANY))
        self.Append(wx.ID_CANCEL)
        self.Bind(wx.EVT_MENU, self.OnMenuClick)

    def OnMenuClick(self, event):
        if event.GetId() == wx.ID_CANCEL:
            return
        self.controller.DoExoAction(
            action=event.GetId(),
            exo=self.__exo,
            seanceSource=self.seanceSource,
            positionSource=self.positionSource,
            seanceDest=self.seanceDest,
            positionDest=self.positionDest)


class ExoSlot(wx.Panel):

    def __init__(self, parent, controller, seanceW, position, *args, **argz):
        super().__init__(parent, *args, **argz)
        self.controller = controller
        self.__exoW = None
        self.__seanceW = seanceW
        self.__position = position
        self.SetMinSize((ui.EXO_WIDTH, -1))
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)
        self.SetDropTarget(ExoDropTarget(controller, self))
        self.SetBackgroundColour(wx.Colour(200, 200, 200))
        self.Bind(wx.EVT_RIGHT_UP, self.OnRightClick)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnClick)
        self.Bind(wx.EVT_RIGHT_DOWN, self.OnClick)

    @property
    def exo(self):
        if self.__exoW is None:
            return None
        return self.__exoW.exo

    def SetExo(self, exo, classe):
        assert classe is not None
        self.sizer.Clear(True)
        if exo is not None:
            self.__exoW = ui.ExoWidget(self,
                                       exo=exo,
                                       classe=classe)
            self.__exoW.dropTarget = ExoDropTarget(self.controller, self)
            self.sizer.Add(self.__exoW)
            self.__exoW.Bind(wx.EVT_MOTION, self.OnMouseMove)
        else:
            self.__exoW = None

    @property
    def exoW(self):
        return self.__exoW

    @property
    def classe(self):
        return self.seance.classe

    @property
    def feuille(self):
        return self.seance.feuille

    @property
    def position(self):
        return self.__position

    @property
    def seance(self):
        return self.seanceW.seance

    @property
    def seanceW(self):
        return self.__seanceW

    @property
    def selected(self):
        return self.__selected

    @selected.setter
    def selected(self, selected=True):
        """
        Show the selection indicator for this slot.
        (No action if the slot is empty)

        Returns
        -------
        None.
        """
        if self.__exoW is not None:
            self.__exoW.selected = selected

    @property
    def score(self):
        return self.__seanceW.seance.scores[self.__position]

    def OnMouseMove(self, event):
        if event.Dragging() and self.__exoW is not None:
            dropSource = wx.DropSource(self)
            dragData = ui.ExoDataHelper(
                exo=self.exo,
                seance=self.seance,
                position=self.__position).GetDataObject()
            dropSource.SetData(dragData)
            self.__result = dropSource.DoDragDrop(True)

    def OnClick(self, event):
        self.__seanceW.GetParent().amFrame.Info(
            'Feuille {} (id {}) ; Seance {} (id {}, feuille {}){}'. format(
                self.feuille.position,
                self.feuille.id,
                self.seance.position,
                self.seance.id,
                self.seance.feuille.id,
                ' ; Question {}'.format(self.exo.id)
                if self.exo is not None else ''))
        self.__seanceW.GetParent().SetSelection(self.seance,
                                                position=self.__position)
        self.controller.amFrame.pdfBtn.Enable()
        self.controller.amFrame.evaluateBtn.Enable()

    def OnRightClick(self, event):
        self.PopupMenu(ContextMenu(self.controller, self))


class SeancesList(wx.ScrolledWindow):

    def __init__(self, parent, controller=None, *args, **kwds):
        super().__init__(parent, *args, **kwds)
        self.controller = controller
        self.__set_properties()
        self.__do_layout()
        self.__classe = None
        self._seancesW = {}
        self.Bind(do.EVT_EXO_CHANGED, self.OnExoChanged)
        self.Bind(do.EVT_FEUILLE_ADDED, self.OnFeuilleAdded)
        self.Bind(do.EVT_FEUILLE_DELETED, self.OnFeuilleDeleted)
        self.Bind(do.EVT_SEANCE_CHANGED, self.OnSeanceChanged)
        self.Bind(do.EVT_SEANCE_DELETED, self.OnSeanceDeleted)
        self.Bind(do.EVT_SEANCE_NEW, self.OnSeanceNew)

    def __set_properties(self):
        print("auto", self.IsAutoScrolling())
        self.SetScrollRate(10, 50)
        self.SetMinSize((amutils.NB_EXOS_PER_SEANCE*(ui.EXO_WIDTH+1)
                         + (amutils.NB_EXOS_PER_SEANCE)+5, -1))

    def __do_layout(self):
        self.sizerV = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizerV)
        self.Layout()

    @property
    def classe(self):
        return self.__classe

    @classe.setter
    def classe(self, classe):
        self.Clear()
        firstFeuille = True
        for feuille in classe.feuilles:
            positionSeance = 0
            if firstFeuille:
                firstFeuille = False
            else:
                self._AddFeuilleSep()
            for seance in feuille.seances:
                self._AddSeanceW(seance)
                positionSeance = seance.position + 1
            self._AddEmptySeanceW(feuille, positionSeance)
        self.Layout()
        self.sizerV.FitInside(self)
        self.Scroll(0,self.GetVirtualSize().GetHeight())

    def GetItemIndex(self, window):
        return self.sizerV.GetChildren().index(self.sizerV.GetItem(window))

    def Clear(self):
        self.sizerV.Clear(delete_windows=True)
        self._seancesW = {}

    def SetSelection(self, seance, position=None, positions=None):
        if positions is None:
            positions = []
        if position is not None:
            positions = [position]
        for item in self.sizerV.GetChildren():
            seanceW = item.GetWindow()
            if seanceW.__class__.__name__ == "SeanceWidget":
                seanceW.SetSelected(positions, seanceW.seance == seance)

    @property
    def selectedSeance(self):
        for item in self.sizerV.GetChildren():
            seanceW = item.GetWindow()
            if seanceW.__class__.__name__ == "SeanceWidget" \
               and seanceW.selected:
                return seanceW.seance
        return None

    @amutils.debug
    def OnExoChanged(self, event):
        exo = event.exo
        for seance, seanceW in self._seancesW.items():
            if exo in seance:
                seanceW.Update()

    @amutils.debug
    def OnFeuilleAdded(self, event):
        feuille = event.feuille
        if feuille.IsFirst():
            self._AddFeuilleSep(0)
            self._AddEmptySeanceW(feuille, 0, 0)
        elif feuille.IsLast():
            self._AddFeuilleSep()
            self._AddEmptySeanceW(feuille, 0)
        else:
            position = 0
            while position < self.sizerV.GetItemCount()-1 \
                and (self.sizerV.GetItem(position).GetWindow().__class__ \
                     != SeanceWidget
                     or self.sizerV.GetItem(position).GetWindow().feuille.position \
                         < feuille.position):
                position += 1
            self._AddFeuilleSep(position)
            self._AddEmptySeanceW(feuille, 0, position)
        self.Layout()
        self.sizerV.FitInside(self)

    @amutils.debug
    def OnFeuilleDeleted(self, event):
        feuille = event.feuille
        position = 0
        while position < self.sizerV.GetItemCount()-1 \
            and (self.sizerV.GetItem(position).GetWindow().__class__ \
                 != SeanceWidget
                 or self.sizerV.GetItem(position).GetWindow().feuille.position \
                     < feuille.position):
            position += 1
        while position < self.sizerV.GetItemCount()-1 \
            and (self.sizerV.GetItem(position).GetWindow().__class__ != SeanceWidget \
                 or self.sizerV.GetItem(position).GetWindow().feuille == feuille):
            self.sizerV.GetItem(position).DeleteWindows()
            position +=1
        self.Layout()
        self.sizerV.FitInside(self)

    @amutils.debug
    def OnSeanceChanged(self, event):
        seance = event.seance
        self._seancesW[seance].Update()
        for exo in event.exos:
            for seance, seanceW in self._seancesW.items():
                if exo in seance:
                    seanceW.Update()
        # if not seance.IsEmpty():
        #     pos = self._GetSeanceWPosition(self._seancesW[seance])
        #     if pos == self.sizerV.GetItemCount()-1 \
        #         or self.sizerV.GetItem(pos+1).GetWindow() is None:
        #         self._AddEmptySeanceW(pos+1)

    @amutils.debug
    def OnSeanceDeleted(self, event):
        seance = event.seance
        seanceW = self._seancesW[seance]
        position = self.GetItemIndex(seanceW)
        seanceW.Destroy()
        del self._seancesW[seance]
        while self.sizerV.GetItem(position).GetWindow().seance.id is not None:
            position += 1
        self.sizerV.GetItem(position).GetWindow().seance.position -= 1
        self.Layout()
        self.sizerV.FitInside(self)

    @amutils.debug
    def OnSeanceNew(self, event):
        seance = event.seance
        feuille = seance.feuille
        position = 0
        if seance in self._seancesW.keys():
            self._seancesW[seance].Update()
            position = self.GetItemIndex(self._seancesW[seance])
        else:
            while position < self.sizerV.GetItemCount() \
                and (self.sizerV.GetItem(position).GetWindow().__class__
                      != SeanceWidget
                      or self.sizerV.GetItem(position).GetWindow().seance.feuille
                      != feuille):
                position += 1
            while self.sizerV.GetItem(position).GetWindow().seance.position \
                  < seance.position-1:
                position += 1
            self._AddSeanceW(seance, position)
        # while position < self.sizerV.GetItemCount() \
        #     and self.sizerV.GetItem(position).GetWindow().__class__ \
        #         == SeanceWidget \
        #     and self.sizerV.GetItem(position).GetWindow().seance.id is not None:
        #     position = self.sizerV.GetItem(position).GetWindow().seance.position
        #     position += 1
        # print(position, self.sizerV.GetItemCount())
        position += 1
        if position == self.sizerV.GetItemCount() or \
            self.sizerV.GetItem(position).GetWindow().__class__ != SeanceWidget:
            self._AddEmptySeanceW(feuille, position+1, position)
        # else:
        #     self.sizerV.GetItem(position).GetWindow().seance.position += 1
        self.sizerV.FitInside(self)

    def __repr__(self):
        return self.__class__.__name__

    def _AddFeuilleSep(self, position=None):
        static_line = wx.StaticLine(self, wx.ID_ANY)
        static_line.SetBackgroundColour(FEUILLE_LIMIT_COLOR)
        static_line.SetMinSize(
            (amutils.NB_EXOS_PER_SEANCE*(ui.EXO_WIDTH+1)
             + (amutils.NB_EXOS_PER_SEANCE)-1,
             FEUILLE_LIMIT_HEIGHT))
        if position is None:
            self.sizerV.Add(static_line, 0, 0, 0)
        else:
            self.sizerV.Insert(position, static_line, 0, 0, 0)

    def _AddEmptySeanceW(self, feuille, positionSeance, position=None):
        self._AddSeanceW(amutils.Seance({'feuille': feuille, 'position': positionSeance}),
                         position)

    def _AddSeanceW(self, seance, position=None):
        seanceW = SeanceWidget(self, self.controller, seance)
        self._seancesW[seance] = seanceW
        if position is None:
            self.sizerV.Add(seanceW, 0, wx.TOP, 1 if seance.position > 0 else 0)
        else:
            self.sizerV.Insert(position,
                               seanceW,
                               0,
                               wx.TOP,
                               1 if seance.position > 0 else 0)

