#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 20:00:22 2020

@author: GuiΩ
"""

import wx
import amutils
from amutils import do
from amutils import ui


class ExosList(wx.Panel):

    def __init__(self, *args, **argz):
        super().__init__(*args, **argz)
        self.controller = None
        self.niveauxFilter = []
        self.themesFilter = []
        self.curentClasse = None
        self.actionHandler = None
        self.exos = []
        self.nExosLbl = wx.StaticText(self, wx.ID_ANY, "0 questions")
        self.exosListPane = wx.ScrolledWindow(self)
        self.__set_properties()
        self.__do_layout()
        self.exosListPane.Bind(wx.EVT_MOTION, self.OnMouseMove)
        self.Bind(do.EVT_EXO_CHANGED, self.OnExoChanged)
        self.Bind(do.EVT_EXO_DELETED, self.OnExoDeleted)
        self.Bind(do.EVT_SEANCE_CHANGED, self.OnSeanceChanged)
        self.Bind(do.EVT_SEANCE_DELETED, self.OnSeanceChanged)
        self.Bind(do.EVT_SEANCE_NEW, self.OnSeanceChanged)
        self.Bind(do.EVT_EXO_LDOWN, self.OnExoClick)
        self.Bind(do.EVT_EXO_RDOWN, self.OnExoClick)
        self.Bind(do.EVT_EXO_RUP, self.OnExoRClick)

    def __set_properties(self):
        self.SetMinSize((222, -1))
        self.exosListPane.SetMinSize((210, -1))
        self.exosListPane.SetScrollRate(0, 30)

    def __do_layout(self):
        vSizer = wx.BoxSizer(wx.VERTICAL)
        vSizer.Add(self.nExosLbl, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        self.exosList = wx.BoxSizer(wx.VERTICAL)
        self.exosListPane.SetSizer(self.exosList)
        vSizer.Add(self.exosListPane, 1, wx.EXPAND, 0)
        self.SetSizer(vSizer)

    def SetActionHandler(self, actionHandler):
        self.actionHandler = actionHandler

    def SetCurrentClasse(self, classe):
        self.currentClasse = classe

    @amutils.debug
    def SetFilter(self, niveaux, themes, classe=None):
        if classe is not None:
            self.currentClasse = classe
        self.niveauxFilter = niveaux
        self.themesFilter = themes
        self.exos = amutils.Exo.GetSelection(self.niveauxFilter,
                                             self.themesFilter)
        self.nExosLbl.SetLabel("{} questions".format(len(self.exos)))
        self.exosList.Clear(True)
        for exo in self.exos:
            exoW = ui.ExoWidget(self.exosListPane,
                                exo,
                                classe=self.currentClasse)
            self.exosList.Add(exoW, 0, wx.ALL, 1)
        self.exosList.FitInside(self.exosListPane)

    def GetSelection(self):
        """

        Returns the selected exo
        -------
        Exo
            Return the selected exo.

        """
        for item in self.exosList.GetChildren():
            exoW = item.GetWindow()
            if exoW is not None and exoW.selected:
                return exoW.exo
        return None

    def SetSelection(self, exoW=None):
        for item in self.exosList.GetChildren():
            item.GetWindow().selected = item.GetWindow() == exoW
            if item.GetWindow() == exoW:
                exoW.SetFocus()

    def OnExoClick(self, event):
        exoW = event.GetEventObject()
        self.actionHandler.Info(
            str(exoW.exo) +
            "Question {} ; position {}"
            .format(exoW.exo.Get('id'), exoW.exo.position))
        self.SetSelection(exoW)
        event.Skip()

    def OnExoRClick(self, event):
        self.PopupMenu(ContextMenu(self.controller,
                                   self.actionHandler,
                                   exo=self.GetSelection()))

    @amutils.debug
    def OnExoChanged(self, event):
        # TODO: update selection
        exo = event.exo
        show = False
        if self.niveauxFilter is not None and len(self.niveauxFilter) > 0:
            for niveau in self.niveauxFilter:
                if niveau in exo.niveaux:
                    for theme in self.themesFilter:
                        if theme in exo.themes:
                            show = True
                            break
                    break
        else:
            for theme in self.themesFilter:
                if theme in exo.themes:
                    show = True
                    break
        if show:    # Exo must be shown
            if exo in self.exos:
                index = self.exos.index(exo)
                exoW = self.exosList.GetItem(index).GetWindow()
                exoW.UpdateThumb()
            else:
                exos = amutils.Exo.GetSelection(self.niveauxFilter,
                                                self.themesFilter)
                index = exos.index(exo)
                exoW = ui.ExoWidget(self.exosListPane,
                                    exo,
                                    classe=self.currentClasse)
                self.exosList.Insert(index, exoW, 0, wx.ALL, 1)
                self.exos = exos
            self.SetSelection(exoW)
        else:
            if exo in self.exos:
                position = self.exos.index(exo)
                item = self.exosList.GetItem(position)
                item.DeleteWindows()
                self.exosList.Detach(position)
                del self.exos[position]
            self.SetSelection(None)
        self.exosList.FitInside(self.exosListPane)

    @amutils.debug
    def OnExoDeleted(self, event):
        print('delete')
        exo = event.exo
        if exo in self.exos:
            position = self.exos.index(exo)
            self.exosList.GetItem(position).DeleteWindows()
            self.exosList.Detach(position)
            del self.exos[position]
        self.exosList.FitInside(self.exosListPane)

    def OnMouseMove(self, event):
        if event.Dragging():
            dropSource = wx.DropSource(self)
            dragData = ui.ExoDataHelper(exo=self.GetSelection())\
                .GetDataObject()
            dropSource.SetData(dragData)
            self.result = dropSource.DoDragDrop(True)

    @amutils.debug
    def OnSeanceChanged(self, event):
        for exo in event.exos:
            if exo in self.exos: 
                positionItem = self.exos.index(exo)
                exoW = self.exosList.GetItem(positionItem).GetWindow()
                exoW.UpdateScores()
        self.exosList.FitInside(self.exosListPane)
                

    def __repr__(self):
        return 'ExoList'


class ContextMenu(wx.Menu):
    def __init__(self, controller, actionHandler, exo, *args, **argz):
        super().__init__(*args, **argz)
        self.controller = controller
        self.actionHandler = actionHandler
        self.exo = exo
        menuItem = self.Append(do.ID_EXO_EDIT, "Modifier")
        menuItem = self.Append(do.ID_EXO_DUPLICATE, "Dupliquer")
        menuItem = self.Append(do.ID_EXO_DELETE, "Supprimer")
        menuItem.Enable(not exo.IsUsed())
        self.Append(wx.ID_CANCEL, 'Annuler')
        self.Bind(wx.EVT_MENU, self.OnMenuClick)

    def OnMenuClick(self, event):
        if event.GetId() == wx.ID_CANCEL:
            return
        elif event.GetId() == do.ID_EXO_EDIT:
            self.actionHandler.EditExo(self.exo)
        elif event.GetId() == do.ID_EXO_DUPLICATE:
            self.actionHandler.EditExo(self.exo.Copy())
        else:
            self.controller.DoExoAction(event.GetId(), exo=self.exo)
