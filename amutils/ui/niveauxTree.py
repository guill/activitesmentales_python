#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 12:41:40 2020

@author: GuiΩ
"""

import amutils
import wx


class NiveauxTree(wx.TreeCtrl):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._UpdateNiveaux()
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnTreeSelChanged)

    def GetSelection(self):
        selection = []
        etabItem, cookie1 = self.GetFirstChild(self.GetRootItem())
        while etabItem.IsOk():
            niveauItem, cookie2 = self.GetFirstChild(etabItem)
            while niveauItem.IsOk():
                if self.IsSelected(niveauItem):
                    selection.append(self.GetItemData(niveauItem)['niveau'])
                niveauItem, cookie2 = self.GetNextChild(etabItem, cookie2)
            etabItem, cookie1 = self.GetNextChild(self.GetRootItem(), cookie1)
        return selection

    def OnTreeSelChanged(self, event):
        data = self.GetItemData(event.GetItem())
        # if etablissement selected, select all its niveaux
        if 'etablissement' in data.keys() \
                and self.IsSelected(event.GetItem()):
            etabId = event.GetItem()
            niveauId, cookie = self.GetFirstChild(etabId)
            while niveauId:
                self.SelectItem(niveauId)
                niveauId, cookie = self.GetNextChild(etabId, cookie)
        # if a niveau is unselected, unselect its etablissement
        if 'niveau' in data.keys() \
                and not self.IsSelected(event.GetItem()):
            etabId = self.GetItemParent(event.GetItem())
            self.SelectItem(etabId, False)
        event.Skip()

    def _UpdateNiveaux(self):
        self.DeleteAllItems()
        rootItem = self.AddRoot("Établissements")
        for etablissement in amutils.Etablissement.GetList():
            item = self.AppendItem(
                rootItem,
                etablissement.label,
                data={'etablissement': etablissement})
            item.etablissement = etablissement
            for niveau in etablissement.niveaux:
                subitem = self.AppendItem(
                    item, niveau.label, data={'niveau': niveau})
                subitem.niveau = niveau
