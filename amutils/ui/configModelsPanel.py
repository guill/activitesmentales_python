#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 09:22:41 2020

@author: GuiΩ
"""

import amutils
from amutils import ui
import wx


class ConfigModelsPanel(ui.AbstractConfigModelsPanel):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.latexModelPages = []
        self.latexModelsCtrl.DeleteAllPages()
        for id in [0, -1]:
            latexModel = amutils.LatexModel.FromId(id)
            latexModelPage = LatexModelPage(self.latexModelsCtrl,
                                            latexModel)
            self.latexModelPages.append(latexModelPage)
            self.latexModelsCtrl.AddPage(latexModelPage, latexModel.Get('label'))
        for latexModel in amutils.LatexModel.GetList():
            if latexModel.Get('id') > 0:
                latexModelPage = LatexModelPage(self.latexModelsCtrl,
                                                latexModel)
                self.latexModelPages.append(latexModelPage)
                self.latexModelsCtrl.AddPage(latexModelPage, latexModel.Get('label'))
        self.Layout()

    def Validate(self):
        for latexModel in amutils.LatexModel.GetList():
            for latexModelPage in self.latexModelPages:
                if latexModelPage.latexModel.Get('id') == latexModel.Get('id'):
                    break
            # if latexModelPage.latexModel.Get('id') != latexModel.Get('id'):
            #     latexModelPage.Delete()
            for latexModelPage in self.latexModelPages:
                latexModel = latexModelPage.GetLatexModel()
                latexModel.Save()

    def OnAddLatexModelClick(self, event):
        latexModel = amutils.LatexModel()
        latexModel.Set('label', "Nouveau Modèle")
        latexModelPage = LatexModelPage(self.latexModelsCtrl, latexModel)
        self.latexModelPages.append(latexModelPage)
        self.latexModelsCtrl.AddPage(latexModelPage, "Nouveau Modèle")
        self.latexModelsCtrl.SetSelection(
            self.latexModelsCtrl.GetPageCount()-1)

    def OnRemoveLatexModelClick(self, event):
        rep = wx.MessageDialog(
            self,
            "Êtes-vous sûr de vouloir supprimer le modèle '{}' ?".format(
                self.latexModelsCtrl.GetPageText(
                    self.latexModelsCtrl.GetSelection())),
            caption="Attention",
            style=wx.OK | wx.CANCEL | wx.CANCEL_DEFAULT | wx.ICON_QUESTION
            | wx.CENTRE,
            pos=wx.DefaultPosition).ShowModal()
        if rep == wx.ID_OK:
            position = self.latexModelsCtrl.GetSelection()
            del self.latexModelsCtrl[position+1]
            self.latexModelsCtrl.DeletePage(position)

    def OnLatexPageChanged(self, event):
        b = event.GetSelection() > 1
        self.latexModelRemoveBtn.Enable(b)

    def OnLatexPageNameChanged(self, event):
        for i in range(len(self.latexModelPages)):
            self.latexModelsCtrl.SetPageText(
                2+i,
                self.latexModelPages[i].nomCtrl.GetValue())


class LatexModelPage(wx.ScrolledWindow):
    def __init__(self, parent, latexModel, *args, **argz):
        if 'style' in argz.keys():
            argz['style'] |= wx.TAB_TRAVERSAL
        else:
            argz['style'] = wx.TAB_TRAVERSAL
        super().__init__(parent, *args, **argz)
        self.SetScrollRate(10, 10)
        self.latexModel = latexModel
        if latexModel.Get('id') == 0:
            self.headerPanel = wx.Panel(self, wx.ID_ANY)
            self.headerModelCtrl = wx.TextCtrl(self.headerPanel,
                                               wx.ID_ANY,
                                               style=wx.TE_MULTILINE)
            if latexModel.GetPart('HEADER') is not None:
                self.headerModelCtrl.SetValue(latexModel.GetPart('HEADER'))
            self.variableHeaderBtn = wx.Button(self, wx.ID_ANY, "Variable...")
            self.variableHeaderBtn.type = 'Header'
            self.Bind(wx.EVT_BUTTON,
                      self.OnVariablesClick,
                      self.variableHeaderBtn)
        elif latexModel.Get('id') == -1:
            self.deleteTempCtrl = wx.CheckBox(
                self,
                wx.ID_ANY,
                "Supprimer les fichiers temporaires :")
            if latexModel.Get('deleteTmp'):
                self.deleteTempCtrl.SetValue(wx.CHK_CHECKED)
            else:
                self.deleteTempCtrl.SetValue(wx.CHK_UNCHECKED)
            self.exoPanel = wx.Panel(self, wx.ID_ANY)
            self.exoModelCtrl = wx.TextCtrl(self.exoPanel,
                                            wx.ID_ANY,
                                            style=wx.TE_MULTILINE)
            if latexModel.GetPart('QUESTION') is not None:
                self.exoModelCtrl.SetValue(latexModel.GetPart('QUESTION'))
            self.variableExoBtn = wx.Button(self, wx.ID_ANY, "Variable...")
            self.variableExoBtn.type = 'Exo'
            self.Bind(wx.EVT_BUTTON,
                      self.OnVariablesClick,
                      self.variableExoBtn)
        else:
            self.nomCtrl = wx.TextCtrl(self,
                                       wx.ID_ANY,
                                       latexModel.Get('label'))
            self.descriptionCtrl = wx.TextCtrl(self, wx.ID_ANY,
                                               latexModel.Get('description'))
            self.enabledCtrl = wx.CheckBox(self, wx.ID_ANY, u"Activé")
            if latexModel.Get('enabled'):
                self.enabledCtrl.SetValue(wx.CHK_CHECKED)
            else:
                self.enabledCtrl.SetValue(wx.CHK_UNCHECKED)
            self.deleteTempCtrl = wx.CheckBox(
                self,
                wx.ID_ANY,
                "Supprimer les fichiers temporaires :")
            if latexModel.Get('deleteTmp'):
                self.deleteTempCtrl.SetValue(wx.CHK_CHECKED)
            else:
                self.deleteTempCtrl.SetValue(wx.CHK_UNCHECKED)
            self.fileNameCtrl = wx.TextCtrl(
                self, wx.ID_ANY,
                "\n".join(latexModel.Get('outfilePaths').split(";")),
                style=wx.TE_MULTILINE)
            self.variableFileBtn = wx.Button(self, wx.ID_ANY, "Variable...")
            self.variableFileBtn.type = 'File'
            self.feuillePanel = wx.Panel(self, wx.ID_ANY)
            self.feuilleModelCtrl = wx.TextCtrl(self.feuillePanel,
                                                wx.ID_ANY,
                                                style=wx.TE_MULTILINE)
            if latexModel.GetPart('FEUILLE') is not None:
                self.feuilleModelCtrl.SetValue(latexModel.GetPart('FEUILLE'))
            self.variableFeuilleBtn = wx.Button(self, wx.ID_ANY, "Variable...")
            self.variableFeuilleBtn.type = 'Feuille'
            self.seancePanel = wx.Panel(self, wx.ID_ANY)
            self.seanceModelCtrl = wx.TextCtrl(self.seancePanel,
                                               wx.ID_ANY,
                                               style=wx.TE_MULTILINE)
            if latexModel.GetPart('SEANCE') is not None:
                self.seanceModelCtrl.SetValue(latexModel.GetPart('SEANCE'))
            self.variableSeanceBtn = wx.Button(self, wx.ID_ANY, "Variable...")
            self.variableSeanceBtn.type = 'Seance'
            self.exoPanel = wx.Panel(self, wx.ID_ANY)
            self.exoModelCtrl = wx.TextCtrl(self.exoPanel,
                                            wx.ID_ANY,
                                            style=wx.TE_MULTILINE)
            if latexModel.GetPart('QUESTION') is not None:
                self.exoModelCtrl.SetValue(latexModel.GetPart('QUESTION'))
            self.variableExoBtn = wx.Button(self, wx.ID_ANY, "Variable...")
            self.variableExoBtn.type = 'Exo'

            self.Bind(wx.EVT_BUTTON,
                      self.OnVariablesClick,
                      self.variableFileBtn)
            self.Bind(wx.EVT_BUTTON,
                      self.OnVariablesClick,
                      self.variableFeuilleBtn)
            self.Bind(wx.EVT_BUTTON,
                      self.OnVariablesClick,
                      self.variableSeanceBtn)
            self.Bind(wx.EVT_BUTTON,
                      self.OnVariablesClick,
                      self.variableExoBtn)
            self.nomCtrl.Bind(wx.EVT_TEXT,
                              self.GetParent().GetParent()
                              .OnLatexPageNameChanged)
        self.__do_layout()

    def __do_layout(self):
        print(self.latexModel.Get('id'))
        gridSizer = wx.FlexGridSizer(2, 3, 3)
        if self.latexModel.Get('id') == 0:
            headerSizer = wx.BoxSizer(wx.HORIZONTAL)
            label = wx.StaticText(self,
                                  wx.ID_ANY,
                                  self.latexModel.Get('description'))
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            headerSizer.Add(self.headerModelCtrl, 1, wx.ALL | wx.EXPAND, 3)
            self.headerPanel.SetSizer(headerSizer)
            gridSizer.Add(self.headerPanel, 1, wx.EXPAND, 0)
            gridSizer.Add(self.variableHeaderBtn, 0, wx.ALL, 3)
            gridSizer.AddGrowableRow(1)
        elif self.latexModel.Get('id') == -1:
            exoSizer = wx.BoxSizer(wx.HORIZONTAL)
            label = wx.StaticText(self,
                                  wx.ID_ANY,
                                  self.latexModel.Get('description'))
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            label = wx.StaticText(
                self,
                wx.ID_ANY,
                u"Ne pas modifier à moins de savoir ce que vous faites\n")
            label.SetForegroundColour(wx.Colour(255, 0, 0))
            label.SetFont(wx.Font(10,
                                  wx.FONTFAMILY_DEFAULT,
                                  wx.FONTSTYLE_NORMAL,
                                  wx.FONTWEIGHT_NORMAL,
                                  0,
                                  "Noto Sans"))
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            gridSizer.Add(self.deleteTempCtrl, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            exoSizer.Add(self.exoModelCtrl, 1, wx.ALL | wx.EXPAND, 3)
            self.exoPanel.SetSizer(exoSizer)
            gridSizer.Add(self.exoPanel, 1, wx.EXPAND, 0)
            gridSizer.Add(self.variableExoBtn, 0, wx.ALL, 3)
            gridSizer.AddGrowableRow(3)
        else:
            feuilleSizer = wx.BoxSizer(wx.HORIZONTAL)
            seanceSizer = wx.BoxSizer(wx.HORIZONTAL)
            exoSizer = wx.BoxSizer(wx.HORIZONTAL)
            label = wx.StaticText(self, wx.ID_ANY, u"Nom du modèle :")
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            gridSizer.Add(self.nomCtrl, 1, wx.ALL | wx.EXPAND, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            label = wx.StaticText(self, wx.ID_ANY, "Description :")
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            gridSizer.Add(self.descriptionCtrl, 0, wx.ALL | wx.EXPAND, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            gridSizer.Add(self.enabledCtrl, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            gridSizer.Add(self.deleteTempCtrl, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            label = wx.StaticText(
                self,
                wx.ID_ANY,
                u"Noms des fichiers à produire (un par ligne) :")
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            gridSizer.Add(self.fileNameCtrl, 0, wx.ALL | wx.EXPAND, 3)
            gridSizer.Add(self.variableFileBtn, 0, wx.ALL, 3)
            label = wx.StaticText(self, wx.ID_ANY, u"Modèle pour la feuille :")
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            feuilleSizer.Add(self.feuilleModelCtrl, 1, wx.ALL | wx.EXPAND, 3)
            self.feuillePanel.SetSizer(feuilleSizer)
            gridSizer.Add(self.feuillePanel, 1, wx.EXPAND, 0)
            gridSizer.Add(self.variableFeuilleBtn, 0, wx.ALL, 3)
            label = wx.StaticText(self,
                                  wx.ID_ANY,
                                  u"Modèle pour chaque séance :")
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            seanceSizer.Add(self.seanceModelCtrl, 1, wx.ALL | wx.EXPAND, 3)
            self.seancePanel.SetSizer(seanceSizer)
            gridSizer.Add(self.seancePanel, 1, wx.EXPAND, 0)
            gridSizer.Add(self.variableSeanceBtn, 0, wx.ALL, 3)
            label = wx.StaticText(self,
                                  wx.ID_ANY,
                                  u"Modèle pour chaque question :")
            gridSizer.Add(label, 0, wx.ALL, 3)
            gridSizer.Add((0, 0), 0, 0, 0)
            exoSizer.Add(self.exoModelCtrl, 1, wx.ALL | wx.EXPAND, 3)
            self.exoPanel.SetSizer(exoSizer)
            gridSizer.Add(self.exoPanel, 1, wx.EXPAND, 0)
            gridSizer.Add(self.variableExoBtn, 0, wx.ALL, 3)
            gridSizer.AddGrowableRow(9)
            gridSizer.AddGrowableRow(11)
            gridSizer.AddGrowableRow(13)
        gridSizer.AddGrowableCol(0)
        self.SetSizer(gridSizer)
        self.Layout()

    def GetTextCtrl(self, type):
        if type == 'Header':
            return self.headerModelCtrl
        if type == 'File':
            return self.fileNameCtrl
        if type == 'Feuille':
            return self.feuilleModelCtrl,
        if type == 'Seance':
            return self.seanceModelCtrl,
        if type == 'Exo':
            return self.exoModelCtrl

    def GetLatexModel(self):
        if self.latexModel.Get('id') == 0:
            self.latexModel.SetPart('HEADER', self.headerModelCtrl.GetValue())
        elif self.latexModel.Get('id') == -1:
            self.latexModel.SetPart('QUESTION', self.exoModelCtrl.GetValue())
            self.latexModel.Set('deleteTmp', self.deleteTempCtrl.IsChecked())
        else:
            self.latexModel.Set('label', self.nomCtrl.GetValue())
            self.latexModel.Set('description', self.descriptionCtrl.GetValue())
            self.latexModel.Set('enabled', self.enabledCtrl.IsChecked())
            self.latexModel.Set('deleteTmp', self.deleteTempCtrl.IsChecked())
            self.latexModel.Set(
                'outfilePaths',
                self.fileNameCtrl.GetValue().replace("\n", ";"))
            self.latexModel.SetPart('FEUILLE',
                                    self.feuilleModelCtrl.GetValue())
            self.latexModel.SetPart('SEANCE', self.seanceModelCtrl.GetValue())
            self.latexModel.SetPart('QUESTION', self.exoModelCtrl.GetValue())
        return self.latexModel

    def OnVariablesClick(self, event):
        type = event.GetEventObject().type
        options = {'Header':  ['ENTTETE', 'IMAGES_DIR', 'THUMBS_DIR',
                               'ANNEE_SCO', 'NIVEAU', 'CLASSE', 'NFEUILLE',
                               'NFEUILLE_n', 'NSEANCE', 'NSEANCE_n',
                               'NQUESTION', 'NQUESTION_n', 'IDQUESTION',
                               'IDQUESTION_n', 'QUESTION'],
                   'Feuille': ['ENTTETE', 'IMAGES_DIR', 'THUMBS_DIR',
                               'ANNEE_SCO', 'NIVEAU', 'CLASSE', 'NFEUILLE',
                               'NFEUILLE_n'],
                   'Seance': ['ENTTETE', 'IMAGES_DIR', 'THUMBS_DIR',
                              'ANNEE_SCO', 'NIVEAU', 'CLASSE', 'NFEUILLE',
                              'NFEUILLE_n', 'NSEANCE', 'NSEANCE_n',
                              'NOT_FIRST', 'ONLY_FIRST'],
                   'Exo': ['ENTTETE', 'IMAGES_DIR', 'THUMBS_DIR', 'ANNEE_SCO',
                           'NIVEAU', 'CLASSE', 'NFEUILLE', 'NFEUILLE_n',
                           'NSEANCE', 'NSEANCE_n', 'NQUESTION', 'NQUESTION_n',
                           'IDQUESTION', 'IDQUESTION_n', 'QUESTION',
                           'NOT_FIRST', 'ONLY_FIRST']}
        self.PopupMenu(VariablesMenu(self.GetTextCtrl(type), options[type]))


class VariablesMenu(wx.Menu):
    names = ['ENTTETE', 'IMAGES_DIR', 'THUMBS_DIR', 'ANNEE_SCO', 'NIVEAU',
             'CLASSE', 'NFEUILLE', 'NFEUILLE_n', 'NSEANCE', 'NSEANCE_n',
             'NQUESTION', 'NQUESTION_n', 'IDQUESTION', 'IDQUESTION_n',
             'QUESTION', 'NOT_FIRST', 'ONLY_FIRST']
    IDs = [wx.Window.NewControlId() for _ in names]

    def __init__(self, textCtrl, variables):
        super().__init__()
        self.textCtrl = textCtrl
        for variable in variables:
            self.q = self.Append(
                wx.MenuItem(self,
                            self.IDs[self.names.index(variable)],
                            text=variable))
            self.Bind(wx.EVT_MENU, self.OnClick, self.q)

    def OnClick(self, event):
        index = self.IDs.index(event.GetId())
        if self.names[index] in ['NOT_FIRST', 'ONLY_FIRST']:
            pos = self.textCtrl.GetSelection()
            self.textCtrl.Replace(pos[1],
                                  pos[1],
                                  '[[END_{}]]'.format(self.names[index]))
            self.textCtrl.Replace(pos[0],
                                  pos[0],
                                  '[[{}]]'.format(self.names[index]))
        else:
            self.textCtrl.Replace(self.textCtrl.GetInsertionPoint(),
                                  self.textCtrl.GetInsertionPoint(),
                                  '[[{}]]'.format(self.names[index]))
