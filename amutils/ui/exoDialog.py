#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 16:17:22 2020

@author: guill
"""

import amutils
import amutils.caseine.ui as caseineui
from amutils import do
import wx


class ExoDialog(wx.Dialog):
    def __init__(self, parent, controller, *args, **kwargs):
        kwargs["style"] = kwargs.get("style", 0) | wx.RESIZE_BORDER | wx.MAXIMIZE_BOX | wx.CLOSE_BOX
        super().__init__(parent, *args, **kwargs)
        self.controller = controller
        if amutils.EXPORT_CASEINE:
            self.notebook = wx.Notebook(self, wx.ID_ANY)
            self.mainPanel = ExoDialogMainPanel(self.notebook, controller)
            self.caseinePanel = caseineui.ExoDialogPanel(self.notebook, controller, self)
        else:
            self.mainPanel = ExoDialogMainPanel(self, controller)

        self.dateUpdateCtrl = wx.CheckBox(self, wx.ID_ANY, "Modifier la date de mise à jour", style=wx.ALIGN_RIGHT)
        self.annulerBtn = wx.Button(self, wx.ID_ANY, "Annuler")
        self.validerBtn = wx.Button(self, wx.ID_ANY, "Valider")

        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_BUTTON, self.OnAnnulerBtnClick, self.annulerBtn)
        self.Bind(wx.EVT_BUTTON, self.OnValiderBtnClick, self.validerBtn)

    def __set_properties(self):
        # self.SetTitle("Question")
        self.SetSize((1100, 800))

    def __do_layout(self):
        vSizer = wx.BoxSizer(wx.VERTICAL)
        buttonsSizer = wx.BoxSizer(wx.HORIZONTAL)
        if amutils.EXPORT_CASEINE:
            self.notebook.AddPage(self.mainPanel, u"Contenu")
            self.notebook.AddPage(self.caseinePanel, u"Caseine")
            vSizer.Add(self.notebook, 1, wx.ALL | wx.EXPAND, 5)
        else:
            vSizer.Add(self.mainPanel, 1, wx.ALL | wx.EXPAND, 5)
        buttonsSizer.Add((0, 0), 1, 0, 0)
        buttonsSizer.Add(self.dateUpdateCtrl, 0, wx.ALL| wx.ALIGN_CENTER_VERTICAL, 3)
        buttonsSizer.Add(self.annulerBtn, 0, wx.ALL, 3)
        buttonsSizer.Add(self.validerBtn, 0, wx.ALL, 3)
        vSizer.Add(buttonsSizer, 0, wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 3)
        self.SetSizer(vSizer)
        self.Layout()
        self.Centre()

    def OnValiderBtnClick(self, event):
        self.mainPanel.Validate(self.dateUpdateCtrl.GetValue())
        if amutils.EXPORT_CASEINE:
            self.caseinePanel.Validate()
        self.EndModal(wx.ID_OK)

    def ShowModal(self, exo=None):
        self.exo = exo
        if exo.id is None:
            self.SetTitle("Création d'une nouvelle question")
            #self.dateUpdateCtrl.SetValue(True)
            # self.dateUpdateCtrl.Enable(False)
        else:
            self.SetTitle(
                "Modification de la question {}".format(exo.id))
            self.dateUpdateCtrl.SetValue(False)
            self.dateUpdateCtrl.Enable(True)
        self.mainPanel.SetExo(exo)
        if amutils.EXPORT_CASEINE:
            self.caseinePanel.SetExo(exo)
        super().ShowModal()

    def OnAnnulerBtnClick(self, event):
        self.EndModal(wx.ID_CANCEL)


class ExoDialogMainPanel(wx.SplitterWindow):

    def __init__(self, parent, controller, *args, **kwargs):
        kwargs["style"] = kwargs.get("style", 0) | wx.SP_3D | wx.SP_LIVE_UPDATE
        super().__init__(parent, *args, **kwargs)
        self.controller = controller
        self.leftPane = wx.Panel(self, wx.ID_ANY)
        self.toolsPane = wx.Panel(self.leftPane, wx.ID_ANY)
        self.leftSplitPane = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D | wx.SP_LIVE_UPDATE)
        self.codePane = wx.ScrolledWindow(self.leftSplitPane, wx.ID_ANY)
        self.previewPane = wx.Panel(self.leftSplitPane, wx.ID_ANY)
        self.codeCtrl = wx.TextCtrl(self.codePane, wx.ID_ANY, "", style=wx.HSCROLL | wx.TE_MULTILINE)
        self.previewBtn = wx.Button(self.previewPane, wx.ID_ANY, "Aperçu")
        self.previewScrolledSizer = wx.BoxSizer(wx.VERTICAL)
        self.previewScrolled = wx.ScrolledWindow(self.previewPane, wx.ID_ANY)
        self.exoPreview = wx.StaticBitmap(self.previewScrolled, wx.ID_ANY, wx.NullBitmap)
        self.rightPane = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D | wx.SP_LIVE_UPDATE)
        self.niveauxTree = wx.TreeCtrl(self.rightPane,
                                       wx.ID_ANY,
                                       style=wx.TR_HAS_BUTTONS | wx.TR_HIDE_ROOT | wx.TR_MULTIPLE)
        self.themesTree = wx.TreeCtrl(self.rightPane,
                                      wx.ID_ANY,
                                      style=wx.TR_HAS_BUTTONS | wx.TR_HIDE_ROOT | wx.TR_MULTIPLE)
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_BUTTON, self.OnPreviewClick)
        self.codeCtrl.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)

    def __set_properties(self):
        self.exoPreview.SetBackgroundColour(wx.Colour(235,235,255))
        self.previewBtn.SetToolTip("Aperçu (F5)")
        self.rightPane.SetMinimumPaneSize(20)
        self.SetMinimumPaneSize(20)
        self.SetEtablissements(amutils.Etablissement.GetList())
        self.SetThemeRoot(amutils.Theme.GetRoot())

    def __do_layout(self):
        leftSizer = wx.BoxSizer(wx.VERTICAL)
        self.toolsSizer = wx.BoxSizer(wx.HORIZONTAL)
        for latexChunk in amutils.LatexChunk.GetList():
            if latexChunk.icon is not None:
                button = wx.BitmapButton(
                    self.toolsPane,
                    wx.ID_ANY,
                    wx.Bitmap('amutils/ui/images/{}'
                              .format(latexChunk.icon)))
            else:
                button = wx.Button(self.toolsPane,
                                   wx.ID_ANY,
                                   latexChunk.label)
            button.latexChunk = latexChunk
            self.Bind(wx.EVT_BUTTON, self.OnToolClick, button)
            self.toolsSizer.Add(button, 0, wx.ALL, 3)
        self.toolsPane.SetSizer(self.toolsSizer)
        leftSizer.Add(self.toolsPane, 0, wx.ALL | wx.EXPAND, 3)
        codeSizer = wx.BoxSizer(wx.VERTICAL)
        codeSizer.Add(self.codeCtrl, 1, wx.ALL | wx.EXPAND, 1)
        self.codePane.SetSizer(codeSizer)
        previewSizer = wx.BoxSizer(wx.VERTICAL)
        previewHeadSizer = wx.BoxSizer(wx.HORIZONTAL)
        previewHeadSizer.Add(self.previewBtn, 0, wx.ALL | wx.EXPAND, 3)
        previewSizer.Add(previewHeadSizer, 0, wx.ALL | wx.EXPAND, 1)
        self.previewScrolledSizer.Add(self.exoPreview, 1, wx.ALL | wx.EXPAND, 1)
        self.previewScrolled.SetSizer(self.previewScrolledSizer)
        previewSizer.Add(self.previewScrolled, 1, wx.ALL|wx.EXPAND, 1)
        self.previewPane.SetSizer(previewSizer)
        self.leftSplitPane.SplitHorizontally(self.codePane, self.previewPane, 300)
        leftSizer.Add(self.leftSplitPane, 1, wx.ALL | wx.EXPAND, 1)
        self.leftPane.SetSizer(leftSizer)
        self.rightPane.SplitHorizontally(self.niveauxTree, self.themesTree)
        self.SplitVertically(self.leftPane, self.rightPane)
        self.Layout()
        self.Centre()

    def SetEtablissements(self, etablissements):
        rootItem = self.niveauxTree.AddRoot("Établissements")
        for etablissement in etablissements:
            item = self.niveauxTree.AppendItem(
                rootItem,
                etablissement.label,
                data={'etablissement': etablissement})
            for niveau in etablissement.niveaux:
                self.niveauxTree.AppendItem(
                    item,
                    niveau.label,
                    data={'niveau': niveau})

    def SetExo(self, exo):
        self.exo = exo
        self.codeCtrl.SetValue(exo.code)
        # selects niveaux
        self.niveauxTree.UnselectAll()
        for niveauId in self.__GetNiveauItemIds():
            if self.niveauxTree.GetItemData(niveauId)['niveau'] in exo.niveaux:
                self.niveauxTree.SelectItem(niveauId)
        # selects themes
        self.themesTree.UnselectAll()
        for themeId in self.__GetThemesItemIds():
            if self.themesTree.GetItemData(themeId)['theme'] in exo.themes:
                self.themesTree.SelectItem(themeId)
        if exo.id is not None:
            self.exoPreview.SetBitmap(exo.thumb)
        else:
            self.__UpdatePreview()
        self.Layout()

    def GetSelectedNiveaux(self):
        sel = []
        for item in self.niveauxTree.GetSelections():
            sel.append(self.niveauxTree.GetItemData(item)['niveau'])
        return sel

    def SetThemeRoot(self, root):
        """Set the themesTree content"""
        rootItem = self.themesTree.AddRoot("Thèmes")
        rootItem.theme = root
        self.__SetSubThemes(rootItem, root)

    def GetSelectedThemes(self):
        sel = []
        for item in self.themesTree.GetSelections():
            sel.append(self.themesTree.GetItemData(item)['theme'])
        return sel

    def Validate(self, updateDate):
        if self.exo.id is None:
            self.exo.code = self.codeCtrl.GetValue()
            self.exo.niveaux = self.GetSelectedNiveaux()
            self.exo.themes = self.GetSelectedThemes()
            self.exo.updateDate=wx.DateTime.Now().FormatISOCombined() if updateDate else self.exo.updateDate
            self.controller.DoExoAction(
                action=do.ID_EXO_NEW,
                exo=self.exo)
        else:
            self.controller.DoExoAction(
                action=do.ID_EXO_EDIT,
                exo=self.exo,
                code=self.codeCtrl.GetValue(),
                niveaux=self.GetSelectedNiveaux(),
                themes=self.GetSelectedThemes(),
                updateDate=wx.DateTime.Now().FormatISOCombined() if updateDate else self.exo.updateDate)

    def OnKeyDown(self, event):
        if event.GetKeyCode() == wx.WXK_F5:
            self.__UpdatePreview()
        event.Skip()

    def OnToolClick(self, event):
        self.codeCtrl.Replace(self.codeCtrl.GetInsertionPoint(),
                              self.codeCtrl.GetInsertionPoint(),
                              event.GetEventObject().latexChunk.code)

    def OnPreviewClick (self, event):
        self.__UpdatePreview()

    def __GetEtabItemIds(self):
        etabIds = []
        root = self.niveauxTree.GetRootItem()
        etabId, cookie = self.niveauxTree.GetFirstChild(root)
        while etabId:
            etabIds.append(etabId)
            etabId, cookie = self.niveauxTree.GetNextChild(root, cookie)
        return etabIds

    def __GetNiveauItemIds(self):
        niveauIds = []
        for etabId in self.__GetEtabItemIds():
            niveauId, cookie = self.niveauxTree.GetFirstChild(etabId)
            while niveauId:
                niveauIds.append(niveauId)
                niveauId, cookie = self.niveauxTree.GetNextChild(etabId,
                                                                 cookie)
        return niveauIds

    def __GetThemesItemIds(self):
        itemIds = []
        queue = [self.themesTree.GetRootItem()]
        while len(queue):
            item = queue.pop()
            childId, cookie = self.themesTree.GetFirstChild(item)
            while childId:
                queue.append(childId)
                itemIds.append(childId)
                childId, cookie = self.themesTree.GetNextChild(item, cookie)
        return itemIds

    def __UpdatePreview (self):
        code = self.codeCtrl.GetValue()
        if code == '':
            thumb, msg = wx.Bitmap(), None
        else:
            thumb, msg = amutils.LatexModel.FromId(-1).ToBitmap(exo=amutils.Exo({"code": code}))
        if msg is not None:
            msgD = amutils.ui.LongMessageDialog(self)
            msgD.SetContent("Sortie de la commande", msg)
            # TODO: Scroll to the end of the content
            msgD.ShowModal()
            msgD.Destroy()
        else:
            self.exoPreview.SetBitmap(thumb)
            self.Layout()

    def __SetSubThemes(self, parentItem, parentTheme):
        for theme in parentTheme.children:
            item = self.themesTree.AppendItem(
                parentItem,
                theme.label,
                data={'theme': theme})
            self.__SetSubThemes(item, theme)
