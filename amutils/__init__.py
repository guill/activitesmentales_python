#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 06:44:00 2020

@author: GuiΩ
"""

EXPORT_CASEINE = True
DEBUG = False
DEBUG_SQL = False

from .debug import debug
from .dbobject import DBObject
from .seance import NB_EXOS_PER_SEANCE
from .controller import Controller
from .classe import Classe
from .etablissement import Etablissement
from .exo import Exo
from .feuille import Feuille
from .latexChunk import LatexChunk
from .latexModel import LatexModel
from .niveau import Niveau
from .theme import Theme
from .seance import Seance
from .config import UserConfig



