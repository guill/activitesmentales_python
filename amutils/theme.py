#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 18:22:27 2020

@author: guill
"""


import amutils


class Theme(amutils.DBObject):
    table = "themes"

    @staticmethod
    def GetRoot():
        return Theme.FromId(0)

    def __init__(self, data=None):
        super().__init__(data)
        self.__children = None

    @property
    def children(self):
        if self.__children is None:
            self.__children = []
            vars = (self.id,)
            for row in amutils.DBObject.connexion.execute(
                    """SELECT "id" FROM "themes"
                        WHERE "parentId"=?
                        ORDER BY "position";""", vars):
                self.__children.append(Theme.FromId(row['id']))
        return self.__children.copy()

    @property
    def fullLabel(self):
        if self.id == 0 or self.parent.id == 0:
            return ''
        else:
            parentLbl = self.parent.fullLabel
            if parentLbl != '':
                parentLbl += ' : '
            return parentLbl + self.label

    @property
    def childrenIds(self):
        ids = [self.id]
        queue = self.children
        while len(queue) > 0:
            item = queue.pop()
            ids.append(item.id)
            for i in item.children:
                queue.append(i)
        return ids

    @property
    def descendants(self):
        list = self.children
        position = 0
        while position < len(list):
            for theme in list[position].children:
                list.append(theme)
            position += 1
        return list

    @staticmethod
    def GetList():
        list = [Theme.GetRoot()]
        position = 0
        while len(list) > position:
            for theme in list[position].children:
                list.append(theme)
            position += 1
        return list

    @staticmethod
    def GetFilteredList(niveaux):
        listIds = []
        sql = """SELECT DISTINCT "id"
                  FROM "themes" T
                    LEFT JOIN "lienExosThemes" LET ON LET."themeId"=T."id"
                    LEFT JOIN "lienExosNiveaux" LEN ON LEN."exoId"=LET."exoID"
                  WHERE LEN."niveauId" IN ({});""".format(
                ",".join(map(lambda niveau: str(niveau.id), niveaux)))
        for row in amutils.DBObject.connexion.execute(sql):
            listIds.append(row['id'])
        flag = True
        while flag:
            flag = False
            sql = """SELECT DISTINCT "parentId"
                      FROM "themes"
                      WHERE "parentId" IS NOT NULL AND id IN ({});""".format(
                      ",".join(map(str, listIds)))
            for row in amutils.DBObject.connexion.execute(sql):
                if row['parentId'] not in listIds:
                    listIds.append(row['parentId'])
                    if row['parentId'] != 0:
                        flag = True
        sql = """SELECT DISTINCT "parentId"
                  FROM "themes"
                  WHERE "parentId" IS NOT NULL AND id IN ({});""".format(
                  ",".join(map(str, listIds)))
        for row in amutils.DBObject.connexion.execute(sql):
            listIds.append(row['parentId'])
        themes = [amutils.Theme.FromId(id) for id in listIds]
        return themes
