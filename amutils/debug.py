#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 14:57:46 2020

@author: GuiΩ
"""

import functools
import amutils

def debug(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if amutils.DEBUG:
            # if func.__name__ in ('Do', 'Redo', 'Undo'):
            #     s = '{}.{}: {}'.format(
            #         args[0],
            #         func.__name__,
            #         {k: _GetActionName(k, v)
            #          for k, v in args[0].__dict__.items() if k != 'label'})
            # else:
            s = '*** {}.{}({})'.format(
                args[0],
                func.__name__,
                (', '.join(list(map(str, args[1:])))
                 + repr({k: _GetActionName(k, v)
                         for k, v in kwargs.items()})))
            print(s)
        return func(*args, **kwargs)
    return wrapper


def _GetActionName(k, v):
    from amutils import do
    if k == 'action':
        return list(do.__dict__.keys())[list(do.__dict__.values()).index(v)]
    else:
        return v
