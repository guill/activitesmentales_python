#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 16:12:40 2020

@author: GuiΩ
"""


from amutils import DBObject


class LatexChunk(DBObject):
    table = "latexChunks"
