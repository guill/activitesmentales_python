#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 23:56:59 2020

@author: guill
"""

import amutils
import tempfile
import os
import pdf2image
import re
import shutil
import subprocess
import wx


class LatexModel(amutils.DBObject):
    userConfig = None
    table = "latexModels"

    def __init__(self, row=None):
        super().__init__(row)
        self.__parts = None
        self.__output = ""
        self.__outfiles = []

    @property
    def parts(self):
        if not self.__parts:
            self.__parts = {}
            if self.id is not None:
                vars = (self.id,)
                for row in amutils.DBObject.connexion.execute(
                     """ SELECT "type", "content"
                         FROM "latexModelParts"
                         WHERE "latexModelId"=?""", vars):
                    self.__parts[row['type']] = row['content']
        return self.__parts

    @amutils.debug
    def GetPart(self, part):
        if part in self.parts.keys():
            return self.parts[part]
        return ''

    @amutils.debug
    def SetPart(self, part, content):
        self.parts
        self.__parts[part] = content
        self._dirty = True

    @amutils.debug
    def CompileModelToFile(self, code, destFileName):
        latexDoc = self.GetLatexSource(code=code)
        return self.CompileToFiles(latexDoc, (destFileName))

    @amutils.debug
    def CompileToFiles(self, latexDoc, destFileNames):
        tmpDir, pdfFileName = self.Compile(latexDoc)
        if pdfFileName is None:
            return False
        for destFileName in destFileNames:
            self.__CopyFile(tmpDir, pdfFileName, destFileName)
            self.__outfiles.append(destFileName)
        return True

    @amutils.debug
    def CompileModel(self, *, classe=None, feuille=None, seance=None, exo=None):
        """
        Compute files from latex model.

        Parameters
        ----------
        :param classe: optional, the default is None.
        :param feuille: TYPE, optional
            DESCRIPTION. The default is None.
        seance : TYPE, optional
            DESCRIPTION. The default is None.
        exo : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        bool
            True if the compilation no produced error
            False otherwise.

        """
        if feuille is None:
            if seance is not None:
                feuille = seance.feuille
        if classe is None:
            if feuille is not None:
                classe = feuille.classe
        self.__outfiles = []
        code = self.GetLatexSource(classe=classe, feuille=feuille, seance=seance, exo=exo)
        destFileNames = []
        for destFileName in self.Get('outfilePaths').split(";"):
            destFileNames.append(self.ReplaceVars(destFileName,
                                                classe=classe,
                                                feuille=feuille,
                                                seance=seance,
                                                exo=exo))
        return self.CompileToFiles(code, destFileNames)

    @amutils.debug
    def codeToBase64(self, code):
        source = self.GetLatexSource(code=code)
        return self.sourceToBase64(source)

    @amutils.debug
    def ToBitmap(self, *, classe=None, feuille=None, seance=None, exo=None):
        if feuille is None:
            if seance is not None:
                feuille = seance.feuille
        if classe is None:
            if feuille is not None:
                classe = feuille.classe
        source = self.GetLatexSource(classe=classe, feuille=feuille, seance=seance, exo=exo)
        return self.sourceToBitmap(source)

    @amutils.debug
    def codeToBitmap(self, code):
        source = self.GetLatexSource(code=code)
        return self.sourceToBitmap(source)

    @amutils.debug
    def sourceToBase64(self, source):
        import base64
        tmpDir, pdfFileName = self.Compile(source)
        if pdfFileName is None:
            return False
        pngFileName = os.path.join(tmpDir, "out.png")
        self.__CopyFile(tmpDir, pdfFileName, pngFileName)

        with open(pngFileName, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
        if self.deleteTmp:
            shutil.rmtree(tmpDir)
        return encoded_string

    @amutils.debug
    def sourceToBitmap(self, source):
        tmpDir, pdfFileName = self.Compile(source)
        if pdfFileName is None:
            return None, self.__output
        pngFileName = os.path.join(tmpDir, "out.png")
        self.__CopyFile(tmpDir, pdfFileName, pngFileName)
        return wx.Image(pngFileName).ConvertToBitmap(), None

    @amutils.debug
    def Compile(self, code):
        """
        Compile the latex Code in a tempDir
        :param code:
        :return: tempDir, pdfFilePath
        """
        tmpDir = tempfile.mkdtemp()
        basePath = os.path.join(tmpDir, "out")
        latexFileName = "{}.tex".format(basePath)
        pdfFileName = "{}.pdf".format(basePath)
        # pngFileName = "{}.png".format(baseName)

        # TODO : use option
        # pngDestPath = os.path.join("thumbs/", pngBaseName)

        if not self.deleteTmp:
            print("création du fichier '{}'".format(latexFileName))
        with open(latexFileName, 'w') as f:
            f.write(code)
        # TODO: Gestion des erreurs
        try:
            self.__output = subprocess.check_output(
                "{} -halt-on-error -interaction=nonstopmode -output-directory {} {}"
                .format(self.userConfig.GetValue('Paths', 'pdflatex'),
                        tmpDir,
                        latexFileName),
                stderr=subprocess.STDOUT,
                shell=True)
        except subprocess.CalledProcessError as error:
            self.__output = error.output
            return None, None
        return tmpDir, pdfFileName


    @amutils.debug
    def GetLatexSource(self, *, classe=None, feuille=None, seance=None, exo=None, code=None):
        return self.GetLatexSourcePart(part=self.type,
                                       classe=classe,
                                       feuille=feuille,
                                       seance=seance,
                                       exo=exo,
                                       code=code)

    def GetOutfiles(self):
        return self.__outfiles

    @amutils.debug
    def GetLatexSourcePart(self,
                           part, *,
                           classe=None,
                           feuille=None,
                           seance=None,
                           exo=None,
                           first=None,
                           code=None):
        out = ""
        if part == "FIGURE":
            out = self.ReplaceVars(self.GetPart(part),
                                   code=code)
        if part == "QUESTION":
            out = self.ReplaceVars(self.GetPart(part),
                                   classe=classe,
                                   feuille=feuille,
                                   seance=seance,
                                   exo=exo,
                                   first=first)
        elif part == "SEANCE":
            out = self.GetPart("SEANCE")
            if out.find("[[QUESTIONS]]") >= 0:
                exosString = ""
                firstExo = True
                for exo in seance.exos:
                    exosString += self.GetLatexSourcePart("QUESTION",
                                                          classe=classe,
                                                          feuille=feuille,
                                                          seance=seance,
                                                          exo=exo,
                                                          first=firstExo)
                    firstExo = False
                out = out.replace("[[QUESTIONS]]", exosString)
            out = self.ReplaceVars(out,
                                   classe=classe,
                                   feuille=feuille,
                                   seance=seance,
                                   first=first)
        elif part == "FEUILLE":
            out = self.parts[part]
            if out.find("[[SEANCES]]") >= 0:
                seancesString = ""
                firstSeance = True
                for seance in feuille.seances:
                    seancesString += self.GetLatexSourcePart(
                        "SEANCE",
                        classe=classe,
                        feuille=feuille,
                        seance=seance,
                        first=firstSeance)
                    firstSeance = False
                out = out.replace("[[SEANCES]]", seancesString)
            out = self.ReplaceVars(out, classe=classe, feuille=feuille)
        return out

    def GetOutput(self):
        return self.__output

    @staticmethod
    @amutils.debug
    def __CopyFile(tmpDir, source, dest):
        dest = dest.strip()
        ext = dest[-4:]
        tmpFile = source
        if ext == ".png":
            tmpFile = os.path.join(tmpDir, "out.png")
            pdf2image.convert_from_path(
                source,
                dpi=140,
                single_file=True,
                transparent=True,
                fmt='png',
                output_folder=tmpDir,
                output_file="out")
        if dest != tmpFile:
            if os.path.isfile(dest):
                os.remove(dest)
            shutil.copy(tmpFile, dest)

    @staticmethod
    def ReplaceVars(out, *,
                    classe=None,
                    feuille=None,
                    seance=None,
                    exo=None,
                    first=None,
                    code=None):
        # TODO: add [[QUESTION_1]] [[QUESTION_2]] ...
        """
        Subtistute all the [[vars]] in a LaTeX source, before compilation

        Possibles vars :
        [[QUESTION]]: the question latex code
        [[CLASSE]]: the classe name
        [[ANNEE_SCO]]: the school year fromated
        [[NFEUILLE]]: the sheet number
        [[NFEUILLE_n]]: the sheet number, encode on n digits with leading 0
        [[NSEANCE]]: the seance number
        [[NSEANCE_n]]: the seance number, encode on n digits with leading 0
        [[NQUESTION]]: the question number, in the seance
        [[NQUESTION_n]]: the question number, in the seance,
            encode on n digits with leading 0
        [[IDQUESTION]] : the question id
        [[IDQUESTION_n]] : the question id, encode on n digits with leading 0
        [[NIVEAU]]: the level name
        [[IMAGES_DIR]]: the images directory AbsolutePath
        [[THUMBS_DIR]]: the thumbs directory AbsolutePath
        [[NOT_FIRST]] something [[END_NOT_FIRST]]: when used with loop
            on Seances or on questions, write 'something' only at
            the non-first loops
        [[ONLY_FIRST]] something [[END_ONLY_FIRST]]: when used with loop on
            Seances or on questions, write 'something' only at the first loop

        Parameters
        ----------
        str : TYPE
            DESCRIPTION.
        classe : TYPE
            DESCRIPTION.
        feuille : TYPE
            DESCRIPTION.
        seance : TYPE
            DESCRIPTION.
        exo : TYPE
            DESCRIPTION.

        Returns
        -------
        str : TYPE
            DESCRIPTION.

        """
        # out = inputString;
        # if (sheet == null && seance !=null) {
        #     sheet = seance.getSheet();
        # }

        if out.find('[[ENTETE]]') >= 0:
            out = out.replace('[[ENTETE]]', LatexModel.ReplaceVars(
                              LatexModel.FromId(0).parts['HEADER'],
                              classe=classe,
                              feuille=feuille,
                              seance=seance,
                              exo=exo))
        if out.find('[[FIGURE_CODE]]') >= 0:
            out = out.replace('[[FIGURE_CODE]]', code)
        out = out.replace("[[IMAGES_DIR]]",
                          LatexModel.userConfig.GetValue('Paths', 'ImagesDir')
                          + "/")
        out = out.replace("[[THUMBS_DIR]]",
                          LatexModel.userConfig.GetValue('Paths', 'ThumbsDir')
                          + "/")
        if classe is not None:
            out = out.replace("[[CLASSE]]", classe.label)
            out = out.replace("[[NIVEAU]]", classe.niveau.label)
            if classe.anneeFin is not None \
               and classe.anneeFin != classe.anneeDebut:
                annee_sco = "{}-{}".format(classe.anneeDebut,
                                           classe.anneeFin)
            else:
                annee_sco = str(classe.anneeDebut)
            out = out.replace("[[ANNEE_SCO]]", annee_sco)

        if feuille is not None:
            out = out.replace("[[NFEUILLE]]", str(feuille.position+1))
            match = re.search("\\[\\[NFEUILLE_(\\d+)\\]\\]", out)
            while match is not None:
                ndigits = match.group(1)
                out = out.replace(
                    "[[NFEUILLE_{}]]".format(ndigits),
                    ("{" + ":0{}d".format(ndigits) + "}")
                    .format(feuille.position))
                match = re.search("\\[\\[NFEUILLE_(\\d+)\\]\\]", out)
        if seance is not None:
            out = out.replace("[[NSEANCE]]", str(seance.position+1))
            match = re.search("\\[\\[NSEANCE_(\\d+)\\]\\]", out)
            while match is not None:
                ndigits = match.group(1)
                out = out.replace(
                    "[[NSEANCE_{}]]".format(ndigits),
                    ("{" + ":0{}d".format(ndigits) + "}").format(seance.position))
                match = re.search("\\[\\[NSEANCE_(\\d+)\\]\\]", out)

    #         // TODO : add NQUESTION and NQUESTION_n parameters
    # //         if (nquestion != null) {
    # //             out = out.replace("[[NQUESTION]]");
    # //             Pattern nseancePattern = Pattern.compile("\\[\\[NQUESTION_(\\d+)\\]\\]");
    # //             Matcher matcher = nseancePattern.matcher(out);
    # //             while(matcher.find()) {
    # //                 String nbDigits = matcher.group(1);
    # //                 out = out.replace(String.format("[[NQUESTION_%s]]",nbDigits), 
    # //                                   String.format(String.format("%%0%sd", nbDigits), 
    # //                                         question.getId());
    # //                 matcher = nseancePattern.matcher(out);
    # //             }
    # //         }
        if exo is not None:
            out = out.replace("[[IDQUESTION]]", str(exo.id))
            match = re.search("\\[\\[IDQUESTION_(\\d+)\\]\\]", out)
            while match is not None:
                ndigits = match.group(1)
                id = exo.id
                if id is None:
                    out = out.replace("[[IDQUESTION_{}]]".format(ndigits),
                                      "{None}")
                else:
                    out = out.replace(
                        "[[IDQUESTION_{}]]".format(ndigits),
                        ("{" + ":0{}d".format(ndigits) + "}")
                        .format(exo.id))
                match = re.search("\\[\\[IDQUESTION_(\\d+)\\]\\]", out)
        # if (out.contains("[[QUESTION]]")) {
        if exo is None:
            out = out.replace("[[QUESTION]]", "~")
        else:
            out = out.replace("[[QUESTION]]", exo.code)

        if first is not None:
            if first:
                out = re.sub(
                    "\\[\\[NOT_FIRST\\]\\].*?\\[\\[END_NOT_FIRST\\]\\]",
                    "",
                    out)
                out = re.sub("\\[\\[(END_|)ONLY_FIRST\\]\\]", "", out)
            else:
                out = re.sub("\\[\\[(END_|)NOT_FIRST\\]\\]", "", out)
                out = re.sub(
                    "\\[\\[ONLY_FIRST\\]\\].*?\\[\\[END_ONLY_FIRST\\]\\]",
                    "",
                    out)
        return out

    def Save(self, commit=True):
        super().Save(commit=False)
        vars = (self.id,)
        if self.__parts is not None:
            amutils.DBObject.connexion.execute(
                'DELETE FROM "latexModelParts" WHERE "latexModelId"=?;', vars)
            for part in self.__parts.keys():
                partLatex = self.__parts[part]
                if partLatex is not None and partLatex.strip(' \t\n\r') != '':
                    vars = (self.id, part, partLatex)
                    amutils.DBObject.connexion.execute(
                        """INSERT INTO "latexModelParts"
                            ("latexModelId", "type", "content")
                            VALUES (?, ?, ?);""", vars)
        if commit:
            amutils.DBObject.connexion.commit()
