#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 21:57:48 2020

@author: guill
"""

import amutils


class Feuille(amutils.DBObject):
    table = "feuilles"

    def __init__(self, data=None):
        super().__init__(data)
        self._seances = None

    def Copy(self):
        newFeuille = amutils.Feuille()
        newFeuille._seances = [seance.Copy() for seance in self.seances]
        return newFeuille

    def IsEmpty(self):
        return len(self.seances) == 0

    def IsEvaluated(self):
        for seance in self.seances:
            if seance.IsEvaluated():
                return True
        return False

    def IsFirst(self):
        return self.position == 0

    def IsLast(self):
        return self.position == len(self.classe.feuilles) - 1

    def ReloadSeances(self):
        self._seances = None

    @property
    def seances(self):
        if self._seances is None:
            self._seances = []
            vars = (self.id,)
            sql = """SELECT "id"
                     FROM "seances"
                     WHERE "feuilleId"=?
                     ORDER BY "position";"""
            for row in amutils.DBObject.connexion.execute(sql, vars):
                self._seances.append(amutils.Seance.FromId(row['id']))
        return self._seances

    def Save(self, commit=True):
        super().Save(commit=False)
        if commit:
            amutils.DBObject.connexion.commit()
        self._dirty = False
