#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 09:20:10 2020

@author: guill
"""

import amutils


class Classe(amutils.DBObject):
    table = "classes"

    @property
    def seances(self):
        seances = []
        vars = (self.id,)
        for row in amutils.DBObject.connexion.execute(
                """SELECT S."id"
                    FROM "seances" S
                        LEFT JOIN "feuilles" F ON F."id"=S."feuilleId"
                        LEFT JOIN "classes" C ON C."id"=F."classeId"
                    WHERE C."id"=?
                    ORDER BY F."position", S."position" """, vars):
            seances.append(amutils.Seance.FromId(row['id']))
        return seances

    @property
    def feuillesCount(self):
        vars = (self.id,)
        row = amutils.DBObject.connexion.execute(
            """SELECT COUNT(F."id")
               FROM "feuilles" F
                    LEFT JOIN "classes" C ON C."id"=F."classeId"
               WHERE C."id"=?
               ORDER BY F."position" """, vars).fetchone()
        return row[0]

    @property
    def feuilles(self):
        feuilles = []
        vars = (self.id,)
        for row in amutils.DBObject.connexion.execute(
                """SELECT F."id"
                    FROM "feuilles" F
                        LEFT JOIN "classes" C ON C."id"=F."classeId"
                    WHERE C."id"=?
                    ORDER BY F."position" """, vars):
            feuilles.append(amutils.Feuille.FromId(row['id']))
        return feuilles

    @staticmethod
    def GetListByYear(year):
        list = []
        vars = (year,)
        for row in amutils.DBObject.connexion.execute(
                """SELECT C."id" FROM "classes"  C
                        JOIN "niveaux" N ON C."niveauId"=N."id"
                    WHERE C."anneeDebut"=?
                    ORDER BY N."position";""", vars):
            list.append(Classe.FromId(row['id']))
        return list

    @staticmethod
    def GetAnnees():
        annees = []
        for row in amutils.DBObject.connexion.execute(
                """SELECT DISTINCT "anneeDebut"
                    FROM "classes"
                    ORDER BY "anneeDebut" DESC;"""):
            annees.append(row['anneeDebut'])
        return annees
