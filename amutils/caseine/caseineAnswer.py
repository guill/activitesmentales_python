"""
Created on 26/03/20

@author: GuiΩ
"""

import amutils


class CaseineAnswer(amutils.DBObject):
    table = 'caseineAnswers'

    def Copy(self):
        newA = super().Copy()
        newA.exo = None
        return newA

    def _getattr(self, attr):
        if attr == 'xml':
            return """
\t\t<answer fraction="{a.note}" format="moodle_auto_format">
\t\t\t<text><![CDATA[{a.answer}]]></text>
\t\t\t<feedback format="html">
\t\t\t\t<text><![CDATA[{a.feedback}]]></text>
\t\t\t</feedback>
\t\t</answer>""".format(a=self)
        else:
            return super()._getattr(attr)
