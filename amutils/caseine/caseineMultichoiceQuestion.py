"""
Created on 22/03/20

@author: GuiΩ
"""

import amutils
from amutils import caseine


class CaseineMultichoiceQuestion(amutils.DBObject):
    table = 'caseineMultichoiceQuestions'

    def _getattr(self, attr):
        if attr == 'xml':
            return self._GetXML()
        else:
            return super()._getattr(attr)

    @staticmethod
    def FromExo(exo, forceReload=False):
        vars = (exo.id,)
        sql = """SELECT * FROM "caseineMultichoiceQuestions"
                WHERE "exoId"=?;"""
        if amutils.DEBUG_SQL:
            print(sql, vars)
        row = amutils.DBObject.connexion.execute(sql, vars).fetchone()
        multichoiceQ = CaseineMultichoiceQuestion(row, forceReload)
        multichoiceQ._dirty = False
        return multichoiceQ

    def _GetXML(self):
        return """
\t\t<single>{single}</single>
\t\t<shuffleanswers>{shuffleanswers}</shuffleanswers>
\t\t<answernumbering>{q.answernumbering}</answernumbering>
\t\t<correctfeedback format="html">
\t\t\t<text>{q.correctfeedback}</text>
\t\t</correctfeedback>
\t\t<partiallycorrectfeedback format="html">
\t\t\t<text>{q.partiallycorrectfeedback}</text>
\t\t</partiallycorrectfeedback>
\t\t<incorrectfeedback format="html">
\t\t\t<text>{q.incorrectfeedback}</text>
\t\t</incorrectfeedback>""".format(single=('false', 'true')[self.single], shuffleanswers=('false', 'true')[self.shuffleanswers], q=self)

