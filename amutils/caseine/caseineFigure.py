"""
Created on 24/03/20

@author: GuiΩ
"""

import amutils
import wx

class CaseineFigure(amutils.DBObject):
    table = 'caseineFigures'

    def Copy(self):
        newF = super().Copy()
        newF.exo = None
        return newF

    def _getattr(self, attr):
        if attr == 'thumb':
            return self._GetThumb()
        elif attr == 'xml':
            return self._GetXML()
        else:
            return super()._getattr(attr)

    def _GetThumb(self):
        if self.code.strip() == '':
            return wx.Bitmap(), None
        else:
            return amutils.LatexModel.FromId(-2).codeToBitmap(self.code)

    def _GetXML(self):
        return '\n\t\t\t<file name="{}" path="/" encoding="base64">{}</file>'.format(
                self.name,
                amutils.LatexModel.FromId(-2).codeToBase64(self.code))



