#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 15:27:42 2020

@author: GuiΩ
"""

import amutils
from amutils import caseine

import wx


class ExoDialogPanel(wx.Panel):

    def __init__(self, parent, controller, exoDialog, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.controller = controller
        self.exoDialog = exoDialog
        self.exo = None
        self.statusCtrl = wx.RadioBox(self, choices=caseine.EXPORT_STATUS)
        self.typeQuestionLabel = wx.StaticText(self, wx.ID_ANY, 'Type de question :')
        self.typeQuestionCtrl = wx.ComboBox(self, style=wx.CB_READONLY, choices=list(caseine.CASEINE_TYPES.keys()))
        self.caseineQPanel = caseine.ui.CaseineQuestionPanel(self, self.exoDialog)
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_RADIOBOX, self.OnStatusChanged, self.statusCtrl)
        self.Bind(wx.EVT_COMBOBOX, self.OnTypeQuestionChanged, self.typeQuestionCtrl)

    def __set_properties(self):
        self.caseineQPanel.Show(False)
        self.typeQuestionCtrl.SetSelection(0)
        self.typeQuestionCtrl.SetMinSize((200, -1))
        self.statusCtrl.EnableItem(False)
        self.typeQuestionLabel.Hide()
        self.typeQuestionCtrl.Hide()

    def __do_layout(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.statusCtrl, 0, wx.ALL, 3)
        typeSizer = wx.BoxSizer(wx.HORIZONTAL)
        typeSizer.Add(self.typeQuestionLabel, 0, wx.ALL, 3)
        typeSizer.Add(self.typeQuestionCtrl, 0, wx.ALL, 3)
        sizer.Add(typeSizer, 0, wx.ALL, 3)
        sizer.Add(self.caseineQPanel, 1, wx.ALL | wx.EXPAND, 3)
        self.SetSizer(sizer)
        sizer.FitInside(self)
        self.Layout()

    def SetExo(self, exo):
        self.exo = exo
        self.caseineQPanel.SetExo(exo)
        if exo.caseineQuestion is not None:
            self.statusCtrl.SetSelection(caseine.EXPORT_STATUS.index(exo.caseineQuestion.status))
            self.statusCtrl.EnableItem(2, exo.caseineQuestion.status == 'exported')
            self.typeQuestionCtrl.SetValue(exo.caseineQuestion.type)
            self._StatusChanged(exo.caseineQuestion.status)
            self._TypeChanged()

    def Validate(self):
        status = self.statusCtrl.GetString(self.statusCtrl.GetSelection())
        caseineQuestion = self.caseineQPanel.GetData(self.typeQuestionCtrl.GetValue())
        caseineQuestion.status = status
        self.exo.caseineQuestion = caseineQuestion
        self.exo.Save()

    def OnStatusChanged(self, event):
        status = self.statusCtrl.GetString(self.statusCtrl.GetSelection())
        self._StatusChanged(status)


    def OnTypeQuestionChanged(self, event):
        self._TypeChanged()

    def _TypeChanged(self):
        type = self.typeQuestionCtrl.GetValue()
        self.caseineQPanel.answerprefixLabel.Show(type == 'algebra')
        self.caseineQPanel.answerprefixCtrl.Show(type == 'algebra')
        self.caseineQPanel.variablesLabel.Show(type == 'algebra')
        self.caseineQPanel.variablesPanel.Show(type == 'algebra')
        self.caseineQPanel.addVariableButton.Show(type == 'algebra')
        self.caseineQPanel.addClozeAnswerButton.Show(type == 'cloze')
        self.caseineQPanel.answersLabel.Show(type != 'cloze')
        self.caseineQPanel.answersPanel.Show(type != 'cloze')
        self.caseineQPanel.addAnswerButton.Show(type != 'cloze')
        self.caseineQPanel.singleCtrl.Show(type == 'multichoice')
        self.caseineQPanel.shuffleanswersCtrl.Show(type == 'multichoice')
        self.caseineQPanel.answernumberingLabel.Show(type == 'multichoice')
        self.caseineQPanel.answernumberingCtrl.Show(type == 'multichoice')
        self.caseineQPanel.correctfeedbackLabel.Show(type == 'multichoice')
        self.caseineQPanel.correctfeedbackCtrl.Show(type == 'multichoice')
        self.caseineQPanel.partiallycorrectfeedbackLabel.Show(type == 'multichoice')
        self.caseineQPanel.partiallycorrectfeedbackCtrl.Show(type == 'multichoice')
        self.caseineQPanel.incorrectfeedbackLabel.Show(type == 'multichoice')
        self.caseineQPanel.incorrectfeedbackCtrl.Show(type == 'multichoice')
        self.Layout()


    @amutils.debug
    def _StatusChanged(self, status):
        if status == 'not exported':
            self.caseineQPanel.Hide()
        elif status == 'to export':
            if self.typeQuestionCtrl.GetValue() != '':
                self.caseineQPanel.Show()
                self.caseineQPanel.Enable()
        else:
            if self.typeQuestionCtrl.GetValue() != '':
                self.caseineQPanel.Show()
                self.caseineQPanel.Enable(False)
        self.typeQuestionLabel.Show(status == 'to export')
        self.typeQuestionCtrl.Show(status == 'to export')
        self._TypeChanged()
        self.GetSizer().FitInside(self)
        self.Layout()

    def __repr__(self):
        return self.__class__.__name__
