"""
Created on 26/03/20

@author: GuiΩ
"""

import wx
from amutils import caseine

class AnswerPanel(wx.Panel):
    def __init__(self, parent, answer=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.answerCtrl = wx.TextCtrl(self, wx.ID_ANY)
        self.noteCtrl = wx.SpinCtrl(self, wx.ID_ANY, min=-100, max=100)
        # TODO: html text editor for feedback
        self.feedbackCtrl =  wx.TextCtrl(self, wx.ID_ANY, style=wx.HSCROLL | wx.TE_MULTILINE)
        self.__set_properties()
        self.__do_layout()
        self.SetAnswer(answer)

    def __set_properties(self):
        self.noteCtrl.SetMinSize((120, -1))
        self.feedbackCtrl.SetMinSize((300, 50))

    def __do_layout(self):
        sizer = wx.FlexGridSizer(2, wx.Size(3, 3))
        staticTxt = wx.StaticText(self, wx.ID_ANY, 'Réponse :')
        sizer.Add(staticTxt, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        sizer.Add(self.answerCtrl, 0, wx.EXPAND | wx.ALL, 3)

        staticTxt = wx.StaticText(self, wx.ID_ANY, 'Note :')
        sizer.Add(staticTxt, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        sizer.Add(self.noteCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)

        staticTxt = wx.StaticText(self, wx.ID_ANY, 'Feedback :')
        sizer.Add(staticTxt, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        sizer.Add(self.feedbackCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        self.SetSizer(sizer)
        self.Layout()

    def SetAnswer(self, answer):
        if answer is None:
            answer = caseine.CaseineAnswer()
        self.answer = answer
        self.answerCtrl.SetValue(answer.answer)
        self.noteCtrl.SetValue(answer.note)
        self.feedbackCtrl.SetValue(answer.feedback)
        self.Layout()

    def GetData(self):
        data = caseine.CaseineAnswer({
            'answer': self.answerCtrl.GetValue(),
            'note': self.noteCtrl.GetValue(),
            'feedback': self.feedbackCtrl.GetValue()
        })
        return data
