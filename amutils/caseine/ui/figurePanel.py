"""
Created on 25/03/20

@author: GuiΩ
"""

import amutils
from amutils import caseine
import wx


class FigurePanel(wx.Panel):
    def __init__(self, parent, figure=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.nameTxt = wx.StaticText(self, wx.ID_ANY)
        self.codeCtrl = wx.TextCtrl(self, wx.ID_ANY, style=wx.HSCROLL | wx.TE_MULTILINE)
        self.button = wx.Button(self, wx.ID_ANY, label='Aperçu')
        self.thumbCtrl = wx.StaticBitmap(self, wx.ID_ANY, wx.NullBitmap)
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_BUTTON, self.OnPreviewClick)
        self.codeCtrl.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.SetFigure(figure)

    def __set_properties(self):
        self.thumbCtrl.SetBackgroundColour(wx.Colour(235,235,255))
        self.codeCtrl.SetMinSize((400, 100))

    def __do_layout(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.nameTxt, 0, wx.ALL, 3)
        sizer.Add(self.codeCtrl, 0, wx.ALL, 3)
        sizer.Add(self.button, 0, wx.ALL, 3)
        sizer.Add(self.thumbCtrl, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 3)
        self.SetSizer(sizer)
        self.Layout()

    def OnPreviewClick(self, event):
        self._UpdateThumb()

    def SetFigure(self, figure):
        if figure is None:
            figure = caseine.CaseineFigure()
        self.figure = figure
        self.nameTxt.SetLabel(figure.name)
        self.codeCtrl.SetValue(figure.code)
        self._UpdateThumb()

    def GetData(self):
        self.figure.name = self.nameTxt.GetLabel()
        self.figure.code = self.codeCtrl.GetValue()
        return self.figure

    def _UpdateThumb(self):
        self.figure.code = self.codeCtrl.GetValue()
        thumb, msg = self.figure.thumb
        if msg is not None:
            msgD = amutils.ui.LongMessageDialog(self)
            msgD.SetContent("Sortie de la commande", msg)
            # TODO: Scroll to the end of the content
            msgD.ShowModal()
            msgD.Destroy()
        else:
            self.thumbCtrl.SetBitmap(thumb)
            self.GetParent().GetParent().Layout()
            self.GetParent().GetParent().GetParent().Layout()

    def OnKeyDown(self, event):
        if event.GetKeyCode() == wx.WXK_F5:
            self._UpdateThumb()
        event.Skip()
