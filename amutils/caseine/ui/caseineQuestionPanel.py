"""
Created on 22/03/20

@author: GuiΩ
"""

from amutils import caseine
import re
import wx


class CaseineQuestionPanel(wx.ScrolledWindow):
    def __init__(self, parent, exoDialog, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.exoDialog = exoDialog
        self.answerPanels = []
        self.variablePanels = []
        self.figurePanels = []
        self.caseineQ = None
        self.questiontxtCtrl = wx.TextCtrl(self, wx.ID_ANY, style=wx.HSCROLL | wx.TE_MULTILINE)
        self.questionImportButton = wx.Button(self, wx.ID_ANY, label='Importer le texte de la question')
        self.addClozeAnswerButton = wx.Button(self, wx.ID_ANY, label='Ajouter une réponse')
        self.generalfeedbackCtrl = wx.TextCtrl(self, wx.ID_ANY, style=wx.HSCROLL | wx.TE_MULTILINE)
        self.answerprefixLabel = wx.StaticText(self, wx.ID_ANY, 'Préfixe de réponse :')
        self.answerprefixCtrl = wx.TextCtrl(self, wx.ID_ANY, style=wx.EXPAND)
        self.singleCtrl = wx.CheckBox(self, wx.ID_ANY, label='Réponse unique')
        self.shuffleanswersCtrl = wx.CheckBox(self, wx.ID_ANY, label='Mélanger les réponses')
        self.answernumberingLabel =  wx.StaticText(self, wx.ID_ANY, 'Numérotation :')
        self.answernumberingCtrl = wx.ComboBox(self, wx.ID_ANY, choices=caseine.MULTICHOICE_NUMBERING_OPTIONS)
        self.correctfeedbackLabel = wx.StaticText(self, wx.ID_ANY, 'Feedback pour\nréponses correctes :')
        self.correctfeedbackCtrl = wx.TextCtrl(self, wx.ID_ANY, style=wx.EXPAND)
        self.partiallycorrectfeedbackLabel = wx.StaticText(self, wx.ID_ANY, 'Feedback pour\nréponses partiellement correctes :')
        self.partiallycorrectfeedbackCtrl = wx.TextCtrl(self, wx.ID_ANY, style=wx.HSCROLL | wx.TE_MULTILINE)
        self.incorrectfeedbackLabel = staticTxt = wx.StaticText(self, wx.ID_ANY, 'Feedback pour \nréponses incorrectes :')
        self.incorrectfeedbackCtrl = wx.TextCtrl(self, wx.ID_ANY, style=wx.EXPAND)
        self.answersLabel = wx.StaticText(self, wx.ID_ANY, 'Réponses :')
        self.answersPanel = wx.Panel(self, wx.ID_ANY, style=wx.BORDER_SIMPLE | wx.TAB_TRAVERSAL)
        self.addAnswerButton = wx.Button(self, wx.ID_ANY, label='Ajouter une réponse')
        self.variablesLabel =  wx.StaticText(self, wx.ID_ANY, 'Variables :')
        self.variablesPanel = wx.Panel(self, wx.ID_ANY, style=wx.BORDER_SIMPLE | wx.TAB_TRAVERSAL)
        self.addVariableButton = wx.Button(self, wx.ID_ANY, label='Ajouter une variable')
        self.figuresPanel =  wx.Panel(self, wx.ID_ANY, style=wx.BORDER_SIMPLE | wx.TAB_TRAVERSAL)
        self.addFigureButton = wx.Button(self, wx.ID_ANY, label='Ajouter une figure')
        self.focus = self.questiontxtCtrl
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_BUTTON, self.OnQuestionImportClick, self.questionImportButton)
        self.Bind(wx.EVT_BUTTON, self.OnAddClozeAnswerClick, self.addClozeAnswerButton)
        self.Bind(wx.EVT_BUTTON, self.OnAddVariableClick, self.addVariableButton)
        self.Bind(wx.EVT_BUTTON, self.OnAddAnswerClick, self.addAnswerButton)
        self.Bind(wx.EVT_BUTTON, self.OnAddFigureClick, self.addFigureButton)
        self.Bind(wx.EVT_CHILD_FOCUS, self.OnFocus)

    def __set_properties(self):
        self.SetScrollRate(10, 10)
        self.questiontxtCtrl.SetMinSize((400, 100))
        self.generalfeedbackCtrl.SetMinSize((400, 100))
        self.answerprefixCtrl.SetMinSize((400, -1))
        self.correctfeedbackCtrl.SetMinSize((400, 100))
        self.partiallycorrectfeedbackCtrl.SetMinSize((400, 100))
        self.incorrectfeedbackCtrl.SetMinSize((400, 100))
        self.answernumberingCtrl.SetValue('none')


    def __do_layout(self):
        hSizer = wx.BoxSizer(wx.HORIZONTAL)
        fgSizer = wx.FlexGridSizer(2, wx.Size(3,3))

        staticTxt = wx.StaticText(self, wx.ID_ANY, 'Texte de la question :')
        fgSizer.Add(staticTxt, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.questiontxtCtrl,  0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add((0,0))
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add(self.questionImportButton, 0, wx.ALL, 3)
        btnSizer.Add(self.addClozeAnswerButton, 0, wx.ALL, 3)
        fgSizer.Add(btnSizer, 0, wx.ALL, 3)

        fgSizer.Add((0,0))
        fgSizer.Add(self.singleCtrl,  0, wx.ALL, 3)

        fgSizer.Add((0,0))
        fgSizer.Add(self.shuffleanswersCtrl, 0, wx.ALL, 3)

        fgSizer.Add(self.answernumberingLabel, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.answernumberingCtrl,  0, wx.ALL, 3)

        staticTxt = wx.StaticText(self, wx.ID_ANY, 'Feedback Général :')
        fgSizer.Add(staticTxt, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.generalfeedbackCtrl,  0, wx.ALIGN_LEFT | wx.ALL, 3)

        fgSizer.Add(self.answerprefixLabel, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.answerprefixCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)

        fgSizer.Add(self.variablesLabel, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.addVariableButton, 0, wx.ALL, 3)
        fgSizer.Add((0,0))
        variablesSizer = wx.BoxSizer(wx.VERTICAL)
        self.variablesPanel.SetSizer(variablesSizer)
        self.AddVariablePanel()
        fgSizer.Add(self.variablesPanel, 0, wx.ALL, 3)

        fgSizer.Add(self.answersLabel, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.addAnswerButton, 0, wx.ALL, 3)
        fgSizer.Add((0,0))
        answersSizer = wx.BoxSizer(wx.VERTICAL)
        self.answersPanel.SetSizer(answersSizer)
        self.AddAnswerPanel()
        fgSizer.Add(self.answersPanel, 0, wx.ALL, 3)
        hSizer.Add(fgSizer, 0, wx.ALL, 3)

        fgSizer.Add(self.correctfeedbackLabel, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.correctfeedbackCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)

        fgSizer.Add(self.partiallycorrectfeedbackLabel, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.partiallycorrectfeedbackCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)

        fgSizer.Add(self.incorrectfeedbackLabel, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        fgSizer.Add(self.incorrectfeedbackCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)

        vSizer = wx.BoxSizer(wx.VERTICAL)
        figuresSizer = wx.BoxSizer(wx.VERTICAL)
        self.figuresPanel.SetSizer(figuresSizer)
        vSizer.Add(self.figuresPanel, 0, wx.ALL, 3)
        vSizer.Add(self.addFigureButton, 0, wx.ALL, 3)
        hSizer.Add(vSizer, 0, wx.ALL, 3)
        self.SetSizer(hSizer)
        self.Layout()

    def SetExo(self, exo):
        self.exo = exo
        self.caseineQ = exo.caseineQuestion
        if self.caseineQ is not None:
            self.questiontxtCtrl.SetValue(self.caseineQ.questiontxt)
            self.generalfeedbackCtrl.SetValue(self.caseineQ.generalfeedback)
            self.ClearAnswers()
            self.ClearFigures()
            self.ClearVariables()
            for variable in self.caseineQ.variables:
                self.AddVariablePanel(variable)
            for answer in self.caseineQ.answers:
                self.AddAnswerPanel(answer)
            for figure in self.caseineQ.figures:
                self.AddFigurePanel(figure)
            if exo.caseineQuestion.type == 'algebra':
                self.answerprefixCtrl.SetValue(self.caseineQ.answerprefix)
            elif exo.caseineQuestion.type == 'multichoice':
                self.singleCtrl.SetValue(self.caseineQ.single)
                self.shuffleanswersCtrl.SetValue(self.caseineQ.shuffleanswers)
                self.answernumberingCtrl.SetValue(self.caseineQ.answernumbering)
                self.correctfeedbackCtrl.SetValue(self.caseineQ.correctfeedback)
                self.partiallycorrectfeedbackCtrl.SetValue(self.caseineQ.partiallycorrectfeedback)
                self.incorrectfeedbackCtrl.SetValue(self.caseineQ.incorrectfeedback)
        self.Layout()

    def GetData(self, type):
        caseineQ = caseine.CaseineQuestion()
        caseineQ.type = type
        caseineQ.questiontxt = self.questiontxtCtrl.GetValue()
        caseineQ.generalfeedback = self.generalfeedbackCtrl.GetValue()
        caseineQ.idnumber = str(self.exo.id)
        figures = []
        for panel in self.figurePanels:
            figure = panel.GetData()
            if figure.code != '':
                figures.append(figure)
        caseineQ.figures = figures
        answers = []
        if type != 'cloze':
            for panel in self.answerPanels:
                answerData = panel.GetData()
                if answerData.answer != '':
                    answers.append(answerData)
        caseineQ.answers = answers
        if type == 'algebra':
            caseineQ.data = caseine.CaseineAlgebraQuestion({
                'exo': self.exo,
                'answerprefix':  self.answerprefixCtrl.GetValue()})
            variables = []
            for panel in self.variablePanels:
                varData = panel.GetData()
                if varData.name != '':
                    variables.append(varData)
            caseineQ.variables = variables
        elif type == 'cloze':
            caseineQ.data = caseine.CaseineClozeQuestion()
        elif type == 'multichoice':
            caseineQ.single = self.singleCtrl.GetValue()
            caseineQ.shuffleanswers = self.shuffleanswersCtrl.GetValue()
            caseineQ.answernumbering = self.answernumberingCtrl.GetValue()
            caseineQ.correctfeedback = self.correctfeedbackCtrl.GetValue()
            caseineQ.partillycorrectfeedback = self.partiallycorrectfeedbackCtrl.GetValue()
            caseineQ.incorrectfeedback = self.incorrectfeedbackCtrl.GetValue()
        return caseineQ

    def ClearAnswers(self):
        self.answersPanel.GetSizer().Clear(True)
        self.answerPanels = []

    def AddAnswerPanel(self, answer=None):
        if len(self.answerPanels) > 0:
            sep = wx.StaticLine(self.answersPanel, wx.ID_ANY, style=wx.LI_HORIZONTAL | wx.EXPAND)
            sep.SetBackgroundColour(wx.LIGHT_GREY)
            self.answersPanel.GetSizer().Add(sep, 0, wx.EXPAND| wx.ALL, 3)
        answerPanel = caseine.ui.AnswerPanel(self.answersPanel, answer)
        if answer is None and len(self.answerPanels) > 0:
            answerPanel.noteCtrl.SetValue(0)
        self.answersPanel.GetSizer().Add(answerPanel, 0, wx.ALL, 3)
        self.answerPanels.append(answerPanel)
        self.GetParent().Layout()

    def ClearFigures(self):
        self.figuresPanel.GetSizer().Clear(True)
        self.figurePanels = []

    def AddFigurePanel(self, figure=None):
        if len(self.figurePanels) > 0:
            sep = wx.StaticLine(self.figuresPanel, wx.ID_ANY, style=wx.LI_HORIZONTAL | wx.EXPAND)
            sep.SetBackgroundColour(wx.LIGHT_GREY)
            self.figuresPanel.GetSizer().Add(sep, 0, wx.EXPAND| wx.ALL, 3)
        if figure is None:
            code = self.focus.GetStringSelection()
            if code.strip() == '':
                code = '\\begin{tikzpicture}[scale=2]\n\t\draw(0,0) node {figure};\n\end{tikzpicture}\n'
            figure = caseine.CaseineFigure({
                'name': "fig_am{}_{}.png".format(self.exo.id, len(self.figurePanels)+1),
                'code': code
            })
            self.focus.Replace(*self.questiontxtCtrl.GetSelection(),
                               '<center><img src="@@PLUGINFILE@@/{}" alt="" /></center><br>\n'.format(figure.name))
        figurePanel = caseine.ui.FigurePanel(self.figuresPanel, figure)
        self.figuresPanel.GetSizer().Add(figurePanel, 0, wx.ALL, 3)
        self.figurePanels.append(figurePanel)
        self.GetParent().Layout()

    def ClearVariables(self):
        self.variablesPanel.GetSizer().Clear(True)
        self.variablePanels = []

    def AddVariablePanel(self, variable=None):
        if len(self.variablePanels) > 0:
            sep = wx.StaticLine(self.variablesPanel, wx.ID_ANY, style=wx.LI_HORIZONTAL | wx.EXPAND)
            sep.SetBackgroundColour(wx.LIGHT_GREY)
            self.variablesPanel.GetSizer().Add(sep, 0, wx.EXPAND | wx.ALL, 3)
        variablePanel = VariablePanel(self.variablesPanel, variable)
        self.variablesPanel.GetSizer().Add(variablePanel, 0, 0, 3)
        self.variablePanels.append(variablePanel)
        self.GetParent().Layout()

    def OnFocus(self, event):
        if isinstance( event.GetWindow(), wx.TextCtrl):
            self.focus = event.GetWindow()

    def OnQuestionImportClick(self, event):
        if self.questiontxtCtrl.GetValue() != '' \
                or self.answerprefixCtrl.GetValue() != ''\
                or len(self.figurePanels) > 0:
            msgBox = wx.MessageDialog(self, 'Cela va écraser le contenu actuel', 'Attention', style=wx.OK | wx.CANCEL)
            if msgBox.ShowModal() != wx.ID_OK:
                return
        questiontxt, figureCodes = caseine.CaseineQuestion.ConvertExoCode(self.exo.id, self.exoDialog.mainPanel.codeCtrl.GetValue())
        if self.GetParent().typeQuestionCtrl.GetValue() == 'algebra':
            match = re.search("^(.*)\n([^\n]*=\\\\\\))[ \n]*", questiontxt, re.DOTALL)
            answerprefix = ''
            if match:
                questiontxt = match.group(1)
                answerprefix = match.group(2)
            questiontxt = re.sub("[\n ]*$", "", questiontxt)
            self.answerprefixCtrl.SetValue(answerprefix)
        self.questiontxtCtrl.SetValue(questiontxt)
        self.ClearFigures()
        for figName, figCode in figureCodes.items():
            figure = caseine.CaseineFigure({
                'name': figName,
                'code': figCode
            })
            self.AddFigurePanel(figure)

    def OnAddAnswerClick(self, event):
        self.AddAnswerPanel()

    def OnAddClozeAnswerClick(self, event):
        dialog = caseine.ui.ClozeAnswerDialog(self)
        if dialog.ShowModal() == wx.ID_CANCEL:
            return
        self.questiontxtCtrl.Replace(*self.questiontxtCtrl.GetSelection(), dialog.GetCode())

    def OnAddFigureClick(self, event):
        self.AddFigurePanel()

    def OnAddVariableClick(self, event):
        self.AddVariablePanel()


class VariablePanel(wx.Panel):
    def __init__(self, parent, variable=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        if variable is None:
            variable = caseine.CaseineVariable()
        self.nameCtrl = wx.TextCtrl(self, wx.ID_ANY)
        self.minCtrl = wx.TextCtrl(self, wx.ID_ANY)
        self.maxCtrl = wx.TextCtrl(self, wx.ID_ANY)
        self.__set_properties()
        self.__do_layout()
        self.SetVariable(variable)

    def __set_properties(self):
        pass

    def __do_layout(self):
        sizer = wx.FlexGridSizer(2, wx.Size(3, 3))
        staticTxt = wx.StaticText(self, wx.ID_ANY, 'Nom de variable:')
        sizer.Add(staticTxt, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        sizer.Add(self.nameCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)

        staticTxt = wx.StaticText(self, wx.ID_ANY, 'Valeur minimum :')
        sizer.Add(staticTxt, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        sizer.Add(self.minCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)

        staticTxt = wx.StaticText(self, wx.ID_ANY, 'Valeur maximum :')
        sizer.Add(staticTxt, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        sizer.Add(self.maxCtrl, 0, wx.ALIGN_LEFT | wx.ALL, 3)
        self.SetSizer(sizer)
        self.Layout()

    def SetVariable(self, variable):
        if variable is None:
            variable = caseine.CaseineVariable()
        self.variable = variable
        if variable is not None:
            self.nameCtrl.SetValue(variable.name)
            self.minCtrl.SetValue(str(variable.min))
            self.maxCtrl.SetValue(str(variable.max))
        self.Layout()

    def GetData(self):
        data = caseine.CaseineVariable({
            'name': self.nameCtrl.GetValue(),
            'min': self.minCtrl.GetValue(),
            'max': self.maxCtrl.GetValue()
        })
        return data
