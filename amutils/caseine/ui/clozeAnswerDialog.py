"""
Created on 25/03/20

@author: GuiΩ
"""

import wx

CLOZE_TYPES = ['Short', 'Numeric', 'QCM']
QCM_TYPES = ['DropDown', 'QCM Vertical', 'QCM Vertical (choix unique)', 'QCM Horizontal', 'QCM Horizontal (choix unique)']
QCM_CODES = ['MULTICHOICE', 'MULTIRESPONSE', 'MULTICHOICE_V', 'MULTIRESPONSE_H', 'MULTICHOICE_H']

class ClozeAnswerDialog(wx.Dialog):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mainPanel = wx.ScrolledWindow(self)
        self.typeCtrl = wx.RadioBox(self.mainPanel, choices=CLOZE_TYPES)
        self.caseLabel = wx.StaticText(self.mainPanel, label='Sensible à la case ?')
        self.caseCtrl = wx.RadioBox(self.mainPanel, choices=['Oui', 'Non'])
        self.qcmTypeLabel = wx.StaticText(self.mainPanel, label='Type de QCM ?')
        self.qcmTypeCtrl = wx.RadioBox(self.mainPanel, choices=QCM_TYPES, style=wx.RA_SPECIFY_ROWS )
        self.qcmShuffleLabel = wx.StaticText(self.mainPanel, label='Mélanger les réponses ?')
        self.qcmShuffleCtrl = wx.RadioBox(self.mainPanel, choices=['Oui', 'Non'])
        self.answersLabel = wx.StaticText(self.mainPanel, label='Réponses :')
        self.answersPanel = wx.Panel(self.mainPanel, wx.ID_ANY, style=wx.BORDER_SIMPLE | wx.TAB_TRAVERSAL)
        self.addButton = wx.Button(self.mainPanel, wx.ID_ANY, label='Ajouter une réponse')
        self.cancelButton = wx.Button(self, wx.ID_ANY, label='Annuler')
        self.validateButton = wx.Button(self, wx.ID_ANY, label='Valider')
        self.answerPanels = []
        self.__set_properties()
        self.__do_layout()
        self._UpdateLayout()
        self._AddAnswerPanel()
        self.Bind(wx.EVT_RADIOBOX, self.OnTypeChanged, self.typeCtrl)
        self.Bind(wx.EVT_BUTTON, self.OnAddAnswerClick, self.addButton)
        self.Bind(wx.EVT_BUTTON, self.OnValidateClick, self.validateButton)
        self.Bind(wx.EVT_BUTTON, self.OnCancelClick, self.cancelButton)


    def __set_properties(self):
        self.mainPanel.SetScrollRate(20,20)
        self.caseCtrl.SetSelection(1)
        self.SetMinSize((600,600))


    def __do_layout(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        fgSizer = wx.FlexGridSizer(2, wx.Size(3,3))
        text = wx.StaticText(self.mainPanel, label='Type de question :')
        fgSizer.Add(text, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 3)
        fgSizer.Add(self.typeCtrl, 0, wx.ALL , 3)
        fgSizer.Add(self.caseLabel, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 3)
        fgSizer.Add(self.caseCtrl, 0, wx.ALL, 3)
        fgSizer.Add(self.qcmTypeLabel, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 3)
        fgSizer.Add(self.qcmTypeCtrl, 0, wx.ALL, 3)
        fgSizer.Add(self.qcmShuffleLabel, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 3)
        fgSizer.Add(self.qcmShuffleCtrl, 0, wx.ALL, 3)
        fgSizer.Add(self.answersLabel, 0, wx.ALL, 3)
        vSizer = wx.BoxSizer(wx.VERTICAL)
        self.answersPanel.SetSizer(vSizer)
        fgSizer.Add(self.addButton, 0, wx.ALL, 3)
        fgSizer.Add((0,0))
        fgSizer.Add(self.answersPanel, 0, wx.ALL, 3)
        self.mainPanel.SetSizer(fgSizer)
        sizer.Add(self.mainPanel, 1, wx.ALL | wx.EXPAND, 3)
        hSizer = wx.BoxSizer(wx.HORIZONTAL)
        hSizer.Add(self.cancelButton, 0, wx.ALL, 3)
        hSizer.Add(self.validateButton, 0, wx.ALL, 3)
        sizer.Add(hSizer, 0, wx.ALL|wx.ALIGN_RIGHT, 3)
        self.SetSizer(sizer)
        self.Layout()
        fgSizer.FitInside(self.mainPanel)

    def GetCode(self):
        answers = ''
        for panel in self.answerPanels:
            if answers != '':
                answers += '~'
            answers += panel.GetCode()
        type = self.typeCtrl.GetSelection()
        if type == 0:   # shortanswer
            if self.caseCtrl.GetSelection() == 0:
                option = '_C'
            else:
                option = ''
            out = '{{1:SHORTANSWER{}:{}}}'.format(option, answers)
        elif type == 1: # numeric
            out = '{{1:NUMERICAL:{}}}'.format(answers)
        elif type == 2: # qcm
            qcmtype = QCM_CODES[self.qcmTypeCtrl.GetSelection()]
            option = ''
            if self.qcmShuffleCtrl.GetSelection() == 0:
                if qcmtype[-2] == '_':
                    option = 'S'
                else:
                    option = '_S'

            out = '{{1:{}{}:{}}}'.format(qcmtype, option, answers)
        return out

    def OnAddAnswerClick(self, event):
        self._AddAnswerPanel()

    def OnCancelClick(self,event):
        self.EndModal(wx.ID_CANCEL)

    def OnTypeChanged(self, event):
        self._UpdateLayout()

    def OnValidateClick(self,event):
        self.EndModal(wx.ID_OK)


    def _AddAnswerPanel(self):
        if len(self.answerPanels) > 0:
            sep = wx.StaticLine(self.answersPanel, wx.ID_ANY, style=wx.LI_HORIZONTAL | wx.EXPAND)
            sep.SetBackgroundColour(wx.LIGHT_GREY)
            self.answersPanel.GetSizer().Add(sep, 0, wx.EXPAND | wx.ALL, 3)
        answerPanel = AnswerPanel(self.answersPanel, self.typeCtrl.GetSelection()==1)
        if len(self.answerPanels) > 0:
            answerPanel.noteCtrl.SetValue(0)
        self.answersPanel.GetSizer().Add(answerPanel, 0, 0, 3)
        self.answerPanels.append(answerPanel)
        self.Layout()
        self.mainPanel.GetSizer().FitInside(self.mainPanel)

    def _UpdateLayout(self):
        sel = self.typeCtrl.GetSelection()
        self.caseLabel.Show(sel == 0)
        self.caseCtrl.Show(sel == 0)
        self.qcmTypeLabel.Show(sel == 2)
        self.qcmTypeCtrl.Show(sel == 2)
        self.qcmShuffleLabel.Show(sel == 2)
        self.qcmShuffleCtrl.Show(sel == 2)
        for panel in self.answerPanels:
            panel.SetNumerical(sel == 1)
        self.Layout()

class caracterMenu(wx.Menu):
    def __init__(self, textCtrl, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.textCtrl = textCtrl
        for char in "∞π∈∉⊂⊄∅π∩∪⊥ℕℤⅅℚℝℂΩ":
            self.Append(ord(char), char)
        self.Bind(wx.EVT_MENU, self.OnMenuClick)

    def OnMenuClick(self, event):
        self.textCtrl.WriteText(chr(event.GetId()))


class AnswerPanel(wx.Panel):
    def __init__(self, parent, numerical, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.numerical = None
        self.answerCtrl = wx.TextCtrl(self)
        self.caracterButton = wx.Button(self, wx.ID_ANY, label='∞')
        self.toleranceLabel = wx.StaticText(self, label='Tolérance')
        self.toleranceCtrl = wx.TextCtrl(self, value='0')
        self.feedbackCtrl = wx.TextCtrl(self, wx.ID_ANY, style=wx.HSCROLL | wx.TE_MULTILINE)
        self.noteCtrl = wx.SpinCtrl(self, initial=100)
        self.__set_properties()
        self.__do_layout()
        self.SetNumerical(numerical)
        self.Bind(wx.EVT_BUTTON, self.OnAddCaracter, self.caracterButton)

    def __set_properties(self):
        self.focus = self.answerCtrl
        self.noteCtrl.SetMinSize((130,-1))
        self.answerCtrl.SetMinSize((200,-1))
        self.feedbackCtrl.SetMinSize((300,50))

    def __do_layout(self):
        sizer = wx.FlexGridSizer(3, (3,3))
        sizer.Add(wx.StaticText(self, label='Note :'))
        sizer.Add(self.noteCtrl, 0, wx.ALL, 3)
        # sizer.Add(self.deleteButton, 0, wx.ALL, 3)
        sizer.Add((0, 0))
        sizer.Add(wx.StaticText(self, label='Réponse:'))
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(self.answerCtrl, 0, wx.ALL, 3)
        hsizer.Add(self.caracterButton, 0, wx.ALL, 3)
        sizer.Add(hsizer, 0, wx.ALL,3)

        sizer.Add((0, 0))
        sizer.Add(self.toleranceLabel, 0, wx.ALL, 3)
        sizer.Add(self.toleranceCtrl, 0, wx.ALL, 3)

        sizer.Add((0,0))
        sizer.Add(wx.StaticText(self, label='Feedback :'))
        sizer.Add(self.feedbackCtrl, 0, wx.ALL, 3)
        self.SetSizer(sizer)
        self.Layout()

    def OnAddCaracter(self, event):
        self.PopupMenu(caracterMenu(self.answerCtrl))

    def SetNumerical(self, numerical):
        self.numerical = numerical
        self.toleranceLabel.Show(numerical)
        self.toleranceCtrl.Show(numerical)
        self.caracterButton.Show(not numerical)

    def GetCode(self):
        note = self.noteCtrl.GetValue()
        noteStr = ''
        if note == 100:
            noteStr = '='
        elif note>0:
            noteStr = '%{}%'.format(note)

        feedbackStr = ''
        if self.feedbackCtrl.GetValue() != '':
            feedbackStr = '#' + self.feedbackCtrl.GetValue().replace('\n', '<br/>')

        return'{note}{answer}{tolerance}'.format(
            note=noteStr,
            tolerance=':{}'.format(self.toleranceCtrl.GetValue()) if self.numerical else '',
            answer=self.answerCtrl.GetValue().replace('}','\\}'),
            feedback=feedbackStr
        )


