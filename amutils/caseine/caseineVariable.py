"""
Created on 03/04/20

@author: GuiΩ
"""

import amutils


class CaseineVariable(amutils.DBObject):
    table = 'caseineVariables'

    def Copy(self):
        newV = super().Copy()
        newV.exo = None
        return newV

    def _getattr(self, attr):
        if attr == 'xml':
            return """<variable name="{v.name}">
        <min>{v.min}</min>
        <max>{v.max}</max>
    </variable>""".format(v=self)
        else:
            return super()._getattr(attr)