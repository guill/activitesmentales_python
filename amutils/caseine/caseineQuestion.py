"""
Created on 22/03/20

@author: GuiΩ
"""

import amutils
from amutils import caseine
import re
import wx

ITEMS_TYPES = {'figures': caseine.CaseineFigure, 'answers': caseine.CaseineAnswer, 'variables': caseine.CaseineVariable}

class CaseineQuestion(amutils.DBObject):
    table = 'caseineQuestions'

    # @amutils.debug
    def __init__(self, data=None, forceReload=False):
        super().__init__(data, forceReload)
        self._fields['data'] = None
        for attr in ITEMS_TYPES.keys():
            if data is not None and attr in data.keys():
                self._fields[attr] = data[attr]
            else:
                self._fields[attr] = None
            # if self.type != None:
        if self.type is not None:
            self.data = caseine.CASEINE_TYPES[self.type](data)

    def Copy(self):
        newQ = super().Copy()
        newQ.exo = None
        for attr in ITEMS_TYPES.keys():
            array = []
            for item in self._getattr(attr):
                array.append(item.Copy())
            newQ._fields[attr] = array
        newQ.data = self.data.Copy()
        return newQ

    def ClearFiguresCode(self):
        self._fields['figures'] = []

    @amutils.debug
    def Save(self, commit=True, force=False):
        vars = (self.exo.id, )
        # load items before deleting them from database
        for itemType in ITEMS_TYPES.keys():
            self._getattr(itemType)
        for table in tuple(map(lambda t: t.table, caseine.CASEINE_TYPES.values())) + (
                      caseine.CaseineQuestion.table,
                      caseine.CaseineAnswer.table,
                      caseine.CaseineFigure.table,
                      caseine.CaseineVariable.table):
            if table is not None:
                sql = 'DELETE From "{}" WHERE "exoId"=?;'.format(table)
                if amutils.DEBUG_SQL:
                    print(sql, vars)
                amutils.DBObject.connexion.execute(sql, vars)
        super().Save(commit=False, force=True)
        if self.data is not None:
            self.data.exo = self.exo
            self.data.Save(commit=False, force=force)
            for itemType in ITEMS_TYPES.keys():
                for item in self._getattr(itemType):
                    item.exo = self.exo
                    item.Save(commit=False, force=True)
        if commit:
            amutils.DBObject.connexion.commit()

    @staticmethod
    def FromExo(exo, forceReload=False):
        vars = (exo.id,)
        sql = """SELECT * FROM "caseineQuestions"
                WHERE "exoId"=?;"""
        if amutils.DEBUG_SQL:
            print(sql, vars)
        row = amutils.DBObject.connexion.execute(sql, vars).fetchone()
        if row is not None:
            cq = caseine.CaseineQuestion(row, forceReload)
            if cq.type is not None:
                cq.data = caseine.CASEINE_TYPES[cq.type].FromExo(exo)
            cq._dirty = False
            return cq
        else:
            cq = caseine.CaseineQuestion(None, forceReload)
            cq.exo = exo
            return None

    @staticmethod
    def ConvertExoCode(exoId, exoCode):
        code = exoCode
        figures = {}
        figId = 1
        while code.find("\\begin{tikzpicture}") >= 0:
            figName = "fig_am{}_{}.png".format(exoId, figId, )
            figId += 1
            figStart = code.index("\\begin{tikzpicture}")
            figEnd = code.index("\\end{tikzpicture}") + 17
            figCode = code[figStart:figEnd]
            code = '{}<img src="@@PLUGINFILE@@/{}" alt="" />{}'.format(code[0:figStart], figName, code[figEnd:])
            figures[figName] = CaseineQuestion._latexFigureConvert(figCode)

        code = re.sub(r'\\vspace{.*?}', r'\n', code)

        code = re.sub(r'\\(small|med|big)skip', r'\n', code)
        code = re.sub(r'\\((N|Z|D|Q|R|C){2})', r'\\mathbf{\2}', code)
        code = re.sub(r'\n\s*\n(\s*\n\s*)*', r'<br/>\n', code)
        code = re.sub(r'\\par([^\w])', r'\1', code)
        code = re.sub(r'\\e([^\w])', r'\\mathrm{e}\1', code)
        code = re.sub(r'\\coord\{([^\}]*)\}\{([^\}]*)\}', r'\\begin{pmatrix}\1\\\\\2\\end{pmatrix}', code)
        code = re.sub(r'\\vecteur\{([^\}]*)\}', r'\\overrightarrow{\1}', code)
        code = re.sub(r'\\\\(\[[^\]]*\])?', r'<br>', code)

        code = CaseineQuestion._latexCenter(code)
        while code.find("$") >= 0:
            code = code.replace("$", "\\(", 1)
            code = code.replace("$", "\\)", 1)
        return code, figures

    @staticmethod
    def ToXML():
        questions = []
        for row in amutils.DBObject.connexion.execute(
            """SELECT exoId FROM caseineQuestions
                WHERE status='to export'"""):
            questions.append(caseine.CaseineQuestion.FromExo(amutils.Exo.FromId(row['exoId'])))
        questionsXML = ''
        for question in questions:
            if question.data is not None:
                questionsXML +=  question.xml
        return '<?xml version="1.0" encoding="UTF-8"?>\n<quiz>\n{}</quiz>\n'.format(questionsXML)

    @staticmethod
    def FromXML(xmlFileName, amFrame):
        import xml.etree.ElementTree as ET
        tree = ET.parse(xmlFileName)
        root = tree.getroot()
        for questionNode in root:
            type = questionNode.get('type')
            if  type == 'category':
                continue
            if type not in ('algebra', 'cloze', 'multichoice', 'regexp', 'truefalse'):
                wx.MessageDialog(None, 'type de question non géré : {}'.format(type)).ShowModal()
                continue
            name = questionNode.findall('name')[0][0].text
            if name[0:2] == 'am':
                exoId = name[2:]
            else:
                exoId = name[9:]
            exo = amutils.Exo.FromId(exoId)
            questiontext = questionNode.findall('questiontext')[0][0].text
            if exo.caseineQuestion is None:
                msgBox = wx.MessageDialog(
                    None,
                    """Cette question n'existe pas encore dans la base de données caseine/local.
---------------------------
{}
---------------------------
{}
---------------------------
Importer ?
""".format(exo.code, questiontext), 'Attention', style=wx.OK | wx.CANCEL)
                if msgBox.ShowModal() != wx.ID_OK:
                    continue
                caseineQ = CaseineQuestion._XMLToCaseineQuestion(questionNode)
                caseineQ.exo = exo
                exo.caseineQuestion = caseineQ
                if not amFrame.EditExo(exo):
                    exo.caseineQuestion = None
                continue
            if exo.caseineQuestion.questiontxt != questiontext:
                msgBox = wx.MessageDialog(
                    None,
                    """Le contenu de la question est différent. Faut-il l\'importer caseine/local ?
---------------------------
{}
---------------------------
{}"""
                        .format(questiontext, exo.caseineQuestion.questiontxt), 'Attention', style=wx.OK | wx.CANCEL)
                if msgBox.ShowModal() != wx.ID_OK:
                    continue
                exo.caseineQuestion.questiontxt = questiontext
            exo.caseineQuestion.status = 'exported'
            exo.Save(force=True)

    def _getattr(self, attr):
        if attr in ITEMS_TYPES.keys():
            return self._GetItems(attr).copy()
        elif attr == 'xml':
            return self._GetXML()
        elif attr in self._fields.keys():
            return super()._getattr(attr)
        else:
            return self.data._getattr(attr)


    def _setattr(self, attr, value):
        if attr == 'type':
            super()._setattr(attr, value)
            self.data = caseine.CASEINE_TYPES[value]()
        elif attr in ITEMS_TYPES.keys():
            self._fields[attr] = value
        elif attr in self._fields.keys():
            super()._setattr(attr, value)
        else:
            self.data._setattr(attr, value)

    def _GetXML(self):
        answersXML = ''
        for answer in self.answers:
            answersXML += answer.xml
        tagsXML = ''
        for niveau in self.exo.niveaux:
            tagsXML += '\n\t\t<tag><text><![CDATA[{}]]></text></tag>'.format(niveau.label)
        for theme in self.exo.themes:
            tagsXML += '\n\t\t<tag><text><![CDATA[{}]]></text></tag>'.format(theme.fullLabel)
        figuresQuestion = ''
        for figure in self.figures:
            if self.questiontxt.find("@@PLUGINFILE@@/" + figure.name) > -1:
                figuresQuestion += figure.xml
        figuresFeedback = ''
        for figure in self.figures:
            if self.generalfeedback.find("@@PLUGINFILE@@/" + figure.name) > -1:
                figuresFeedback += figure.xml
        variablesXML = ''
        for variable in self.variables:
            variablesXML += variable.xml
        return """\n
<!-- question {q.exo.id}  -->
\t<question type="{q.type}">
\t\t<name>
\t\t\t<text>question_{q.exo.id}</text>
\t\t</name>
\t\t<questiontext format="html">
\t\t\t<text><![CDATA[<p>{q.questiontxt}</p>]]></text>{figuresQuestion}
\t\t</questiontext>
\t\t<generalfeedback format="html">
\t\t\t<text><![CDATA[{q.generalfeedback}]]></text>{figuresFeedback}
\t\t</generalfeedback>
\t\t<defaultgrade>{q.defaultgrade}</defaultgrade>
\t\t<penalty>{q.penalty}</penalty>
\t\t<hidden>0</hidden>
\t\t<idnumber>{q.exo.id}</idnumber>{data}{variables}{answer}
\t\t<tags>{tags}
\t\t</tags>
\t</question>
""".format(q=self,
           figuresQuestion=figuresQuestion,
           figuresFeedback=figuresFeedback,
           data=self.data.xml,
           variables=variablesXML,
           answer=answersXML,
           tags=tagsXML)


    def _GetItems(self, attr):
        cls = ITEMS_TYPES[attr]
        if self._fields[attr] is None:
            self._fields[attr] = []
            vars = (self.exo.id,)
            for row in amutils.DBObject.connexion.execute(
                    """SELECT * FROM "{}"
                        WHERE "exoId"=?;""".format(cls.table), vars):
                self._fields[attr].append(cls(row))
        return self._fields[attr]

    @staticmethod
    def _latexCenter(txt):
        index = txt.find('\\centering')
        while index > 0:
            indexIn = txt.rfind('{',0,index)
            indexOut = txt.find('}',index)
            if indexIn >= 0 and indexOut >= 0:
                txt = '{}<center>{}</center>{}'.format(txt[0:indexIn], txt[index+10:indexOut], txt[indexOut+1:])
                index = txt.find('\\centering')
            else:
                index = -1
        return txt

    @staticmethod
    def _latexFigureConvert(figs):
        end = figs[19:]
        if re.match('^\s*\\[', end):
            end = re.sub('^\s*\\[', '[scale=2,', end)
        else:
            end = '[scale=2]' + end
        end = re.sub('{\$', '{\\Large $', end)
        end = re.sub('\]\s*\{', ']{\\Large ', end)
        end = re.sub('stealth', '', end)
        end = re.sub(r'(axes|vecteur)', r'\1,-{Latex[width=2mm,length=3mm]}', end)
        end = re.sub(r'grille', r'grille,black!40', end)
        return r'\begin{tikzpicture}' + end

    @staticmethod
    def _XMLToCaseineQuestion(xmlNode):
        type = xmlNode.get('type')
        cq = CaseineQuestion({'type': type})
        cq.status = 'exported'
        cq.questiontxt = xmlNode.findall('questiontext')[0][0].text
        cq.generalfeedback = xmlNode.findall('generalfeedback')[0][0].text
        if cq.generalfeedback is None:
            cq.generalfeedback = ''
        if len(xmlNode.findall('defaultgrade')):
            cq.defaultgrade = float(xmlNode.findall('defaultgrade')[0].text)
        else:
            cq.defaultgrade = 1.0
        cq.penaly = float(xmlNode.findall('penalty')[0].text)
        # if xmlNode.getElementsByTagName('idnumber')[0].hasChildren()
        cq.idnumber = xmlNode.findall('idnumber')[0].text
        if cq.idnumber is None:
            cq.idnumber = ''
        variables = []
        for xmlVariable in xmlNode.findall('variable'):
            variable = caseine.CaseineVariable()
            variable.name = xmlVariable.get('name')
            variable.min = float(xmlVariable.findall('min')[0].text)
            variable.max = float(xmlVariable.findall('max')[0].text)
            variables.append(variable)
        cq.variables = variables
        answers = []
        for xmlAnswer in xmlNode.findall('answer'):
            answer = caseine.CaseineAnswer()
            answer.note = int(xmlAnswer.get('fraction'))
            answer.answer = xmlAnswer.findall('text')[0].text
            answer.feedback = xmlAnswer.findall('feedback')[0][0].text
            if answer.feedback is None:
                answer.feedback = ''
            answers.append(answer)
        cq.answers = answers
        if type == 'algebra':
            CaseineQuestion._XMLToAlgebra(xmlNode, cq)
        elif type == 'cloze':
            pass
        elif type == 'multichoice':
            CaseineQuestion._XMLToMultichoice(xmlNode, cq)
        elif type == 'regexp':
            CaseineQuestion._XMLToRegexp(xmlNode, cq)
        elif type == 'truefalse':
            CaseineQuestion._XMLToTruefalse(xmlNode, cq)
        return cq

    @staticmethod
    def _XMLToAlgebra(xmlNode, caseineQ):
        caseineQ.compareby = xmlNode.findall('compareby')[0].text
        caseineQ.tolerance = float(xmlNode.findall('tolerance')[0].text)
        caseineQ.nchecks = int(xmlNode.findall('nchecks')[0].text)
        caseineQ.answerprefix = xmlNode.findall('answerprefix')[0][0].text
        if caseineQ.answerprefix is None:
            caseineQ.answerprefix = ''

    @staticmethod
    def _XMLToMultichoice(xmlNode, caseineQ):
        caseineQ.single = xmlNode.findall('single')[0].text == 'true'
        caseineQ.shuffleanswers = xmlNode.findall('shuffleanswers')[0].text == 'true'
        caseineQ.answernumbering = xmlNode.findall('answernumbering')[0].text
        caseineQ.correctfeedback = xmlNode.findall('correctfeedback')[0][0].text
        if caseineQ.correctfeedback is None:
            caseineQ.correctfeedback = ''
        caseineQ.partiallycorrectfeedback = xmlNode.findall('partiallycorrectfeedback')[0][0].text
        if caseineQ.partiallycorrectfeedback is None:
            caseineQ.partiallycorrectfeedback = ''
        caseineQ.incorrectfeedback = xmlNode.findall('incorrectfeedback')[0][0].text
        if caseineQ.incorrectfeedback is None:
            caseineQ.incorrectfeedback = ''

    @staticmethod
    def _XMLToRegexp(xmlNode, caseineQ):
        caseineQ.usehint = xmlNode.findall('usehint')[0].text == 'true'
        caseineQ.usecase = float(xmlNode.findall('usecase')[0].text) == 'true'
        caseineQ.studentshowalternate = xmlNode.findall('studentshowalternate')[0].text == 'true'

    @staticmethod
    def _XMLToTruefalse(xmlNode, caseineQ):
        for answer in caseineQ.answers:
            if answer.answer == 'true':
                answer.answer = 'Vrai'
            elif answer.answer == 'false':
                answer.answer = 'Faux'
        caseineQ.type = 'multichoice'
        caseineQ.data = caseine.CaseineMultichoiceQuestion()
        caseineQ.single = True
        caseineQ.shuffleanswers = False
        caseineQ.answernumbering = 'none'
        caseineQ.correctfeedback = ''
        caseineQ.partiallycorrectfeedback = ''
        caseineQ.incorrectfeedback = ''