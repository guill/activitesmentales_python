"""
Created on 24/03/20

@author: GuiΩ
"""

import amutils


class CaseineClozeQuestion:
    table = None

    def __init__(self, data=None, forceReload=False):
        self.exo = None

    @property
    def xml(self):
        return ''

    @staticmethod
    def FromExo(exo, forceReload=False):
        return CaseineClozeQuestion()

    def Copy(self):
        pass

    def Save(self, commit=True, force=False):
        pass


