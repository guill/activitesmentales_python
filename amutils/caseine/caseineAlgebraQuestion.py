"""
Created on 22/03/20

@author: GuiΩ
"""

import amutils

class CaseineAlgebraQuestion(amutils.DBObject):
    table = 'caseineAlgebraQuestions'

    def _getattr(self, attr):
        if attr == 'xml':
            return self._GetXML()
        else:
            return super()._getattr(attr)

    def Copy(self):
        newQ = super().Copy()
        newQ.exo = None
        return newQ

    @staticmethod
    def FromExo(exo, forceReload=False):
        vars = (exo.id,)
        sql = """SELECT * FROM "caseineAlgebraQuestions"
                WHERE "exoId"=?;"""
        if amutils.DEBUG_SQL:
            print(sql, vars)
        row = amutils.DBObject.connexion.execute(sql, vars).fetchone()
        algebraQ = CaseineAlgebraQuestion(row, forceReload)
        algebraQ._dirty = False
        return algebraQ

    def _GetXML(self):
        return """
\t\t<compareby>eval</compareby>
\t\t<tolerance>0.001</tolerance>
\t\t<nchecks>10</nchecks>
\t\t<disallow>
\t\t\t<text></text>
\t\t</disallow>
\t\t<allowedfuncs></allowedfuncs>
\t\t<answerprefix>
\t\t\t<text><![CDATA[{q.answerprefix}]]></text>
\t\t</answerprefix>""".format(q=self)



