
from .caseineVariable import CaseineVariable
from .caseineAlgebraQuestion import CaseineAlgebraQuestion
from .caseineClozeQuestion import CaseineClozeQuestion
from .caseineFigure import CaseineFigure
from .caseineAnswer import CaseineAnswer
from .caseineQuestion import CaseineQuestion
from .caseineMultichoiceQuestion import CaseineMultichoiceQuestion
from .caseineRegexpQuestion import CaseineRegexpQuestion

CASEINE_TYPES = {'algebra': CaseineAlgebraQuestion,
                 'cloze': CaseineClozeQuestion,
                 'multichoice': CaseineMultichoiceQuestion,
                 'regexp': CaseineRegexpQuestion}
EXPORT_STATUS = ('not exported', 'to export', 'exported')
MULTICHOICE_NUMBERING_OPTIONS = ('abc', 'ABCD','123', 'iii', 'IIII', 'none')
