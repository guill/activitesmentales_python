"""
Created on 24/03/20

@author: GuiΩ
"""

import amutils
from amutils import caseine
import re


class CaseineRegexpQuestion(amutils.DBObject):
    table = 'caseineRegexpQuestions'

        
    def _getattr(self, attr):
        if attr == 'xml':
            return self._GetXML()
        else:
            return super()._getattr(attr)

    @staticmethod
    def FromExo(exo, forceReload=False):
        vars = (exo.id,)
        sql = """SELECT * FROM "caseineRegexpQuestions"
                WHERE "caseineQuestionId"=?;"""
        row = amutils.DBObject.connexion.execute(sql, vars).fetchone()
        regexpQ = CaseineRegexpQuestion(row, forceReload)
        regexpQ._dirty = False
        return regexpQ

    def ClearFiguresCode(self):
        self._fields['figures'] = []

    def _GetXML(self):
        return  """
\t\t<usehint>{q.usehint}</usehint>
\t\t<usecase>{q.usecase}</usecase>
\t\t<studentshowalternate>{q.studentshowalternate}</studentshowalternate>""".format(q=self)

