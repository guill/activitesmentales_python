#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 22:08:39 2020

@author: guill
"""

import amutils
import sqlite3


class DBObject:
    table = None
    connexion = None

    def __init__(self, data=None, forceReload=False):
        self._fields = {}
        if not data:
            data = {}
        # TODO: memorise the table infos
        for row in DBObject.connexion.execute(
                'PRAGMA table_info("{}");'.format(self.table)):
            if row['name'] not in data.keys() \
               and (row['name'][-2:] != 'Id'
                    or row['name'][:-2] not in data.keys()):
                # Change row to dict to write inside
                if isinstance(data, sqlite3.Row):
                    data = dict(zip(data.keys(), data))
                if row['dflt_value']:
                    data[row['name']] = \
                        DBObject.connexion.execute(
                            'SELECT {} '.format(row['dflt_value'])) \
                        .fetchone()[0]  # do not escape {} because it must be evaluated
                else:
                    data[row['name']] = None
        # TODO: set values for all unsetted values ?
        #if 'id' not in data.keys():
         #   self._fields['id'] = None
            # data['id'] = None
        for key in data.keys():
            if key[-2:] == "Id":
                vars = (key,)
                sql = """SELECT "table"
                         FROM pragma_foreign_key_list("{}")
                         WHERE "from"=?;""".format(self.table)
                if amutils.DEBUG_SQL:
                    print(sql, vars)
                row = DBObject.connexion.execute(sql, vars).fetchone()
                moduleName = row['table'][:-1]
                className = moduleName[0].upper() + moduleName[1:]
                if data[key] is not None:
                    if className[:7] == 'Caseine':
                        self._fields[key[:-2]] = \
                            getattr(getattr(amutils.caseine, moduleName), className) \
                                .FromId(int(data[key]), forceReload)
                    else:
                        self._fields[key[:-2]] = \
                            getattr(getattr(amutils, moduleName), className)\
                            .FromId(int(data[key]))
                else:
                    self._fields[key[:-2]] = None
            else:
                self._fields[key] = data[key]
        self._dirty = True

    def __getattr__(self, attr):
        return self._getattr(attr)

    def _getattr(self, attr):
        return self._fields[attr]

    def __setattr__(self, attr, value):
        if attr[0] == '_':
            super().__setattr__(attr, value)
        else:
            self._setattr(attr, value)

    def _setattr(self, attr, value):
        if attr not in self._fields.keys() or self._fields[attr] != value:
            self._fields[attr] = value
            self._dirty = True

    def __repr__(self):
        s = self.__class__.__name__
        if 'id' in self._fields:
            s += '_{}'.format(self.id)
        else:
            s += '({})'.format(
                ' ; '.join(['{}: {}'.format(key, val)
                            for key, val in self._fields.items()]))
        return s

    def Get(self, field):
        return self._fields[field]

    def Set(self, field, value):
        if self.Get(field) != value:
            self._fields[field] = value
            self._dirty = True

    def Copy(self):
        newObj = self.__class__()
        newObj._fields = self._fields.copy()
        if 'id' in self._fields.keys():
            newObj._fields['id'] = None
        newObj._dirty = True
        return newObj

    @amutils.debug
    def Save(self, commit=True, force=False):
        """

        :param commit: commit after the sql statement ?
        :param force: force saving even if _dirty is False
        :return:
        """
        if not force and not self._dirty:
            return
        fields = []
        values = []
        for row in DBObject.connexion.execute(
                'PRAGMA table_info("{}");'.format(self.table)):
            if row['name'] == 'id':
                continue
            field = row['name']
            fields.append(field)
            if field[-2:] == 'Id':
                if self._fields[field[:-2]]:
                    values.append(self._fields[field[:-2]]._fields['id'])
                else:
                    values.append(None)
            else:
                values.append(self._fields[field])
        if 'id' not in self._fields.keys() or self._fields['id'] is None:
            sql = 'INSERT INTO "{}" ({}) VALUES ({});'.format(
                      self.table,
                      ", ".join(map(lambda f: '"{}"'.format(f), fields)),
                      ", ".join(["?" for _ in range(len(fields))]))
            vars = tuple(values)
        elif self._fields['id'] is not None \
            and DBObject.connexion.execute(
                'SELECT COUNT(*) from "{}" WHERE "id"=?'.format(self.table),
                (self._fields['id'], )).fetchone()[0] == 0:
            sql = 'INSERT INTO "{}" ("id", {}) VALUES (?, {});'.format(
                      self.table,
                      ", ".join(map(lambda f: '"{}"'.format(f), fields)),
                      ", ".join(["?" for _ in range(len(fields))]))
            values.insert(0, self._fields['id'])
            vars = tuple(values)
        else:
            sql = 'UPDATE "{}" SET {} WHERE "id"=?;'.format(
                    self.table,
                    ", ".join(map(lambda f: '"{}"=?'.format(f), fields)))
            values.append(self._fields['id'])
            vars = tuple(values)
        if amutils.DEBUG_SQL:
            print(sql, vars)
        cursor = DBObject.connexion.execute(sql, vars)
        if 'id' in self._fields.keys() and self._fields['id'] is None:
            # TODO: vérifier cohérence si commit = False
            self._fields['id'] = cursor.lastrowid
        self.__AddToList()
        if commit:
            DBObject.connexion.commit()
        self._dirty = False

    def Delete(self, commit=True):
        vars = (self._fields['id'],)
        sql = 'DELETE FROM "{}" WHERE "id"=?;'.format(self.table)
        if amutils.DEBUG_SQL:
            print(sql, vars)
        DBObject.connexion.execute(sql, vars)
        if commit:
            DBObject.connexion.commit()
        self._dirty = True
        self.__RemoveFromList()

    @classmethod
    def Clear(cls):
        """
        Supprime la liste des objets chargés depuis la base de données

        Parameters
        ----------
        cls : class
            Classe pour laquelle il faut supprimer les objets.

        Returns
        -------
        None.

        """
        del cls._list

    @classmethod
    # @amutils.debug
    def FromId(cls, id, forceReload=False):
        """
        Get an object from its Id

        Parameters
        ----------
        cls: class
            the class of the requested object.
        id: int
            id of the requested object.
        forceReload: boolean, optional
            if True, force the reading of the database even if this object is
            already if the known list. The default is False.

        Returns
        -------
        obj: Object
            the object of the given id.

        """
        obj = cls.__GetFromList(id)
        if not obj or forceReload:
            vars = (id,)
            row = DBObject.connexion.execute(
                'SELECT * FROM "{}" WHERE "id"=?;'.format(cls.table),
                vars).fetchone()
            if row is None:
                return None
            obj = cls(row)
            obj._dirty = False
            obj.__AddToList()
        return obj

    @classmethod
    def GetList(cls, ordered=True):
        """
        Parameters
        ----------
        cls : class
            the class for which the objects list must be given.
        ordered : boolean, optional
            If True, the list will be sorted by "position" if exists.
            The default is True.

        Returns
        -------
        list : list of objects
            Returns the list of all the object for a given class,
            possibly sorted.
        """
        list = []
        if ordered:
            fields = [row['name'] for row in DBObject.connexion.execute(
                          "PRAGMA table_info({})".format(cls.table))]
            ordered &= "position" in fields
        sqlStr = 'SELECT "id" FROM "{}" {};'.format(
            cls.table,
            ' ORDER BY "position"' if ordered else "")
        for row in DBObject.connexion.execute(sqlStr):
            list.append(cls.FromId(row['id']))
        return list

    @classmethod
    def __GetFromList(cls, id):
        if '_list' in cls.__dict__ and id in cls._list.keys():
            return cls._list[id]
        else:
            return None

    def __AddToList(self):
        if 'id' not in self._fields.keys():
            return
        if '_list' not in self.__class__.__dict__:
            self.__class__._list = {}
        self.__class__._list[self._fields['id']] = self

    def __RemoveFromList(self):
        del self.__class__._list[self._fields['id']]

    @staticmethod
    def Open(dbFileName):
        connexion = sqlite3.connect(dbFileName)
        connexion.row_factory = sqlite3.Row
        connexion.execute("PRAGMA foreign_keys = ON;")
        DBObject.connexion = connexion

    @staticmethod
    def Close():
        DBObject.connexion.close()
