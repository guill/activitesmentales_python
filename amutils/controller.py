#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 14 14:54:41 2020

@author: user
"""

import amutils
from amutils import do


class Controller:

    def __init__(self, amFrame=None):
        # TODO: user config for undolist size
        self.undoList = do.UndoList(amFrame, 20)
        self.amFrame = amFrame
        self.exoBuffer = None
        self.seanceBuffer = None
        self.feuilleBuffer = None

    @amutils.debug
    def DoExoAction(self,
                    action,
                    exo=None,
                    seanceSource=None,
                    positionSource=None,
                    seanceDest=None,
                    positionDest=None,
                    code=None,
                    position=None,
                    niveaux=None,
                    themes=None,
                    updateDate=None):
        """
        in all the cases, the score must be None
        if action == do.ID_EXO_ADD, the exo is add in the seanceDest
        if action == do.ID_EXO_REMOVE, the exo is removed from the seanceDest
        if action == do.ID_EXO_MOVE, the exo is move from seanceSource to
            seanceDest. If there's an exo in senaceDest, it will be replaced
        if action == do.ID_EXO_SWAP, the exos are swapped
        if action == do.ÏD_EXO_NEW, a new Exo is created with the given fields
        if action == do.ID_EXO_DELETE, the exo is deleted

        Parameters
        ----------
        action: int.
        exo: Exo, optional
            The default is None.
        seanceSource: Seance, optional
            The default is None.
        positionSource: int, optional
            The default is None.
        seanceDest: Seance, optional
            The default is None.
        positionDest: int, optional
            The default is None.
        code: str, optional
            The default is None.
        position: int, optional
            The default is None.
        niveaux: list of Niveau, optional
            The default is None.
        themes: list of Theme, optional
            The default is None.

        Returns
        -------
        None.

        """
        if action == do.ID_EXO_NEW:
            todo = do.UndoableExoNewAction(exo=exo)
        elif action == do.ID_EXO_DELETE:
            todo = do.UndoableExoDelAction(exo=exo)
        elif action == do.ID_EXO_EDIT:
            todo = do.UndoableExoEditAction(exo=exo,
                                            code=code,
                                            position=exo.position,
                                            niveaux=niveaux,
                                            themes=themes,
                                            updateDate=updateDate)
        else:
            todo = do.UndoableExoMoveAction(
                action=action,
                exo=exo,
                seanceSource=seanceSource,
                positionSource=positionSource,
                seanceDest=seanceDest,
                positionDest=positionDest)
        self.undoList.Add(todo)

    @amutils.debug
    def DoSeanceAction(self,
                       action,
                       seance=None,
                       feuilleDest=None,
                       positionDest=None,
                       date=None,
                       exos=None,
                       scores=None,
                       desc=''):
        if action == do.ID_SEANCE_ADD:
            todo = do.UndoableSeanceMoveAction(
                action=action,
                seance=seance,
                feuilleDest=feuilleDest,
                positionDest=positionDest)
        elif action == do.ID_SEANCE_REMOVE:
            assert seance is not None
            feuilleDest = seance.feuille
            todo = do.UndoableSeanceMoveAction(
                action=action,
                seance=seance)
            # if len(feuilleDest.seances) <= 1:
            #     todo2 = do.UndoableSeanceAddAction(
            #         seance=amutils.Seance(),
            #         feuille=feuilleDest,
            #         position=0)
            #     todo = do.UndoableMultipleAction(
            #         actions=[todo, todo2])
        elif action == do.ID_SEANCE_EDIT:
            todo = do.UndoableSeanceEditAction(
                seance=seance,
                feuille=feuilleDest,
                position=positionDest,
                date=date,
                exos=exos,
                scores=scores,
                desc=desc)
        self.undoList.Add(todo)

    @amutils.debug
    def DoFeuilleAction(self,
                        action,
                        feuille=None,
                        classe=None,
                        position=None):
        if action == do.ID_FEUILLE_ADD:
            assert feuille is not None
            assert classe is not None
            assert position is not None
            todo = do.UndoableFeuilleMoveAction(
                action=action,
                feuille=feuille,
                classe=classe,
                position=position)
        elif action == do.ID_FEUILLE_REMOVE:
            assert feuille is not None
            classe = feuille.classe
            todo = do.UndoableFeuilleMoveAction(
                action=action,
                feuille=feuille)
            if len(classe.feuilles) <= 1:
                newFeuille = amutils.Feuille()
                todo2 = do.UndoableFeuilleAddAction(
                    feuille=newFeuille,
                    classe=classe,
                    position=0)
                # todo3 = do.UndoableSeanceMoveAction(
                #     do.ID_SEANCE_ADD,
                #     seance=amutils.Seance(),
                #     feuilleDest=newFeuille,
                #     positionDest=0)
                todo = do.UndoableMultipleAction([todo, todo2])
                todo.label = 'Suppression de feuille'
        self.undoList.Add(todo)

    def Redo(self):
        self.undoList.Redo()

    def Undo(self):
        self.undoList.Undo()

    def __repr__(self):
        return 'controller'
