#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 08:11:51 2020

@author: guill
"""

import amutils
import os
import wx


class Exo(amutils.DBObject):
    table = "exos"
    thumbPath = None
    defaultThumb = None

    def __init__(self, data=None):
        super().__init__(data)
        self.__thumb = None
        self._fields['niveaux'] = None
        self._fields['themes'] = None
        if amutils.EXPORT_CASEINE:
            self._fields['caseineQuestion'] = None

    def Copy(self):
        newExo = super().Copy()
        newExo.__thumb = self.__thumb
        newExo.niveaux = self.niveaux
        newExo.themes = self.themes
        if amutils.EXPORT_CASEINE and self.caseineQuestion is not None:
            newExo.caseineQuestion = self.caseineQuestion.Copy()
        return newExo

    def IsUsed(self):
        """
        Returns
        -------
        bool
            False only if the exo was never used in a Seance.
        """
        vars = (self.id,)
        row = amutils.DBObject.connexion.execute(
                'SELECT COUNT(*) FROM "lienExosSeances" WHERE "exoId"=?;',
                vars).fetchone()
        return row[0] > 0

    def _getattr(self, attr):
        if attr == 'niveaux':
            return self.__GetNiveaux().copy()
        elif attr == 'themes':
            return self.__GetThemes().copy()
        elif attr == 'thumb':
            return self.__GetThumb()
        elif attr == 'caseineQuestion':
            return self.__GetCaseineQuestion()
        else:
            return super()._getattr(attr)
    
    @amutils.debug
    def _setattr(self, attr, value):
        if attr == 'code':
            if self.code != value:
                super().Set('code', value)
                self.UpdateThumb()
        else:
            super()._setattr(attr, value)

    def GetScores(self, classe):
        __scores = []
        __dates = []
        vars = (self.id, classe.id)
        for row in amutils.DBObject.connexion.execute(
            """SELECT "score", "date"
                FROM "lienExosSeances" LES
                    LEFT JOIN "seances" S ON S."id"=LES."seanceId"
                    LEFT JOIN "feuilles" F ON F."id"=S."feuilleId"
                WHERE "exoId"=? AND F."classeId"=?
                ORDER BY F."position", S."position", LES."position";""", vars):
            __scores.append(row['score'])
            __dates.append(row['date'])
        return {'dates': __dates.copy(), 'scores': __scores.copy()}

    def __GetThumb(self):
        flagOk = False
        if not self.__thumb and self.id is not None:
            path = os.path.join(Exo.thumbPath, "question_{:06d}.png".format(self.id))

            if not os.path.isfile(path):
                if self.UpdateThumb():
                    flagOk = True
            # TODO : gérer les fichier existants mais corrompus
            else:
                self.__thumb = wx.Image(path).ConvertToBitmap()
                flagOk = True
            if not flagOk or self.__thumb is None:
                self.__thumb = Exo.GetDefaultThumb()
        return self.__thumb

    @amutils.debug
    def UpdateThumb(self):
        self.__thumb = None
        return amutils.LatexModel.FromId(-1).CompileModel(exo=self)

    def Delete(self, commit=True):
        vars = (self.id,)
        amutils.DBObject.connexion.execute(
            'DELETE FROM "lienExosNiveaux" WHERE "exoId"=?;', vars)
        amutils.DBObject.connexion.execute(
            'DELETE FROM "lienExosThemes" WHERE "exoId"=?;', vars)
        super().Delete(commit=False)
        if commit:
            amutils.DBObject.connexion.commit()
        self._dirty = True

    def Save(self, commit=True, force=False):
        super().Save(commit=False, force=force)
        if self.niveaux is not None:
            vars = (self.id,)
            amutils.DBObject.connexion.execute(
                'DELETE FROM "lienExosNiveaux" WHERE "exoId"=?;', vars)
            for niveau in self.niveaux:
                vars = (self.id, niveau.id)
                amutils.DBObject.connexion.execute(
                    """INSERT INTO "lienExosNiveaux" ("exoId", "niveauId")
                    VALUES (?, ?);""", vars)
        if self.themes is not None:
            vars = (self.id,)
            amutils.DBObject.connexion.execute(
                'DELETE FROM "lienExosThemes" WHERE "exoId"=?;', vars)
            for theme in self.themes:
                vars = (self.id, theme.id)
                amutils.DBObject.connexion.execute(
                    """INSERT INTO "lienExosThemes" ("exoId", "themeId")
                    VALUES (?, ?);""", vars)
        if amutils.EXPORT_CASEINE and self.caseineQuestion is not None:
            self.caseineQuestion.exo = self
            self.caseineQuestion.Save(commit=False, force=force)
        if commit:
            amutils.DBObject.connexion.commit()
        self._dirty = False
        return self

##################
# Static Methods #
##################

    @staticmethod
    def SetThumbPath(path):
        Exo.thumbPath = path

    @staticmethod
    def IncreasePosition(fromPosition, exceptId):
        vars = (fromPosition, exceptId)
        amutils.DBObject.connexion.execute(
            'UPDATE "exos" SET "position"="position"+1 WHERE "position">=? AND "id"!=?;',
            vars)
        for row in amutils.DBObject.connexion.execute(
                'SELECT "id", "position" FROM "exos" WHERE "position">=? AND "id"!=?;', vars):
            id = row['id']
            if id in Exo.list.keys():
                Exo.list[id].position = row['position']

    @staticmethod
    def GetSelection(niveaux=None, themes=None):
        constraint = ''
        if niveaux is not None:
            niveauIds = list(map(lambda niveau: str(niveau.id), niveaux))
            if len(niveauIds):
                constraint = 'WHERE LEN."niveauId" IN ({})'.format(
                    ", ".join(niveauIds))
        if themes is not None:
            if constraint != '':
                constraint += ' AND '
            else:
                constraint = 'WHERE '
            constraint += ' LET."themeId" IN ({})'.format(", ".join(
                map(lambda theme: str(theme.id), themes)))
        exos = []
        sql = """SELECT "id" FROM "exos" AS E
                    LEFT JOIN "lienExosNiveaux" AS LEN ON E."id"=LEN."exoId"
                    LEFT JOIN "lienExosThemes" AS LET ON E."id"=LET."exoId"
                {}
                ORDER BY "position";""".format(constraint)
        for row in amutils.DBObject.connexion.execute(sql):
            exos.append(Exo.FromId(row['id']))
        return exos

###################
# Private methods #
###################

    def __GetCaseineQuestion(self):
        from amutils import caseine
        if self._fields['caseineQuestion'] is None:
            self._fields['caseineQuestion'] = caseine.CaseineQuestion.FromExo(self)
        return self._fields['caseineQuestion']

    def __GetNiveaux(self):
        if self._fields['niveaux'] is None:
            self._fields['niveaux'] = []
            vars = (self.id,)
            for row in amutils.DBObject.connexion.execute(
                    """SELECT "niveauId" FROM "lienExosNiveaux"
                        WHERE "exoId"=?;""", vars):
                self._fields['niveaux'].append(
                    amutils.Niveau.FromId(row['niveauId']))
        return self._fields['niveaux']

    def __GetThemes(self):
        if self._fields['themes'] is None:
            self._fields['themes'] = []
            vars = (self.id,)
            for row in amutils.DBObject.connexion.execute(
                    """SELECT "themeId" FROM "lienExosThemes"
                        WHERE "exoId"=?;""", vars):
                self._fields['themes'].append(
                    amutils.Theme.FromId(row['themeId']))
        return self._fields['themes']

    @staticmethod
    def GetDefaultThumb():
        if Exo.defaultThumb is None:
            code = "\\begin{tikzpicture} \\draw (0,0) node[rotate=20]{\\large Aperçu Impossible};\\end{tikzpicture}"
            Exo.defaultThumb, _ = amutils.LatexModel.FromId(-2).codeToBitmap(code)
        return Exo.defaultThumb